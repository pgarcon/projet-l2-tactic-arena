ifeq ($(OS),Windows_NT)
	detected_OS := Windows
else
	detected_OS := $(shell uname -s)
endif

ifeq ($(detected_OS),Windows)
	SDL := -I include -L lib -lmingw32 -lSDL2main -lSDL2 -lSDL2_image -lSDL2_ttf -mwindows
endif

ifeq ($(detected_OS),Linux)
	SDL := $(sdl2-config --cflags --libs) -lSDL2 -lSDL2_image -lSDL2_ttf
endif


all : main clean

main: mainSDL clean

menu_terminale.o : ./src/menu_terminale.c ./include/fonction.h ./include/menu_terminale.h
		gcc -o menu_terminale.o -c ./src/menu_terminale.c


mainSDL: menu.o selection.o map.o equipe.o lignedevue.o personnage.o assassin.o tank.o mage.o healer.o archer.o epeiste.o race.o attaque.o liste_malus.o malus.o dictionnaire_attaques.o liste_positions.o fonctions_deplacements.o nom_alea.o  map2D.o
		gcc ./src/final_SDL.c -o bin/final_SDL bin/menu.o bin/selection.o ./bin/map.o ./bin/equipe.o ./bin/lignedevue.o ./bin/personnage.o ./bin/assassin.o ./bin/tank.o ./bin/mage.o ./bin/healer.o ./bin/archer.o ./bin/epeiste.o ./bin/race.o ./bin/attaque.o ./bin/liste_malus.o ./bin/malus.o ./bin/dictionnaire_attaques.o ./bin/liste_positions.o ./bin/fonctions_deplacements.o ./bin/nom_alea.o bin/map2D.o $(SDL)

clean:
	rm -rf *.o
	rm -rf bin/*.o

mrpropre: clean
	rm -rf bin/final_SDL
	rm -rf bin/test_terminal
	rm -rf bin/*.exe


#---------------#
# TEST TERMINAL #
#---------------#

test_terminal: lignedevue.o personnage.o test_terminal.o map.o tank.o epeiste.o healer.o assassin.o mage.o archer.o liste_positions.o fonctions_deplacements.o attaque.o dictionnaire_attaques.o malus.o liste_malus.o equipe.o race.o nom_alea.o
	gcc -o ./bin/test_terminal ./bin/lignedevue.o ./bin/personnage.o ./bin/map.o ./bin/tank.o ./bin/healer.o ./bin/epeiste.o ./bin/assassin.o ./bin/mage.o ./bin/archer.o ./bin/liste_positions.o ./bin/fonctions_deplacements.o ./bin/test_terminal.o ./bin/dictionnaire_attaques.o ./bin/attaque.o ./bin/malus.o ./bin/liste_malus.o ./bin/equipe.o ./bin/race.o ./bin/nom_alea.o

test_terminal.o: ./test/test_terminal.c
	gcc -o ./bin/test_terminal.o -c ./test/test_terminal.c -Wall


#---------------#
# TEST LDV -----#
#---------------#

test_lignedevue: test_lignedevue.o lignedevue.o personnage.o map.o tank.o epeiste.o healer.o assassin.o mage.o archer.o liste_positions.o fonctions_deplacements.o attaque.o dictionnaire_attaques.o malus.o liste_malus.o equipe.o race.o nom_alea.o
	gcc -o ./bin/test_lignedevue ./bin/lignedevue.o ./bin/test_lignedevue.o ./bin/personnage.o ./bin/map.o ./bin/tank.o ./bin/healer.o ./bin/epeiste.o ./bin/assassin.o ./bin/mage.o ./bin/archer.o ./bin/liste_positions.o ./bin/fonctions_deplacements.o ./bin/dictionnaire_attaques.o ./bin/attaque.o ./bin/malus.o ./bin/liste_malus.o ./bin/equipe.o ./bin/race.o ./bin/nom_alea.o

test_lignedevue.o: ./test/test_lignedevue.c
	gcc -o ./bin/test_lignedevue.o -c ./test/test_lignedevue.c -Wall

lignedevue.o: ./src/lignedevue.c ./include/lignedevue.h
	gcc -o ./bin/lignedevue.o -c ./src/lignedevue.c -Wall



#
#Le test de archers
#

test_Archer: test_archer

test_archer: archer.o test_archer.o race.o fonction_attaques.o liste_malus.o malus.o dictionnaire_attaques.o personnage.o tank.o epeiste.o mage.o healer.o assassin.o equipe.o nom_alea.o fonctions_deplacements.o liste_positions.o lignedevue.o
	gcc -o ./bin/test_archer ./bin/archer.o ./bin/test_archer.o ./bin/race.o ./bin/fonction_attaques.o ./bin/liste_malus.o ./bin/malus.o ./bin/dictionnaire_attaques.o ./bin/personnage.o ./bin/equipe.o ./bin/mage.o ./bin/assassin.o ./bin/tank.o ./bin/healer.o ./bin/epeiste.o ./bin/nom_alea.o ./bin/fonctions_deplacements.o ./bin/liste_positions.o ./bin/lignedevue.o

archer.o: ./src/personnages/archer.c
	gcc -o ./bin/archer.o -c ./src/personnages/archer.c -Wall

test_archer.o: ./test/test_archer.c
	gcc -o ./bin/test_archer.o -c ./test/test_archer.c -Wall

race.o: ./src/personnages/race.c
	gcc -o ./bin/race.o -c ./src/personnages/race.c -Wall

fonction_attaques.o: ./src/fonction_attaques.c
	gcc -o ./bin/fonction_attaques.o -c ./src/fonction_attaques.c -Wall

personnage.o: ./src/personnages/personnage.c
	gcc -o ./bin/personnage.o -c ./src/personnages/personnage.c

nom_alea.o: ./src/personnages/nom_alea.c
	gcc -o ./bin/nom_alea.o -c ./src/personnages/nom_alea.c


#-------------------#
# TEST DES ATTAQUES #
#-------------------#

test_attaque: lignedevue.o personnage.o test_attaques.o map.o tank.o epeiste.o healer.o assassin.o mage.o archer.o liste_positions.o fonctions_deplacements.o attaque.o dictionnaire_attaques.o malus.o liste_malus.o equipe.o race.o nom_alea.o
	gcc -o ./bin/test_attaque ./bin/test_attaques.o ./bin/lignedevue.o ./bin/personnage.o ./bin/map.o ./bin/tank.o ./bin/healer.o ./bin/epeiste.o ./bin/assassin.o ./bin/mage.o ./bin/archer.o ./bin/liste_positions.o ./bin/fonctions_deplacements.o ./bin/dictionnaire_attaques.o ./bin/attaque.o ./bin/malus.o ./bin/liste_malus.o ./bin/equipe.o ./bin/race.o ./bin/nom_alea.o

attaque.o: ./src/fonction_attaques.c ./include/attaques.h
	gcc -o ./bin/attaque.o -c ./src/fonction_attaques.c -Wall

dictionnaire_attaques.o: ./src/dictionnaire_attaques.c ./include/attaques.h
	gcc -o ./bin/dictionnaire_attaques.o -c ./src/dictionnaire_attaques.c -Wall

test_attaques.o: ./test/test_attaques.c ./include/attaques.h
	gcc -o ./bin/test_attaques.o -c ./test/test_attaques.c -Wall


#-------------------#
# --TEST DES MAPS-- #
#-------------------#

test_map: map.o test_map.o fonctions_deplacements.o liste_positions.o
	gcc -o ./bin/test_map ./bin/map.o ./bin/test_map.o ./bin/fonctions_deplacements.o ./bin/liste_positions.o

map.o: ./src/map.c
	gcc -o ./bin/map.o -c ./src/map.c -Wall

test_map.o: ./test/test_map.c ./include/map.h
	gcc -o ./bin/test_map.o -c ./test/test_map.c -Wall


#-----------------------------------#
# --TEST DE LA LISTE DE POSITIONS-- #
#-----------------------------------#

test_liste_positions: liste_positions.o test_liste_positions.o
	gcc -o ./bin/test_liste_positions ./bin/liste_positions.o ./bin/test_liste_positions.o

liste_positions.o: ./src/liste_positions.c
	gcc -o ./bin/liste_positions.o -c ./src/liste_positions.c -Wall

test_liste_positions.o: ./test/test_liste_positions.c ./include/liste_positions.h
	gcc -o ./bin/test_liste_positions.o -c ./test/test_liste_positions.c -Wall

#---------------------------#
# --TEST DES DEPLACEMENTS-- #
#---------------------------#

test_deplacements: fonctions_deplacements.o test_deplacements.o map.o liste_positions.o
	gcc -o ./bin/test_deplacements ./bin/fonctions_deplacements.o ./bin/test_deplacements.o ./bin/map.o ./bin/liste_positions.o

fonctions_deplacements.o: ./src/fonctions_deplacements.c
	gcc -o ./bin/fonctions_deplacements.o -c ./src/fonctions_deplacements.c -Wall

test_deplacements.o: ./test/test_deplacements.c ./include/deplacements.h
	gcc -o ./bin/test_deplacements.o -c ./test/test_deplacements.c -Wall



#
#Test des combats
#



test_combat: test_Combat

test_Combat: combat.o test_combat.o competence.o archer.o
	gcc -o ./bin/test_combat ./bin/archer.o ./bin/competence.o ./bin/combat.o ./bin/test_combat.o

combat.o: ./src/combat.c
	gcc -o ./bin/combat.o -c ./src/combat.c -Wall

test_combat.o: ./test/test_combats.c ./include/combat.h
	gcc -o ./bin/test_combat.o -c ./test/test_combats.c -Wall

#
#Le test de assassin
#

test_Assassin: test_ass

test_ass: assassin.o test_assassin.o race.o fonction_attaques.o malus.o liste_malus.o dictionnaire_attaques.o personnage.o tank.o epeiste.o mage.o healer.o archer.o equipe.o nom_alea.o fonctions_deplacements.o liste_positions.o lignedevue.o
	gcc -o ./bin/test_assassin ./bin/assassin.o ./bin/test_assassin.o ./bin/race.o ./bin/fonction_attaques.o ./bin/malus.o ./bin/liste_malus.o ./bin/dictionnaire_attaques.o ./bin/personnage.o ./bin/equipe.o ./bin/mage.o ./bin/archer.o ./bin/tank.o ./bin/healer.o ./bin/epeiste.o ./bin/nom_alea.o ./bin/fonctions_deplacements.o ./bin/liste_positions.o ./bin/lignedevue.o

assassin.o: ./src/personnages/assassin.c
	gcc -o ./bin/assassin.o -c ./src/personnages/assassin.c -Wall

test_assassin.o: ./test/test_assassin.c
	gcc -o ./bin/test_assassin.o -c ./test/test_assassin.c -Wall



#
#Le test de epeiste
#

test_Epeiste: test_epeiste

test_epeiste: epeiste.o test_epeiste.o race.o fonction_attaques.o malus.o liste_malus.o dictionnaire_attaques.o personnage.o tank.o archer.o mage.o healer.o assassin.o equipe.o nom_alea.o fonctions_deplacements.o liste_positions.o lignedevue.o
	gcc -o ./bin/test_epeiste ./bin/epeiste.o ./bin/test_epeiste.o ./bin/race.o ./bin/fonction_attaques.o ./bin/malus.o ./bin/liste_malus.o ./bin/dictionnaire_attaques.o ./bin/personnage.o ./bin/equipe.o ./bin/mage.o ./bin/assassin.o ./bin/tank.o ./bin/healer.o ./bin/archer.o ./bin/nom_alea.o ./bin/fonctions_deplacements.o ./bin/liste_positions.o ./bin/lignedevue.o

epeiste.o: ./src/personnages/epeiste.c ./include/epeiste.h
	gcc -o ./bin/epeiste.o -c ./src/personnages/epeiste.c -Wall

test_epeiste.o: ./test/test_epeiste.c ./include/epeiste.h
	gcc -o ./bin/test_epeiste.o -c ./test/test_epeiste.c -Wall


#
#Le test de heal
#

test_Heal: test_heal

test_heal: healer.o test_healer.o race.o fonction_attaques.o malus.o liste_malus.o dictionnaire_attaques.o personnage.o tank.o epeiste.o mage.o archer.o assassin.o equipe.o nom_alea.o fonctions_deplacements.o liste_positions.o lignedevue.o
	gcc -o ./bin/test_healer ./bin/healer.o ./bin/test_healer.o ./bin/race.o ./bin/fonction_attaques.o ./bin/malus.o ./bin/liste_malus.o ./bin/dictionnaire_attaques.o ./bin/personnage.o ./bin/equipe.o ./bin/mage.o ./bin/assassin.o ./bin/tank.o ./bin/archer.o ./bin/epeiste.o ./bin/nom_alea.o ./bin/fonctions_deplacements.o ./bin/liste_positions.o ./bin/lignedevue.o

healer.o: ./src/personnages/healer.c ./include/healer.h
	gcc -o ./bin/healer.o -c ./src/personnages/healer.c -Wall

test_healer.o: ./test/test_healer.c ./include/healer.h
	gcc -o ./bin/test_healer.o -c ./test/test_healer.c -Wall



#
#Le test de mage
#

test_Mage: test_mage

test_mage: mage.o test_mage.o race.o fonction_attaques.o malus.o liste_malus.o dictionnaire_attaques.o personnage.o tank.o epeiste.o archer.o healer.o assassin.o equipe.o nom_alea.o fonctions_deplacements.o liste_positions.o lignedevue.o
	gcc -o ./bin/test_mage ./bin/mage.o ./bin/test_mage.o ./bin/race.o ./bin/fonction_attaques.o ./bin/malus.o ./bin/liste_malus.o ./bin/dictionnaire_attaques.o ./bin/personnage.o ./bin/equipe.o ./bin/archer.o ./bin/assassin.o ./bin/tank.o ./bin/healer.o ./bin/epeiste.o ./bin/nom_alea.o ./bin/fonctions_deplacements.o ./bin/liste_positions.o ./bin/lignedevue.o

mage.o: ./src/personnages/mage.c ./include/mage.h
	gcc -o ./bin/mage.o -c ./src/personnages/mage.c -Wall

test_mage.o: ./test/test_mage.c ./include/mage.h
	gcc -o ./bin/test_mage.o -c ./test/test_mage.c -Wall




#
#Le test de Tanks
#

test_Tank: test_tank

test_tank: tank.o test_tank.o race.o fonction_attaques.o malus.o liste_malus.o dictionnaire_attaques.o personnage.o archer.o epeiste.o mage.o healer.o assassin.o equipe.o nom_alea.o fonctions_deplacements.o liste_positions.o lignedevue.o
	gcc -o ./bin/test_tank ./bin/tank.o ./bin/test_tank.o ./bin/race.o ./bin/fonction_attaques.o ./bin/malus.o ./bin/liste_malus.o ./bin/dictionnaire_attaques.o ./bin/personnage.o ./bin/equipe.o ./bin/mage.o ./bin/assassin.o ./bin/archer.o ./bin/healer.o ./bin/epeiste.o ./bin/nom_alea.o ./bin/fonctions_deplacements.o ./bin/liste_positions.o ./bin/lignedevue.o

tank.o: ./src/personnages/tank.c ./include/tank.h
	gcc -o ./bin/tank.o -c ./src/personnages/tank.c -Wall

test_tank.o: ./test/test_tank.c ./include/tank.h
	gcc -o ./bin/test_tank.o -c ./test/test_tank.c -Wall



#Chargement des competences

competence.o: ./src/personnages/competence.c ./include/competence.h
	gcc -o ./bin/competence.o -c ./src/personnages/competence.c -Wall


#
#Test des malus
#

test_Malus: test_malus

test_malus: archer.o malus.o liste_malus.o test_liste_malus.o attaque.o dictionnaire_attaques.o personnage.o tank.o epeiste.o mage.o healer.o assassin.o equipe.o nom_alea.o fonctions_deplacements.o liste_positions.o fonctions_deplacements.o liste_positions.o lignedevue.o
	gcc -o ./bin/test_liste_malus ./bin/archer.o ./bin/malus.o ./bin/liste_malus.o ./bin/test_liste_malus.o ./bin/race.o ./bin/attaque.o ./bin/dictionnaire_attaques.o ./bin/personnage.o ./bin/equipe.o ./bin/mage.o ./bin/assassin.o ./bin/tank.o ./bin/healer.o ./bin/epeiste.o ./bin/nom_alea.o ./bin/fonctions_deplacements.o ./bin/liste_positions.o ./bin/lignedevue.o

liste_malus.o: ./src/liste_malus.c
	gcc -o ./bin/liste_malus.o -c ./src/liste_malus.c -Wall

malus.o: ./src/malus.c
	gcc -o ./bin/malus.o -c ./src/malus.c -Wall

test_liste_malus.o: ./test/test_liste_malus.c
	gcc -o ./bin/test_liste_malus.o -c ./test/test_liste_malus.c -Wall

#
#Le test des equipes
#

test_Equipe: test_equipe

test_equipe: archer2.o assassin2.o healer2.o tank2.o mage2.o epeiste2.o equipe.o test_equipe.o race.o fonction_attaques.o malus.o liste_malus.o dictionnaire_attaques.o personnage.o nom_alea.o fonctions_deplacements.o liste_positions.o lignedevue.o
	gcc -g -o ./bin/test_equipe ./bin/equipe.o ./bin/archer.o ./bin/epeiste.o ./bin/healer.o ./bin/tank.o ./bin/assassin.o ./bin/mage.o ./bin/test_equipe.o ./bin/race.o ./bin/fonction_attaques.o ./bin/malus.o ./bin/liste_malus.o ./bin/dictionnaire_attaques.o ./bin/personnage.o ./bin/nom_alea.o ./bin/fonctions_deplacements.o ./bin/liste_positions.o ./bin/lignedevue.o
#On inclue les personnages

archer2.o: ./src/personnages/archer.c
	gcc -o ./bin/archer.o -c ./src/personnages/archer.c -Wall

tank2.o: ./src/personnages/tank.c ./include/tank.h
	gcc -o ./bin/tank.o -c ./src/personnages/tank.c -Wall

mage2.o: ./src/personnages/mage.c ./include/mage.h
	gcc -o ./bin/mage.o -c ./src/personnages/mage.c -Wall

healer2.o: ./src/personnages/healer.c ./include/healer.h
	gcc -o ./bin/healer.o -c ./src/personnages/healer.c -Wall

epeiste2.o: ./src/personnages/epeiste.c ./include/epeiste.h
	gcc -o ./bin/epeiste.o -c ./src/personnages/epeiste.c -Wall

assassin2.o: ./src/personnages/assassin.c ./include/assassin.h
	gcc -o ./bin/assassin.o -c ./src/personnages/assassin.c -Wall

#FIn de l'inclusion des personnages
#On compile la gestion des équipes

equipe.o: ./src/equipe.c ./include/equipe.h ./include/commun.h
	gcc -o ./bin/equipe.o -c ./src/equipe.c -Wall

test_equipe.o: ./test/test_equipe.c ./include/equipe.h ./include/commun.h ./include/archer.h
	gcc -o ./bin/test_equipe.o -c ./test/test_equipe.c -Wall



test_equipe2: test_archer test_ass test_tank test_heal test_epeiste test_Mage equipe.o test_equipe.o race.o dictionnaire_attaques.o attaque.o malus.o liste_malus.o personnage.o fonctions_deplacements.o liste_positions.o
	gcc -o ./bin/test_equipe ./bin/equipe.o ./bin/archer.o ./bin/epeiste.o ./bin/healer.o ./bin/tank.o ./bin/assassin.o ./bin/mage.o ./bin/test_equipe.o ./bin/attaque.o ./bin/race.o ./bin/dictionnaire_attaques.o ./bin/malus.o ./bin/liste_malus.o ./bin/personnage.o ./bin/nom_alea.o ./bin/fonctions_deplacements.o ./bin/liste_positions.o ./bin/lignedevue.o


selection: ./include/commun.h src/selection.c
	gcc -o bin/selection src/selection.c -I include -L lib -lmingw32 -lSDL2main -lSDL2 -lSDL2_image

selection.o: ./src/selection.c ./include/selection.h
	gcc -o ./bin/selection.o -c ./src/selection.c

#
#Section SDL
#

#Compilation Menu

menu: src/menu.c
	gcc src/menu.c -o bin/menu -I include -L lib -lmingw32 -lSDL2main -lSDL2 -lSDL2_image

menu.o: src/menu.c ./include/menu.h
	gcc -o ./bin/menu.o -c ./src/menu.c


#Compilation des fichiers de la map 2D

map2D.o: ./src/map2D.c ./include/map2D.h
	gcc -o ./bin/map2D.o -c ./src/map2D.c -Wall

map2D: map.o equipe.o assassin.o tank.o mage.o healer.o archer.o epeiste.o race.o attaque.o liste_malus.o malus.o dictionnaire_attaques.o liste_positions.o fonctions_deplacements.o src/map2D.c nom_alea.o
	    gcc src/map2D.c -o bin/map2D -I include -L lib -lmingw32 -lSDL2main -lSDL2 -lSDL2_image bin/map.o bin/equipe.o bin/assassin.o bin/tank.o bin/mage.o bin/healer.o bin/archer.o bin/epeiste.o bin/race.o bin/attaque.o bin/liste_malus.o bin/malus.o bin/dictionnaire_attaques.o bin/liste_positions.o bin/fonctions_deplacements.o ./bin/nom_alea.o


#
#Test du réseau
#

serveur: reseau.o test_serveur.o race.o liste_malus.o map.o equipe.o lignedevue.o personnage.o lignedevue.o
	gcc -o ./bin/reseau ./bin/reseau.o ./bin/test_serveur.o ./bin/archer.o ./bin/tank.o ./bin/assassin.o ./bin/healer.o ./bin/mage.o ./bin/epeiste.o ./bin/race.o ./bin/attaque.o ./bin/dictionnaire_attaques.o ./bin/liste_malus.o ./bin/malus.o ./bin/nom_alea.o ./bin/fonctions_deplacements.o ./bin/liste_positions.o	./bin/map.o ./bin/equipe.o ./bin/lignedevue.o ./bin/personnage.o ./bin/lignedevue.o -lws2_32

reseau.o: ./src/reseau.c
	gcc -o ./bin/reseau.o -c ./src/reseau.c

test_serveur.o: ./test/test_serveur.c
	gcc -o ./bin/test_serveur.o -c ./test/test_serveur.c -lws2_32

client: reseau.o test_client.o nom_alea.o map.o equipe.o lignedevue.o personnage.o lignedevue.o
	gcc -o ./bin/client ./bin/reseau.o ./bin/test_client.o ./bin/archer.o ./bin/tank.o ./bin/assassin.o ./bin/healer.o ./bin/mage.o ./bin/epeiste.o ./bin/race.o ./bin/attaque.o ./bin/dictionnaire_attaques.o ./bin/liste_malus.o ./bin/malus.o ./bin/nom_alea.o ./bin/fonctions_deplacements.o ./bin/liste_positions.o ./bin/map.o ./bin/equipe.o ./bin/lignedevue.o ./bin/personnage.o ./bin/lignedevue.o -lws2_32

test_client.o: ./test/test_client.c
	gcc -o ./bin/test_client.o -c ./test/test_client.c -lws2_32

test_reseau: serveur client
