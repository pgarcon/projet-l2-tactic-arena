var searchData=
[
  ['placer_5fequipes_1994',['placer_equipes',['../map_8h.html#a0cad72c66c23b13c243b730940f37e3e',1,'placer_equipes(liste_perso_t *equipe_rouge, liste_perso_t *equipe_bleue):&#160;map.c'],['../map_8c.html#a0cad72c66c23b13c243b730940f37e3e',1,'placer_equipes(liste_perso_t *equipe_rouge, liste_perso_t *equipe_bleue):&#160;map.c']]],
  ['premiere_5fposition_5fequipe_5fadverse_1995',['premiere_position_equipe_adverse',['../map_8h.html#abe6b52dfaa553caad40493d26b5db9d7',1,'premiere_position_equipe_adverse(t_pos position, equipe_t couleur):&#160;map.c'],['../map_8c.html#abe6b52dfaa553caad40493d26b5db9d7',1,'premiere_position_equipe_adverse(t_pos position, equipe_t couleur):&#160;map.c']]],
  ['pressenter_1996',['PressEnter',['../selection_8h.html#acb20f43a5620dd3601fcf15a23f78421',1,'PressEnter(char selection, SDL_Surface *choose):&#160;selection.c'],['../selection_8c.html#acb20f43a5620dd3601fcf15a23f78421',1,'PressEnter(char selection, SDL_Surface *choose):&#160;selection.c']]],
  ['proposer_5fdeplacement_5fterminal_1997',['proposer_deplacement_terminal',['../deplacements_8h.html#a096a03a2557a8589a110b85a7d01c7f9',1,'proposer_deplacement_terminal(personnage_t *joueur):&#160;fonctions_deplacements.c'],['../fonctions__deplacements_8c.html#a096a03a2557a8589a110b85a7d01c7f9',1,'proposer_deplacement_terminal(personnage_t *joueur):&#160;fonctions_deplacements.c']]]
];
