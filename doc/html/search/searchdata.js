var indexSectionsWithContent =
{
  0: "abcdefghijklmnopqrstuvwxy",
  1: "acdehilmprstv",
  2: "abcdefghlmnprst",
  3: "abcdefgilmprstvx",
  4: "abcdefhijklmnopqrstuvwxy",
  5: "s",
  6: "cdestw",
  7: "s",
  8: "amnprsv",
  9: "dp"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines",
  9: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros",
  9: "Pages"
};

