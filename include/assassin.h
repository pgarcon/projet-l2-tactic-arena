/**
  \file assassin.h
  \brief Les prototypes de la classe assassin
  \details ce fichier contient la structure de la classe assassin et les prototypes de fonctions :
  - existe_assassin
  - creer_assassin
  \author Pierre garcon
  \date 06 fevrier
  \fn int existe_assassin, assassin_t creer_assassin
*/



#ifndef H_ASSASSIN
#define H_ASSASSIN

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "personnage.h"
#include "nom_alea.h"


/*
  variables globales
*/


/*
Défnition de la strucure assassin
*/

/**
 * \struct assassin_t
 * \brief Structure définissant un assassin
 */
typedef struct assassin_s assassin_t;

struct assassin_s{
    #include "caract_perso.h"
};

/*
Fonctions
*/

extern
int existe_assassin(assassin_t * ass);

extern
assassin_t * creer_assassin(equipe_t equipe, race_t * race, liste_perso_t * liste);


#endif
