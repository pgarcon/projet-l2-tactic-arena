
#ifndef _ATTAQUES_H_
#define _ATTAQUES_H_

#include <stdio.h>
#include "personnage.h"
#include <math.h>
/**
 * \file attaques.h
 * \brief Définition des structures de données et prototypes des fonctions liées aux attaques des personnages
 * \author Yohann Delacroix
 * \version 1.0
 * \date 18 Février 2021
 */

#define NB_CASES_TABLEAU_CIBLES 15

/**
 * \enum etat_attaque
 * \brief Résultat de l'attaque après l'avoir lancé
 * Reussite : L'attaque a été portée avec succès
 * Echec : L'attaque a echouée
 * SansCible : L'attaque a été portée sur une case vide
 */
typedef enum {Reussite, Echec, SansCible} etat_attaque;


/**
 * \enum type_attaque
 * \brief Détermine si l'attaque ocassione des dégâts ou des soins 
 * Attaque : Occasione des dégâts
 * Soin : Prodigue des soins
 * NoDamage : Ajoute seulement des effets
 */
typedef enum {Attaque, Soin, NoDamage} type_attaque;


/**
 * \struct statut_att commun.h
 * \brief Contient les informations de retour à l'issue d'une attaque portée
 * Contient un type etat_attaque
 * Contient la listes des cibles atteintes, les types de dégâts et sa valeur qui leurs sont occasionnés, les esquives, l'état.
 * Chaine de caracteres contenant la description de l'effet et le nom de l'attaque
 * Le lanceur de l'attaque et les éventuels soins ou dégâts occasionnés sur lui même
 */
struct statut_att_s
{
	etat_attaque etat;

	int nombre_cibles;

	personnage_t * cibles[NB_CASES_TABLEAU_CIBLES];
	type_attaque type[NB_CASES_TABLEAU_CIBLES];
	int degats[NB_CASES_TABLEAU_CIBLES];
	int attaque_esquivee[NB_CASES_TABLEAU_CIBLES]; //Tableau de Booléen
	etat_t etat_cible[NB_CASES_TABLEAU_CIBLES];

	char * description_effet;
	char * nom_attaque;

	personnage_t * lanceur;
	int soins_sur_lanceur;
	int degats_sur_lanceur;
};


/**
 * \struct info_att commun.h
 * \brief Contient les informations de bases d'une attaque
 * Portée de l'attaque
 * Taille de la zone d'effet, si elle vaut 0, la zone est monocible
 * Le type d'attaque et ses éventuels dégâts minimum et maximum
 * Le nom de l'attaque et sa description
 */
struct info_att_s
{
	int portee;
	int zone;
	int degats_min;
	int degats_max;
	type_attaque type;
	char * nom;
	char * description;
};


//Fonctions d'attaques liées aux Personnages
//ARCHERS
statut_att tirdelaigle(personnage_t * lanceur, t_pos case_cible);
statut_att tirdecombat(personnage_t * lanceur, t_pos case_cible);
statut_att barrageabsolu(personnage_t * lanceur, t_pos case_cible);
//ASSASSINS
statut_att coupdeDague(personnage_t * lanceur, t_pos case_cible);
statut_att lancerdedagues(personnage_t * lanceur, t_pos case_cible);
statut_att beniparlesdieuxdusang(personnage_t * lanceur, t_pos case_cible);
//MAGES
statut_att firaball(personnage_t * lanceur, t_pos case_cible);
statut_att fabriquantdeglacons(personnage_t * lanceur, t_pos case_cible);
statut_att interitumnotitia(personnage_t * lanceur, t_pos case_cible);
//HEALERS
statut_att benedictiondivine(personnage_t * lanceur, t_pos case_cible);
statut_att transfertdevitalite(personnage_t * lanceur, t_pos case_cible);
statut_att gracedeladeesse(personnage_t * lanceur, t_pos case_cible);
//TANKS
statut_att coupepeesimple(personnage_t * lanceur, t_pos case_cible);
statut_att coupdebouclierrecul(personnage_t * lanceur, t_pos case_cible);
statut_att volontedudefenseur(personnage_t * lanceur, t_pos case_cible);
//EPEISTES
statut_att voleedepee(personnage_t * lanceur, t_pos case_cible);
statut_att ragedudueliste(personnage_t * lanceur, t_pos case_cible);
statut_att dansedelamort(personnage_t * lanceur, t_pos case_cible);

//Fonction d'informations sur les attaques
info_att tirdelaigle_info();
info_att tirdecombat_info();
info_att barrageabsolu_info();
info_att coupdeDague_info();
info_att lancerdedagues_info();
info_att beniparlesdieuxdusang_info();
info_att firaball_info();
info_att fabriquantdeglacons_info();
info_att interitumnotitia_info();
info_att benedictiondivine_info();
info_att transfertdevitalite_info();
info_att gracedeladeesse_info();
info_att coupepeesimple_info();
info_att coupdebouclierrecul_info();
info_att volontedudefenseur_info();
info_att voleedepee_info();
info_att ragedudueliste_info();
info_att dansedelamort_info();

//Fonctions de calculs et applications de dommage
int tirage_jet_dommage(int min, int max);
float SystemePFC(personnage_t * perso1, personnage_t * perso2);
int echec_attaque();
int esquiver_attaque(personnage_t * cible);
int afficher_statut_attaque(statut_att statut_attaque);
personnage_t** recherche_cibles_zone(t_pos case_cible, int taille_zone);
int comparaison_position(int p1x, int p1y, int p2x, int p2y);
personnage_t** trouver_la_cible_la_plus_proche(t_pos case_cible, int taille_zone);
statut_att effectuer_attaque(personnage_t * lanceur, t_pos case_cible, char * nom_attaque, int dommage_minimum, int dommage_maximum, int zone , type_attaque type);
void afficher_description_attaque(info_att (*attaque)());

//Validité des attaques
liste_pos_t* attaque_valide(personnage_t * joueur, int portee_attaque);
int controler_coordonnees_attaque(t_pos case_cible, liste_pos_t * liste_de_positions);
void ajouter_marqueurs_attaque_carte(liste_pos_t * liste_positions);
void retirer_marqueurs_attaque_carte(liste_pos_t * liste_positions);
int compter_cases_valides_attaque(int taille_zone, t_pos case_cible);
int compter_cases_obstacles_attaque(int taille_zone, t_pos case_cible);
int cible_corps_a_corps(t_pos joueur, t_pos cible);

#endif
