/**
  \file caract_perso.h
  \brief Les caractèristiques des personnages
  \details ce fichier contient tous les composants des personnages
  \author Pierre garcon
  \date 06 fevrier
*/


void (*print)(personnage_t * const);
void (*deplacer)(int , int , personnage_t * const);
void(*tourner_droite)(personnage_t *);
void(*tourner_gauche)(personnage_t *);
void(*tourner_haut)(personnage_t *);
void(*tourner_bas)(personnage_t *);
statut_att(*competence1)(personnage_t *, t_pos);
statut_att(*competence2)(personnage_t *, t_pos);
statut_att(*competence3)(personnage_t *, t_pos);

info_att(*info_comp1)();
info_att(*info_comp2)();
info_att (*info_comp3)();


t_pos (*position_pers)(personnage_t *);
etat_t (*f_subir_degats)(int, personnage_t * const);
err_t (*detruire)(personnage_t ** const);
int (*f_pt_deplacement) (personnage_t *);
int (*f_pt_attaque) (personnage_t *);
etat_t(*f_etat_pers)(personnage_t *);
int(*f_vie)(personnage_t *);
float(*f_esquive_parade)(personnage_t *);
int(*f_portee_attaque)(personnage_t *);
int(*f_portee_deplacement)(personnage_t *);
equipe_t(*f_equipe)(personnage_t *);
float(*f_precision)(personnage_t *);
bool(*f_comp_spe)(personnage_t *);




liste_perso_t * liste;
class_t classe;
char nom[60];
int pv;
int pv_max;
int degat;
portee_t portee;
float esquive;
float crit;
int resistance;
float precision;
int pos_x;
int pos_y;
float parade;
etat_t etat;
race_t * race;
equipe_t equipe;
int pt_deplacement;
int pt_attaque;
bool comp_special;
direction_t direction;
type_race_t type_race;
