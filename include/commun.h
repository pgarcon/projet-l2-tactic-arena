/**
  \file commun.h
  \brief Les variable utilisé en commun
  \details Ce fichier contient les variables :
  - class_t
  - personnage_t
  - type_race_t
  - err_t
  - equipe_t
  - etat_t
  - portee_t
  - pos_t
  - type_malus_t
  - t_temps
  - statut_att
  - liste_perso_t
  - info_att_t
  - liste_pos_t
  \author Pierre garcon, Yohann Delacroix
  \date 06 fevrier
  \fn int existe_archer, archer_t creer_archer
*/

#ifndef H_COMMUN
#define H_COMMUN

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define NB_COMP 3


/*
  type classe
*/
/**
 * \enum class_t
 * \brief Les différents types de classes
 * - archer
 * - mage
 * - tank
 * - assassin
 * - healer
 * - epeiste
 *
 */
typedef enum {archer, mage, tank, assassin, healer, epeiste} class_t;

/*
  type race
*/
/**
 * \enum type_race_t
 * \brief Les différents types de races
 * - nain
 * - kobold
 * - demon
 * - humain
 * - orc
 * - elfe
 *
 */
typedef enum {nain, kobolt, demon, humain, orc, elfe} type_race_t;

/*
  type err
*/
/**
 * \enum erreur_t
 * \brief Les erreurs
 * - Ok
 * - Erreur
 *
 */
typedef enum {Ok, Erreur} err_t;

/*
  type Equipe
*/
/**
 * \enum equipe_t
 * \brief Les différents equipe
 * - Bleu
 * - Rouge
 *
 */
typedef enum {Rouge, Bleu} equipe_t;

/*
  Etat d'un personnage
*/
/**
 * \enum etat_t
 * \brief Les différents etats d'un personnage
 * - Mort
 * - Vivant
 * - Rien
 *
 */
typedef enum {Mort, Vivant, Rien} etat_t;

/*
  Structure portee
*/
/**
 * \struct portee_t
 * \brief Structure qui défini la portee de deplacement et d'attaque
 * - deplacement
 * - attaque
 */
typedef struct portee_s{
  int deplacement;
  int attaques;
}portee_t;


//Position sur la carte
/**
 * \struct t_pos
 * \brief Les position sur la carte
 * - x
 * - y
 *
 */
typedef struct t_pos_s t_pos;
struct t_pos_s
{
	int x;
	int y;
};



/*Type de malus*/

/**
 * \enum type_malus_t
 * \brief Les différents types de malus/bonus
 * - bouclier
 * - degats
 * - poison
 * - deplacement
 *
 */
typedef enum {bouclier, degats, poison, deplacement} type_malus_t;


/*Le cycle jour/nuit*/
/**
 * \enum t_temps
 * \brief Les deux temps possibles
 * - jour
 * - nuit
 *
 */
typedef enum {jour, nuit} t_temps;

typedef struct personnage_s personnage_t ;
typedef struct statut_att_s statut_att;
typedef struct liste_perso_s  liste_perso_t;
typedef struct info_att_s info_att;
typedef struct liste_pos_s liste_pos_t;

#endif
