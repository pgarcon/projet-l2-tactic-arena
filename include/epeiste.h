/**
  \file epeiste.h
  \brief Les prototypes de la classe epeiste
  \details ce fichier contient la structure de la classe epeiste et les prototypes des fonctions :
  - existe_epeiste
  - creer_epeiste

  \author Pierre garcon
  \date 06 fevrier
  \fn int existe_epeiste, epeiste_t creer_epeiste
*/



#ifndef H_epeiste
#define H_epeiste

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "personnage.h"
#include "nom_alea.h"


/*
  variables globales
*/


/*
Défnition de la strucure epeiste
*/
/**
 * \struct epeiste_t
 * \brief Structure définissant un épeiste
 */
typedef struct epeiste_s epeiste_t;

struct epeiste_s{
    #include "caract_perso.h"
};

/*
Fonctions
*/

extern
int existe_epeiste(epeiste_t * epe);

extern
epeiste_t * creer_epeiste(equipe_t equipe, race_t * race, liste_perso_t * liste);


#endif
