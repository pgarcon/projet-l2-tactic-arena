/**
  \file equipe.h
  \brief Les prototypes des fonctions
  \details ce fichier contient les prototypes des fonctions :
  - liste_perso_creer
  - liste_existe
  - liste_pleine
  - liste_decrementer_nb_vivant
  - liste_incrementer_nb_vivant
  - liste_vide
  - liste_nb
  - liste_perso_maj
  - liste_perso_equipe_creer
  - liste_detruire
  - liste_recuperer_pers
  - liste_ajouter
  - liste_enlever
  - liste_vider
  - liste_ajouter_perso_equipe
  - liste_afficher
  - liste_perso_nb_personnage_vivant

  \author Pierre garcon
  \date 06 fevrier
  \fn int existe_epeiste, epeiste_t creer_epeiste
*/


#ifndef _EQUIPE_H_
#define _EQUIPE_H_

#include "equipe_sdd.h"
#include "archer.h"
#include "epeiste.h"
#include "healer.h"
#include "mage.h"
#include "tank.h"
#include "assassin.h"
#include "race.h"

#define NB_PERS 5


/*!
 * Creation d'une liste
 */
extern
liste_perso_t * liste_perso_creer( const int nb);


/*
fonctions externes
*/

extern
bool liste_existe( liste_perso_t * const liste );

extern
bool liste_pleine(liste_perso_t * liste);

extern
void liste_decrementer_nb_vivant(liste_perso_t * liste);

extern
void liste_incrementer_nb_vivant(liste_perso_t * liste);

extern
bool liste_vide(liste_perso_t * liste);

extern
int liste_nb(liste_perso_t * list);

extern
void liste_perso_maj(liste_perso_t * equipe1, liste_perso_t * equipe2, t_temps temps);

extern
void liste_perso_equipe_creer(liste_perso_t * equipe1, equipe_t equipe);

extern
err_t liste_detruire( liste_perso_t ** liste );

extern
personnage_t * liste_recuperer_pers(liste_perso_t * liste, int indice);

extern
void liste_ajouter(liste_perso_t * liste, personnage_t * pers);

extern
void liste_enlever(liste_perso_t * liste);

extern
void liste_vider(liste_perso_t * liste);

extern
int liste_ajouter_perso_equipe(class_t classe, type_race_t race, liste_perso_t * equipe, equipe_t team, int * nb_arc, int * nb_ass, int * nb_heal, int * nb_tank, int * nb_epe, int * nb_mage);

extern
void liste_afficher(liste_perso_t * liste);

extern
void liste_perso_nb_personnage_vivant(liste_perso_t * liste);

#endif
