/**
  \file equipe_sdd.h
  \brief Le structure de données des equipes
  \details ce fichier contient la structure des equipes
  \author Pierre garcon
  \date 06 fevrier
  \fn int existe_epeiste, epeiste_t creer_epeiste
*/


#ifndef _EQUIPE_SDD_H_
#define _EQUIPE_SDD_H_

#include "personnage.h"




/**
 * \struct liste_perso_t
 * \brief Structure d'une liste de personnage
 * - nb
 * - nb_pers
 * - liste
 * - nb_vivant
 *
 */
struct liste_perso_s
{
  int nb ;		/* Nombre d'objets dans la liste  */
  int nb_pers; /*Savoir le nombre de perso qu'il y a dans la liste*/
  personnage_t ** liste ;	/* liste  des objets */

  int nb_vivants;
} ;

#endif
