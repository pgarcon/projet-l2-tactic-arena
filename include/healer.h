
/**
  \file healer.h
  \brief Les prototypes de la classe healer
  \details ce fichier contient la structure de la classe healer et les prototypes suivants :
  - existe_healer
  - creer_healer

  \author Pierre garcon
  \date 06 fevrier
  \fn int existe_healer, healer_t creer_healer
*/



#ifndef H_healer
#define H_healer

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "personnage.h"
#include "nom_alea.h"

/*
  variables globales
*/


/*
Défnition de la strucure healer
*/

/**
 * \struct healer_t
 * \brief Structure définissant un healer
 */
typedef struct healer_s healer_t;

struct healer_s{
    #include "caract_perso.h"
};

/*
Fonctions
*/

extern
int existe_healer(healer_t * heal);

extern
healer_t * creer_healer(equipe_t equipe, race_t * race, liste_perso_t * liste);


#endif
