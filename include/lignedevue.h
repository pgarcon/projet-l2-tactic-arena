#ifndef H_LDV
#define H_LDV

#include "../include/commun.h"
#include "../include/personnage.h"
#include "../include/map.h"
#include "../include/lignedevue.h"
#include "../include/liste_positions.h"
#include "../include/attaques.h"
/**
 * \file lignedevue.h
 * \brief Définition des structures de données et prototypes des fonctions liées aux lignes de vue
 * \author Yohann Delacroix
 * \version 1.0
 * \date 15 Avril 2021
 */

/**
 * \struct vecteur_t
 * \brief Représente un vecteur mathématique
 */
typedef struct vecteur_s
{
	float x;
	float y;
}vecteur_t;


/**
 * \struct coor_t
 * \brief Représente une coordonnée réelle
 */
typedef struct coor
{
	float x;
	float y;
}coor_t;


/**
 * \struct segment_t
 * \brief Représente un segment de coordonnées réelles dans un plan
 */
typedef struct segment_s
{
	coor_t pointA;
	coor_t pointB;
}segment_t;


//Prototypes des fonctions
int round_left(float x);
int round_right(float x);
int round_value(float x);
float affine(float coeff, float constante, float x);
float x_affine(float coeff, float constante, float y);
liste_pos_t * ligne_de_vue(t_pos perso, t_pos mur, int portee_max);

#endif
