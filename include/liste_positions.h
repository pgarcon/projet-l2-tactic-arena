/*Définition de la structure liste de malus*/

#ifndef H_LISTE_POSITIONS
#define H_LISTE_POSITIONS

#include "deplacements.h"
#include "commun.h"

/**
 * \file liste_positions.h
 * \brief Définition des structures de données et prototypes des fonctions liées aux listes de positions
 * \author Yohann Delacroix
 * \version 1.0
 * \date 22 Mars 2021
 */

/**
 * \struct liste_pos_t commun.h
 * \brief Liste de positions
 */
struct liste_pos_s
{
	t_pos * positions;
	int nb_elem;
};

//Fonctions de gestion de la liste de positions
extern
liste_pos_t * liste_positions_initialiser(int nb_elements);
extern
err_t liste_positions_detruire( liste_pos_t ** liste );
extern
err_t liste_positions_ajouter( liste_pos_t * liste, t_pos position, int indice );
extern
t_pos liste_position_lire(liste_pos_t * liste_positions, int indice);
extern
void afficher_liste_positions_terminal(liste_pos_t * liste_positions);
int detecter_doublon(t_pos position, liste_pos_t* liste_positions);
liste_pos_t* concatener_listes_positions(liste_pos_t * liste_1, liste_pos_t * liste_2);
int taille_liste(liste_pos_t * liste);
int liste_positions_existe(liste_pos_t * liste);
liste_pos_t* ajuster_taille_liste(liste_pos_t * liste_1, int taille_liste);
liste_pos_t * retirer_occurences_liste(liste_pos_t * liste_initiale, liste_pos_t * liste_occurences);
liste_pos_t * retirer_une_occurence_liste(liste_pos_t * liste_initiale, t_pos position);
#endif
