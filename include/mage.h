/**
  \file mage.h
  \brief Les prototypes de la classe mage
  \details ce fichier contient la structure de la classe mage et les prototypes suivants :
  - existe_mage
  - creer_mage
  \author Pierre garcon
  \date 06 fevrier
  \fn int existe_mage, mage_t creer_mage
*/



#ifndef H_mage
#define H_mage

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "personnage.h"
#include "nom_alea.h"


/*
  variables globales
*/


/*
Défnition de la strucure mage
*/
/**
 * \struct mage_t
 * \brief Structure définissant un mage
 */
typedef struct mage_s mage_t;

struct mage_s{
    #include "caract_perso.h"
};

/*
Fonctions
*/

extern
int existe_mage(mage_t * mage);

extern
mage_t * creer_mage(equipe_t equipe, race_t * race, liste_perso_t * liste);


#endif
