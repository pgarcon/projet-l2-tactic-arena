/**
  \file malus.h
  \brief Le prototype de Malus
  \details ce fichier contient la structure des malus et le prototypes suivant :
  - creer_malus
  \author Pierre garcon
  \date 06 fevrier
  \fn int existe_mage, mage_t creer_mage
*/

#ifndef _H_MALUS
#define _H_MALUS

/*Définition des malus*/

#include "personnage.h"

typedef struct malus_s malus_t;

/**
 * \struct malus_t
 * \brief Les différents types de malus/bonus
 * - type_malus
 * - duree
 * - cible
 * - malus
 *
 */
struct malus_s{
  type_malus_t type_mal;
  int duree;
  personnage_t * cible;
  int malus;

  err_t(*detruire)(malus_t **);
  int (*decrementer)(malus_t *);
  void (*print)(malus_t *);

};

extern
malus_t * creer_malus(type_malus_t type, int duree, personnage_t * cible, int malus);

#endif
