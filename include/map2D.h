/**
 * \file map2D.h
 * \brief Définition des fonctions liées à l'affichage de la carte en SDL
 * \author Nicolas
 * \version 0.8
 * \date 29 Mars 2021
 */

#ifndef _MAP2D_H_
#define _MAP2D_H_

# include <stdlib.h>
# include <stdio.h>
# include <time.h>

# include "../include/SDL/SDL.h"
# include  "../include/SDL/SDL_image.h"
# include "../include/SDL/SDL_ttf.h"


# include  "../include/map.h"
# include "../include/equipe.h"
# include "../include/race.h"

# include "../include/assassin.h"
# include "../include/archer.h"
# include "../include/epeiste.h"
# include "../include/healer.h"
# include "../include/mage.h"
# include "../include/tank.h"


#include "../include/attaques.h"
#include "../include/liste_malus.h"
#include "../include/malus.h"

#include "../include/deplacements.h"

# define TAILLE_BLOC 32 /** Taille d'un bloc (carré) en pixels */

int game();
void tirage_SDL(FILE  *carte);
void SDLErrorr(const char *message);
SDL_Surface * chargementImage(SDL_Surface * PtImage,char *chemin);
void displayOver(liste_pos_t *pos,t_pos sop, SDL_Surface *claie, SDL_Surface *screen, SDL_Rect position, SDL_Window *window);
personnage_t * select_pers_SDL(liste_perso_t * armee, equipe_t equipe, race_t * race2, int * nb_arc, int * nb_ass, int * nb_heal, int * nb_tank, int * nb_epe, int * nb_mage,int personnage);
void liste_perso_equipe_creer_SDL(liste_perso_t * armee1, equipe_t equipe,int selection,int personnage);

void displayMap(SDL_Surface *sol,SDL_Surface *mur, SDL_Surface *screen, SDL_Window *window, SDL_Rect position,
 SDL_Surface *ImageArcherRougeBas, SDL_Surface *ImageAssassinRougeBas, SDL_Surface *ImageEpeisteRougeBas, SDL_Surface *ImageHealerRougeBas, SDL_Surface *ImageMageRougeBas, SDL_Surface *ImageTankRougeBas,
 SDL_Surface *ImageArcherRougeGauche, SDL_Surface *ImageAssassinRougeGauche, SDL_Surface *ImageEpeisteRougeGauche, SDL_Surface *ImageHealerRougeGauche, SDL_Surface *ImageMageRougeGauche, SDL_Surface *ImageTankRougeGauche,
 SDL_Surface *ImageArcherRougeDroite, SDL_Surface *ImageAssassinRougeDroite, SDL_Surface *ImageEpeisteRougeDroite, SDL_Surface *ImageHealerRougeDroite, SDL_Surface *ImageMageRougeDroite, SDL_Surface *ImageTankRougeDroite,
 SDL_Surface *ImageArcherRougeHaut, SDL_Surface *ImageAssassinRougeHaut, SDL_Surface *ImageEpeisteRougeHaut, SDL_Surface *ImageHealerRougeHaut, SDL_Surface *ImageMageRougeHaut, SDL_Surface *ImageTankRougeHaut,
 SDL_Surface *ImageArcherBleuHaut, SDL_Surface *ImageAssassinBleuHaut, SDL_Surface *ImageEpeisteBleuHaut, SDL_Surface *ImageHealerBleuHaut, SDL_Surface *ImageMageBleuHaut, SDL_Surface *ImageTankBleuHaut,
 SDL_Surface *ImageArcherBleuBas, SDL_Surface *ImageAssassinBleuBas, SDL_Surface *ImageEpeisteBleuBas, SDL_Surface *ImageHealerBleuBas, SDL_Surface *ImageMageBleuBas, SDL_Surface *ImageTankBleuBas,
 SDL_Surface *ImageArcherBleuGauche, SDL_Surface *ImageAssassinBleuGauche, SDL_Surface *ImageEpeisteBleuGauche, SDL_Surface *ImageHealerBleuGauche, SDL_Surface *ImageMageBleuGauche, SDL_Surface *ImageTankBleuGauche,
 SDL_Surface *ImageArcherBleuDroite, SDL_Surface *ImageAssassinBleuDroite, SDL_Surface *ImageEpeisteBleuDroite, SDL_Surface *ImageHealerBleuDroite, SDL_Surface *ImageMageBleuDroite, SDL_Surface *ImageTankBleuDroite );

void displayZoneAttaque(SDL_Surface *purple, SDL_Surface *screen, SDL_Rect position, SDL_Window *window);
void displayZoneDeplacement(SDL_Surface *claie, SDL_Surface *screen, SDL_Rect position, SDL_Window *window);
int lire_liste_attaques(personnage_t * joueur, t_pos case_cible, int portee);
void displayInfoAtt(TTF_Font *font,SDL_Color colorFont,SDL_Surface *screen, SDL_Window *window, info_att info_comp);
#endif