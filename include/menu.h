#ifndef _MENU_H_
#define _MENU_H_

#include <stdio.h>
#include <stdlib.h>

#include "../include/SDL/SDL.h"
#include "../include/SDL/SDL_image.h"

#include "../include/selection.h"

#define WINDOW_WIDTH_MENU 460
#define WINDOW_HEIGHT_MENU 600


int menu();

#endif