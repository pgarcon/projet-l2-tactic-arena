/**
  \file nom_alea.h
  \brief Gestion des nom aléatoires
  \details Dans ce fichier, on peut trouver le prototype suivant :
  - donner_nom
  \author Pierre garcon
  \date 06 fevrier
*/

#ifndef H_NOM_ALEA
#define H_NOM_ALEA

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define NOM_FICHIER "../src/personnages/Fichiers/nom.txt"

extern
void donner_nom(char * nom);

#endif
