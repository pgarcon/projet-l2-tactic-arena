/**
  \file personnage.h
  \brief Structure de la classe personnage
  \details Dans ce fichier, on peut trouver la structure des personnage, elle va envelopper toutes les unités et le prototype suivant :
  - personnage_MAJ_equipe_mort
  \author Pierre garcon
  \date 06 fevrier
*/


#ifndef _OBJET_H_
#define _OBJET_H_

#include "race.h"
#include "attaques.h"
#include "commun.h"

/**
 * \enum direction_t
 * \brief Structure définissant la direction d'un personnage
 *
 * - droite
 * - gauche
 * - haut
 * - bas
 *
 */
/*Structure des races*/
typedef enum{droite, gauche, haut, bas}direction_t;


/*
 * DEFINITION STRUCTURE PERSONNAGE
 */

 /**
  * \struct race_t
  * \brief Structure définissant une race
  *
  * -liste
  * -classe
  * -nom
  * -pv
  * -pv_max
  * -degat
  * -portee
  * -esquive
  * -crit
  * -resistance
  * -precision
  * -pos_x
  * -pos_y
  * -parade
  * -etat
  * -race
  * -equipe
  * -pt_deplacement
  * -pt_attaque
  * -comp_special
  * -direction
  * -type_race
  *
  */
 /*Structure des races*/
struct personnage_s
{
#include "caract_perso.h"
};

extern
void personnage_MAJ_equipe_mort(personnage_t * pers);

#endif
