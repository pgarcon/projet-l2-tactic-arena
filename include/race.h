/**
* \file race.h
* \brief les prototypes des fonction de race
* \details Ce fichier contient la structure d'une race ainsi que les prototypes des fonctions suivantes :
* - race_creer_humain
* - race_creer_orc
* - race_creer_elfe
* - race_creer_nain
* - race_creer_demon
* - race_creer_kobolt
* - race_maj_equipe
* - race_activer
* \author Pierre GARCON
* \version 1.0
* \date 11 mars 2021
*/

#ifndef _H_RACE
#define _H_RACE


/*Définition des races*/
#include <stdio.h>
#include <stdlib.h>
#include "personnage.h"
#include "commun.h"
#include "equipe_sdd.h"


typedef struct race_s race_t;
/**
 * \struct race_t
 * \brief Structure définissant une race
 *
 * - Nom_race
 * - Bonus
 * - Malus
 * - Race
 *
 */
/*Structure des races*/
struct race_s
{
  char nom_race[50];
  int bonus;
  int malus;
  type_race_t race;

  err_t(*detruire)(race_t **);
  void(*race_activer_bonus)(void * pers);
  void(*race_desactiver_bonus)(void * pers);

};


extern
race_t * race_creer_humain();

extern
race_t * race_creer_orc();

extern
race_t * race_creer_elfe();

extern
race_t * race_creer_nain();

extern
race_t * race_creer_demon();

extern
race_t * race_creer_kobolt();

extern
void race_maj_equipe(liste_perso_t * liste, liste_perso_t * lsite2, t_temps temps);

extern
void race_activer(liste_perso_t * liste);

#endif
