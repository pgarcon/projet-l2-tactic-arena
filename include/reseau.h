/**
  \file reseau.h
  \brief Les en-tête des fonctions lié au réseau
  \details ce fichier contient les en-tete des fonctions :
  - reseau_demarrer
  - reseau_fermer
  - reseau_init_sin_serveur
  - reseau_init_sin_client
  - reseau_connection_serveur
  - reseau_connection_client
  - reseau_personnage_envoyer
  - reseau_personnage_receptionner
  - reseau_map_envoyer
  - reseau_map_recevoir
  - reseau_mettre_a_jour_carte
  - reseau_equipe_recevoir
  - reseau_equipe_envoyer
  - fonction_envoyer_carte : pour les threads
  - fonction_envoyer_equipe : pour les threads
  - reseau_coordonnees_envoye
  - reseau_coordonnees_recevoir
  - reseau_effectuer_tour
  - reseau_statut_attaque_envoyer
  - reseau_statut_attaque_recevoir
  - reseau_afficher_direction_perso
  - reseau_afficher_direction_persos
  - reseau_temps_envoyer
  - reseau_temps_recevoir
  - reseau_envoyer_variables
  - reseau_recevoir_variables
  - reseau_recuperer_tour
  - reseau_jouer_tour

  \author Pierre garcon
  \date 08 Mars
  \fn int existe_archer, archer_t creer_archer
*/

#ifndef _RESEAU_H_
#define _RESEAU_H_


#ifdef WIN32 /* si vous êtes sous Windows */

#include <winsock2.h>
#pragma comment(lib, "ws2_32.lib")

#elif defined (linux)

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h> /* close */
#include <netdb.h> /* gethostbyname */
#define INVALID_SOCKET -1
#define SOCKET_ERROR -1
#define closesocket(s) close(s)
typedef int SOCKET;
typedef struct sockaddr_in SOCKADDR_IN;
typedef struct sockaddr SOCKADDR;
typedef struct in_addr IN_ADDR;

#endif

/*Inclusion des fichiers standards*/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "commun.h"
#include "personnage.h"
#include "equipe.h"
#include "map.h"
#include "deplacements.h"
#include "liste_malus.h"
#include "malus.h"

#define PORT 30000

typedef int socklen_t;


/*Structure à envoyer au thread*/
typedef struct donnees_equipe_s{
  pthread_mutex_t mutex;
  liste_perso_t * equipe;
  int *arg;
}donnees_equipe_t;

/*Structure à envoyer au thread pour les equipe*/
typedef struct donnees_carte_s{
  pthread_mutex_t mutex;
  int *arg;
}donnees_carte_t;


extern
int reseau_demarrer(void);

extern
void reseau_fermer(void);

extern
SOCKADDR_IN reseau_init_sin_serveur(void);

extern
SOCKADDR_IN reseau_init_sin_client(char * adr_ip);

extern
SOCKET reseau_connection_serveur(SOCKET sock, SOCKADDR_IN * adr, socklen_t * size);

extern
int reseau_connection_client(SOCKET sock, SOCKADDR_IN * adr, int size);

extern
int reseau_personnage_envoyer(SOCKET csock, personnage_t * personnage);

extern
personnage_t * reseau_personnage_receptionner(SOCKET sock);

extern
int reseau_map_envoyer(SOCKET sock, t_carte map[N][M]);

extern
int reseau_map_recevoir(SOCKET sock, t_carte map[N][M]);

extern
void reseau_mettre_a_jour_carte(liste_perso_t * liste, t_carte map[N][M]);

extern
void reseau_equipe_recevoir(SOCKET socket, liste_perso_t * liste);

extern
void reseau_equipe_envoyer(SOCKET sock, liste_perso_t * liste);

extern
void * fonction_envoyer_carte(void * arg);

extern
void * fonction_envoyer_equipe(void * arg);

extern
void reseau_coordonnees_envoye(personnage_t * pers, SOCKET sock);

extern
void reseau_coordonnees_recevoir(int * x, int * y, SOCKET sock);

extern
void reseau_effectuer_tour(personnage_t * joueur, int * tour_pas_fini, SOCKET sock);

extern
void reseau_statut_attaque_envoyer(statut_att stat, SOCKET sock);

extern
statut_att reseau_statut_attaque_recevoir(SOCKET sock);

extern
void reseau_afficher_direction_perso(personnage_t * perso);

extern
void reseau_afficher_direction_persos(liste_perso_t * equipe_rouge, liste_perso_t * equipe_bleue);

extern
void reseau_temps_envoyer(t_temps temps, SOCKET sock);

extern
t_temps reseau_temps_recevoir(SOCKET sock);

extern
int reseau_envoyer_variables(int p_equipe, t_temps temps, SOCKET sock);

extern
void reseau_recevoir_variables(int * p_equipe, t_temps * temps, SOCKET sock);

extern
void reseau_recuperer_tour(liste_perso_t * equipe, int p_equipe, personnage_t * personnage_courant, SOCKET sock);

extern
void reseau_jouer_tour(liste_perso_t * equipe, int p_equipe, SOCKET sock);

extern
int reseau_actualiser_carte_perso_equipe(liste_perso_t * equipe, personnage_t * perso_courant, int p_equipe);

#endif
