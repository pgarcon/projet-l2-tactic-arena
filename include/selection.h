#ifndef _SELECTION_H_
#define _SELECTION_H_

#include <stdio.h>
#include <stdlib.h>

#include "../include/commun.h"

#include "../include/SDL/SDL_image.h"

#define WINDOW_WIDTH_SELECTION 800
#define WINDOW_HEIGHT_SELECTION 600
#define NB_PERS 5

typedef enum {Partage,Resaux} type_mode_t;

/**
 * \file selection.h
 * \struct  t_position
 * \brief   Coordonées de rectangle
 *
 * t_position permet de definir le point minimum de X et max de X puis de meme pour Y
 * De sorte à creer un rectangle
 *
 */
typedef struct
{
    int minX;   /*!< Entier minimum de X */
    int maxX;   /*!< Entier maximum de X */
    int minY;   /*!< Entier minimum de Y */
    int maxY;   /*!< Entier maximum de Y */
} t_position;

int selection(int mode);
void SDLError(const char *message);
void DisplaySelection(char choosen, SDL_Window *window, SDL_Surface *choose, SDL_Surface *screen, SDL_Rect posSelect);
void ReloadScreen(char selection, SDL_Window *window, SDL_Surface *background, SDL_Surface *screen);
char GoLeft(char selection, SDL_Window *window, SDL_Surface *background, SDL_Surface *screen );
char GoRight(char selection, SDL_Window *window, SDL_Surface *background, SDL_Surface *screen );
void PressEnter(char selection, SDL_Surface *choose);

#endif
