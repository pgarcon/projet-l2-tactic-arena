/**
  \file tank.h
  \brief Les prototypes de la classe tank
  \details ce fichier contient la structure de la classe tank et les prototypes suivants :
  - existe_tank
  - creer_tank
  \author Pierre garcon
  \date 06 fevrier
  \fn int existe_tank, tank_t creer_tank
*/



#ifndef H_tank
#define H_tank

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "personnage.h"
#include "nom_alea.h"


/*
  variables globales
*/


/*
Défnition de la strucure tank
*/
/**
 * \struct tank_t
 * \brief Structure définissant un tank
 */
typedef struct tank_s tank_t;

struct tank_s{
    #include "caract_perso.h"
};

/*
Fonctions
*/

extern
int existe_tank(tank_t * tank);

extern
tank_t * creer_tank(equipe_t equipe, race_t * race, liste_perso_t * liste);


#endif
