# Projet : Tactic Arena
Auteurs : Evan Rebours, Yoan Delacrois, Pierre Garçon, Nicolas



## Description du projet : 

Tactic Arena est un jeu de stratégie dans lequel deux équipes d'affrontent sur un plateau. Basé sur un système de combat de tour par tour, ce projet a pour but de remettre
à jour le jeu.


## Langage Utilisé :
language C

## Bibliothèque exterieur : 
SDL / SDL_image / SDL_TTF

Pthread

Socket

## Exécution du programme :
Version graphique
```
make
```
Version terminal
```
make test_terminal
```
Il y a également un exécutable (Windows et Linux) déjà prêt !

## Gantt : 
https://docs.google.com/spreadsheets/d/1YhStuRU7ZUN4Kdpo_f3OkOeA_g1TBVd48CtP0cZa9RY/edit#gid=1115838130





##### Cordialement.

L'équipe de développement.

## License
Tous droits réservé.

![Logo of the project](images/icon.png)
