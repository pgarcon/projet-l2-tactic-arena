/**
 * \file combat.c
 * \brief Fichier pour les combats
 * \author 
 * \version 1.0
 * \date 
 *
 * Divers fonction pour le combat
 *
 */


#include "../include/combat.h"

/*
  gestion des combats
*/


/*Fonction pour vérifier si un personnage peut attquer un autre*/
/**
 * \fn bool est_a_portee(personnage_t * att, personnage_t * vict, int num_competence)
 * \brief test si un perssonage ce trouve dans la distance d'un attaque
 * 
 * \param att perssonne qui lance l'attaque
 * \param vict perssonage sur lequel l'attaque est lancer
 * \param num_competence indice de la competence
 * 
 * \return bool true ou false
 * 
 */
extern
bool est_a_portee(personnage_t * att, personnage_t * vict, int num_competence)
{
  int portee, x_vict, y_vict, x_att, y_att;

  portee = att->competences[num_competence]->f_portee_competence(att->competences[num_competence]);
  vict->position(&x_vict, &y_vict, vict);

  att->position(&x_att, &y_att, att);

  /*je vérifie si la victime est à portée de coup*/
  if((x_vict - x_att > portee) && (y_vict - y_att > portee))
  {
    return false;
  }

  return true;


}

/*Fonction pour l'attque d'un personnage*/
/**
 * \fn void attaquer(personnage_t * attaquant, personnage_t * victime, int num_competence)
 * \brief fonction pour qu'un perssonage attaque
 * 
 * \param attaquant perssonnage qui lance l'attaque
 * \param victime perssonage sur lequel l'attaque est lancer
 * \param num_competence indice de la competence
 * 
 * \return void pas de retour
 * 
 */
extern
void attaquer(personnage_t * attaquant, personnage_t * victime, int num_competence)
{
  int degat_min, degat_max, /*zone,*/ esquive;
  etat_t etat;

  printf("\n\nDébut de l'attaque attention !\n");

  /*On initialise les variables de l'attaquant*/
  degat_min = attaquant->competences[num_competence]->f_degat_min(attaquant->competences[num_competence]);
  degat_max = attaquant->competences[num_competence]->f_degat_max(attaquant->competences[num_competence]);

  /*On initialise les variables de la victime*/
  esquive = victime->f_esquive_parade(victime);

  bool test = est_a_portee(attaquant, victime, num_competence);

  printf("\n\n Test boolean : %d", test);

  if(est_a_portee(attaquant, victime, num_competence))
  {
    printf("\n\nLa victime est à portée ! \n\n");
    /*Je génère un float aléatoire en 0 et 1*/
    float x = ((float)rand()/(float)(RAND_MAX)) * 1;

    printf("\n\nNuméro random : %f ! \n\n", x);

    /*Si le float est supérieur à l'esquive, alors je fais subire les dégats entre dégats min et degat max, sinon, il ne se passe rien*/
    if(x>=esquive)
    {
      etat = victime->f_subir_degats(degat_min + rand() %degat_max, victime);
    }

    /*Je vérifie ensuite l'état de la victime*/
    if(etat == Mort)
    {
      printf("Le personnage est mort");
    }
  }

}

/*Fonction pour le soin d'un personnage*/
/**
 * \fn void donner_soins(personnage_t * healer, personnage_t * soigner, int num_competence)
 * \brief fonction pour qu'un perssonage procure des soins
 * 
 * \param healer perssonnage qui lance le soins
 * \param soigner perssonage qui sera soigner
 * \param num_competence indice de la competence
 * 
 * \return void pas de retour
 * 
 */
extern
void donner_soins(personnage_t * healer, personnage_t * soigner, int num_competence)
{

}

/*Fonction pour le déplacement d'un personnage*/
extern
personnage_t * deplacer(personnage_t * perso)
{
  return NULL;
}
