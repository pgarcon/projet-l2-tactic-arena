/**
*  \file equipe.c
*  \brief classe equipe
*  \details Cette classe contient tous ce qui est relatif aux équipes
*  \author Pierre garcon
*  \date 06 fevrier
*  \fn
*/

#include "../include/equipe.h"

/*Methode et fonctions*/


/**
 * \fn bool liste_existe( liste_perso_t * const liste )
 * \brief fonction qui test l'existance d'une liste de perssonages
 *
 * \param liste Liste de perssonage
 *
 * \return bool true | false
 *
 */
extern
bool liste_existe( liste_perso_t * const liste )
{
  if( liste == NULL )
    {
      return(false) ;
    }
  else
    {
      return(true) ;
    }
}



/**
 * \fn void liste_decrementer_nb_vivant(liste_perso_t *)
 * \brief fonction qui décremente le nombre de personne vivante dans une équipe
 *
 * \param equipe liste_perso_t à décrémenter
 *
 * \return self
 *
 */
extern
void liste_decrementer_nb_vivant(liste_perso_t * liste)
{
  liste->nb_vivants--;
}


/**
 * \fn void liste_incrementer_nb_vivant(liste_perso_t *)
 * \brief fonction qui décremente le nombre de personne vivante dans une équipe
 *
 * \param equipe liste_perso_t à incrémenter
 *
 * \return self
 *
 */
extern
void liste_incrementer_nb_vivant(liste_perso_t * liste)
{
  liste->nb_vivants++;
}




/*Renvoie le nombre max de la liste*/
/**
 * \fn int liste_nb(liste_perso_t * liste)
 * \brief fonction affecte un nombre d'element à la liste
 *
 * \param liste Liste de perssonage
 *
 * \return int
 *
 */
extern
int liste_nb(liste_perso_t * liste)
{
  return(liste->nb);
}

/*Renvoie true si la liste est pleine*/
/**
 * \fn bool liste_pleine(liste_perso_t * liste)
 * \brief fonction test si la liste de perssonages est pleine
 *
 * \param liste Liste de perssonage
 *
 * \return bool
 *
 */
extern
bool liste_pleine(liste_perso_t * liste)
{
  return (liste->nb == liste->nb_pers);
}


/*Renvoie true si la liste est vide*/
/**
 * \fn bool liste_vide(liste_perso_t * liste)
 * \brief fonction test si la liste de perssonages est vide
 *
 * \param liste Liste de perssonage
 *
 * \return bool
 *
 */
extern
bool liste_vide(liste_perso_t * liste)
{
  return(liste->nb_pers == -1);
}

/**
 * \fn err_t liste_detruire( liste_perso_t ** liste )
 * \brief fonction qui detruit la liste des perssonages
 *
 * \param liste Liste de perssonage
 *
 * \return Ok
 *
 */
extern
err_t liste_detruire( liste_perso_t ** liste )
{
   /*On libère la mémoire et on met les pointeurs sur NULL*/
   /*je libère les races, mais comme certains personnages pointent sur la même, je dois faire attention à ne free qu'une fois chaque race*/
   int nb_orc =0, nb_demon=0, nb_kobolt=0, nb_humain=0, nb_nain=0, nb_elfe=0;
   for(int i=0; i<liste_nb(*liste); i++)
   {

     switch((*liste)->liste[i]->type_race)
     {
       case orc:

       if(nb_orc == 0)
          (*liste)->liste[i]->race->detruire(&((*liste)->liste[i]->race));
       nb_orc++;
       (*liste)->liste[i]->race = NULL;
       break;

       case demon:

       if(nb_demon == 0)
          (*liste)->liste[i]->race->detruire(&((*liste)->liste[i]->race));
       nb_demon++;
       (*liste)->liste[i]->race = NULL;
       break;

       case kobolt:

       if(nb_kobolt == 0)
          (*liste)->liste[i]->race->detruire(&((*liste)->liste[i]->race));
       nb_kobolt++;
       (*liste)->liste[i]->race = NULL;
       break;

       case elfe:

       if(nb_elfe == 0)
          (*liste)->liste[i]->race->detruire(&((*liste)->liste[i]->race));
       nb_elfe++;
       (*liste)->liste[i]->race = NULL;
       break;

       case nain:

       if(nb_nain == 0)
          (*liste)->liste[i]->race->detruire(&((*liste)->liste[i]->race));
       nb_nain++;
       (*liste)->liste[i]->race = NULL;
       break;

       case humain:

       if(nb_humain == 0)
          (*liste)->liste[i]->race->detruire(&((*liste)->liste[i]->race));
       nb_humain++;
       (*liste)->liste[i]->race = NULL;
       break;

       default:
        printf("\nErreur de suppression des races\n");
        break;
     }

     (*liste)->liste[i]->detruire((personnage_t**)&(*liste)->liste[i]);
     (*liste)->nb_pers--;

   }

   free((*liste)->liste);
   (*liste)->liste = NULL;

   free(*liste);
   (*liste)=NULL;

   return(Ok) ;
}



/*Récupération d'un personnage à un indice donné*/
/**
 * \fn personnage_t * liste_recuperer_pers(liste_perso_t * liste, int indice)
 * \brief fonction qui recupere un perssonage percis a l'indice voulue
 *
 * \param liste Liste de perssonage
 * \param indice Indice du perssonage voulue
 *
 * \return liste->liste[indice] | NULL
 *
 */
extern
personnage_t * liste_recuperer_pers(liste_perso_t * liste, int indice)
{
  if(!(liste_vide(liste))&&(indice>=0)&&(indice<=liste->nb))
  {
    return(liste->liste[indice]);
  }
  return NULL;
}



/*Ajout d'un élément dans la liste*/
/**
 * \fn void liste_ajouter(liste_perso_t * liste, personnage_t * pers)
 * \brief fonction qui ajoute un perssonage à la liste
 *
 * \param liste Liste de perssonage
 * \param pers Perssonnage a ajouter a la liste
 *
 * \return void pas de retour
 *
 */
extern
void liste_ajouter(liste_perso_t * liste, personnage_t * pers)
{
  if(!liste_pleine(liste))
  {
    liste->nb_pers++;
    liste->liste[liste->nb_pers] = pers;
  }
}



/*Enlever une valeur*/
/**
 * \fn void liste_enlever(liste_perso_t * liste, personnage_t * pers)
 * \brief fonction qui enleve un personnage de la liste des perssonages
 *
 * \param liste Liste de perssonage
 *
 * \return void pas de retour
 *
 */
extern
void liste_enlever(liste_perso_t * liste)
{
  if(!(liste_vide(liste)))
  {
    free(liste->liste[liste->nb_pers]);
    liste->liste[liste->nb_pers] = NULL;
    liste->nb_pers--;
  }
}



/*Vider la liste*/
/**
 * \fn void liste_vider(liste_perso_t * liste, personnage_t * pers)
 * \brief fonction qui vide la liste des perssonages
 *
 * \param liste Liste de perssonage
 *
 * \return void pas de retour
 *
 */
extern
void liste_vider(liste_perso_t * liste)
{
  while(!liste_vide(liste))
  {
    liste_enlever(liste);
  }
}

/**
 * \fn void liste_afficher(liste_perso_t * liste, personnage_t * pers)
 * \brief fonction qui affiche la liste de tout les perssonages
 *
 * \param liste Liste de perssonage
 *
 * \return void pas de retour
 *
 */
extern
void liste_afficher(liste_perso_t * liste)
{
  int i;
  printf("Nombre de personnages vivants : %i\n----------------------------------------------------------------\n", liste->nb_vivants);
  for(i=0; i<liste->nb; i++)
  {
    if(liste->liste[i] != NULL)
    {
      liste->liste[i]->print(liste->liste[i]);
    }
    printf("----------------------------------------------------------------");
  }
}



/*
 * Creation d'une liste
 */
/**
 * \fn liste_perso_t * liste_perso_creer( const int nb )
 * \brief fonction qui creer une liste de perssonnages
 *
 * \param nb Taille de la liste
 *
 * \return liste
 *
 */
extern
liste_perso_t * liste_perso_creer( const int nb )
{
  liste_perso_t * liste ;
  int i;
  liste= malloc(sizeof(liste_perso_t));


  /*On créer l'objet*/
  liste->nb = nb ;
  liste->liste = NULL ;
  liste->nb_vivants = 5;

  /*On créer la liste de personnage dans l'objet liste*/
  if( nb > 0 )
  {
    liste->liste = malloc( sizeof(personnage_t *) * nb );
  }

  /*On parcours chacun des pointeurs de la liste de personnage, et on met leur valeur à NULL*/
  for(i=0; i<nb; i++)
  {
    liste->liste[i] = NULL;
  }
  liste->nb_pers = -1;

  return(liste);
}

extern
void liste_perso_nb_personnage_vivant(liste_perso_t * liste)
{
  int i;
  for(i=0; i<liste->nb; i++)
  {
    if(liste->liste[i]->etat == Mort)
    {
      liste->nb_vivants--;
    }
  }
}

/**
 * \fn void liste_ajouter_perso_equipe(class_t classe, type_race_t race, liste_perso_t * equipe, equipe_t team, int * nb_arc, int * nb_ass, int * nb_heal, int * nb_tank, int * nb_epe, int * nb_mage)
 * \brief fonction qui décremente le nombre de personne vivante dans une équipe
 *
 * \param classe classe du personnage a ajouter
 * \param race race du personnage a ajouter
 * \param equipe liste_perso_t à remplir
 * \param team couleur de l'équipe (bleu ou rouge)
 * \param int *  nombre d'archer
 * \param int *  nombre d'assassin
 * \param int *  nombre de healer
 * \param int *  nombre de tank
 * \param int *  nombre d'épéiste
 * \param int *  nombre de mage
 *
 * \return self
 *
 */
extern
int liste_ajouter_perso_equipe(class_t classe, type_race_t race, liste_perso_t * equipe, equipe_t team, int * nb_arc, int * nb_ass, int * nb_heal, int * nb_tank, int * nb_epe, int * nb_mage)
{
  race_t * race_t = NULL;
  personnage_t * pers;
  if(race == kobolt)
  {
    race_t = race_creer_kobolt();
    if((classe == archer) && (*nb_arc < 2))
    {
      pers = (personnage_t *)creer_archer(team, race_t, equipe);
      liste_ajouter(equipe, pers);
      nb_arc++;
    }
    if((classe == tank) && (*nb_tank < 2))
    {
      pers = (personnage_t *)creer_tank(team, race_t, equipe);
      liste_ajouter(equipe, pers);
      nb_tank++;
    }
    if((classe == mage) && (*nb_mage < 2))
    {
      pers = (personnage_t *)creer_mage(team, race_t, equipe);
      liste_ajouter(equipe, pers);
      nb_mage++;
    }
    if((classe == healer) && (*nb_heal < 2))
    {
      pers = (personnage_t *)creer_healer(team, race_t, equipe);
      liste_ajouter(equipe, pers);
      nb_heal++;
    }
    if((classe == assassin) && (*nb_ass < 2))
    {
      pers = (personnage_t *)creer_assassin(team, race_t, equipe);
      liste_ajouter(equipe, pers);
      nb_ass++;
    }
    if((classe == epeiste) && (*nb_epe < 2))
    {
      pers = (personnage_t *)creer_epeiste(team, race_t, equipe);
      liste_ajouter(equipe, pers);
      nb_epe++;
    }
  }


  if(race == nain)
  {
    race_t = race_creer_nain();
    if((classe == archer) && (*nb_arc < 2))
    {
      pers = (personnage_t *)creer_archer(team, race_t, equipe);
      liste_ajouter(equipe, pers);
      nb_arc++;
    }
    if((classe == tank) && (*nb_tank < 2))
    {
      pers = (personnage_t *)creer_tank(team, race_t, equipe);
      liste_ajouter(equipe, pers);
      nb_tank++;
    }
    if((classe == mage) && (*nb_mage < 2))
    {
      pers = (personnage_t *)creer_mage(team, race_t, equipe);
      liste_ajouter(equipe, pers);
      nb_mage++;
    }
    if((classe == healer) && (*nb_heal < 2))
    {
      pers = (personnage_t *)creer_healer(team, race_t, equipe);
      liste_ajouter(equipe, pers);
      nb_heal++;
    }
    if((classe == assassin) && (*nb_ass < 2))
    {
      pers = (personnage_t *)creer_assassin(team, race_t, equipe);
      liste_ajouter(equipe, pers);
      nb_ass++;
    }
    if((classe == epeiste) && (*nb_epe < 2))
    {
      pers = (personnage_t *)creer_epeiste(team, race_t, equipe);
      liste_ajouter(equipe, pers);
      nb_epe++;
    }
  }


  if(race == humain)
  {
    race_t = race_creer_humain();
    if((classe == archer) && (*nb_arc < 2))
    {
      pers = (personnage_t *)creer_archer(team, race_t, equipe);
      liste_ajouter(equipe, pers);
      nb_arc++;
    }
    if((classe == tank) && (*nb_tank < 2))
    {
      pers = (personnage_t *)creer_tank(team, race_t, equipe);
      liste_ajouter(equipe, pers);
      nb_tank++;
    }
    if((classe == mage) && (*nb_mage < 2))
    {
      pers = (personnage_t *)creer_mage(team, race_t, equipe);
      liste_ajouter(equipe, pers);
      nb_mage++;
    }
    if((classe == healer) && (*nb_heal < 2))
    {
      pers = (personnage_t *)creer_healer(team, race_t, equipe);
      liste_ajouter(equipe, pers);
      nb_heal++;
    }
    if((classe == assassin) && (*nb_ass < 2))
    {
      pers = (personnage_t *)creer_assassin(team, race_t, equipe);
      liste_ajouter(equipe, pers);
      nb_ass++;
    }
    if((classe == epeiste) && (*nb_epe < 2))
    {
      pers = (personnage_t *)creer_epeiste(team, race_t, equipe);
      liste_ajouter(equipe, pers);
      nb_epe++;
    }
  }


  if(race == elfe)
  {
    race_t = race_creer_elfe();
    if((classe == archer) && (*nb_arc < 2))
    {
      pers = (personnage_t *)creer_archer(team, race_t, equipe);
      liste_ajouter(equipe, pers);
      nb_arc++;
    }
    if((classe == tank) && (*nb_tank < 2))
    {
      pers = (personnage_t *)creer_tank(team, race_t, equipe);
      liste_ajouter(equipe, pers);
      nb_tank++;
    }
    if((classe == mage) && (*nb_mage < 2))
    {
      pers = (personnage_t *)creer_mage(team, race_t, equipe);
      liste_ajouter(equipe, pers);
      nb_mage++;
    }
    if((classe == healer) && (*nb_heal < 2))
    {
      pers = (personnage_t *)creer_healer(team, race_t, equipe);
      liste_ajouter(equipe, pers);
      nb_heal++;
    }
    if((classe == assassin) && (*nb_ass < 2))
    {
      pers = (personnage_t *)creer_assassin(team, race_t, equipe);
      liste_ajouter(equipe, pers);
      nb_ass++;
    }
    if((classe == epeiste) && (*nb_epe < 2))
    {
      pers = (personnage_t *)creer_epeiste(team, race_t, equipe);
      liste_ajouter(equipe, pers);
      nb_epe++;
    }
  }


  if(race == demon)
  {
    race_t = race_creer_demon();
    if((classe == archer) && (*nb_arc < 2))
    {
      pers = (personnage_t *)creer_archer(team, race_t, equipe);
      liste_ajouter(equipe, pers);
      nb_arc++;
    }
    if((classe == tank) && (*nb_tank < 2))
    {
      pers = (personnage_t *)creer_tank(team, race_t, equipe);
      liste_ajouter(equipe, pers);
      nb_tank++;
    }
    if((classe == mage) && (*nb_mage < 2))
    {
      pers = (personnage_t *)creer_mage(team, race_t, equipe);
      liste_ajouter(equipe, pers);
      nb_mage++;
    }
    if((classe == healer) && (*nb_heal < 2))
    {
      pers = (personnage_t *)creer_healer(team, race_t, equipe);
      liste_ajouter(equipe, pers);
      nb_heal++;
    }
    if((classe == assassin) && (*nb_ass < 2))
    {
      pers = (personnage_t *)creer_assassin(team, race_t, equipe);
      liste_ajouter(equipe, pers);
      nb_ass++;
    }
    if((classe == epeiste) && (*nb_epe < 2))
    {
      pers = (personnage_t *)creer_epeiste(team, race_t, equipe);
      liste_ajouter(equipe, pers);
      nb_epe++;
    }

  }


  if(race == orc)
  {
    race_t = race_creer_orc();
    if((classe == archer) && (*nb_arc < 2))
    {
      pers = (personnage_t *)creer_archer(team, race_t, equipe);
      liste_ajouter(equipe, pers);
      nb_arc++;
    }
    if((classe == tank) && (*nb_tank < 2))
    {
      pers = (personnage_t *)creer_tank(team, race_t, equipe);
      liste_ajouter(equipe, pers);
      nb_tank++;
    }
    if((classe == mage) && (*nb_mage < 2))
    {
      pers = (personnage_t *)creer_mage(team, race_t, equipe);
      liste_ajouter(equipe, pers);
      nb_mage++;
    }
    if((classe == healer) && (*nb_heal < 2))
    {
      pers = (personnage_t *)creer_healer(team, race_t, equipe);
      liste_ajouter(equipe, pers);
      nb_heal++;
    }
    if((classe == assassin) && (*nb_ass < 2))
    {
      pers = (personnage_t *)creer_assassin(team, race_t, equipe);
      liste_ajouter(equipe, pers);
      nb_ass++;
    }
    if((classe == epeiste) && (*nb_epe < 2))
    {
      pers = (personnage_t *)creer_epeiste(team, race_t, equipe);
      liste_ajouter(equipe, pers);
      nb_epe++;
    }
  }


  return(1);
}

/*Les menu des selection*/
/**
 * \fn void liste_perso_creer( int n)
 * \brief fonction qui affiche le menu choisis
 *
 * \param nb 0 ou 1 en fonction du menu
 *
 * \return self
 *
 */
static
void afficher_menu(int n)
{
  if(n==0)
  {
    printf("\nSelection des personnages : \n");
    printf("\n\n");
    printf("1 - Archer\n");
    printf("2 - mage\n");
    printf("3 - healer\n");
    printf("4 - assassin\n");
    printf("5 - tank\n");
    printf("6 - epeiste\n");
  }

  if(n==1)
  {
    printf("\nChoississez votre race : \n");
    printf("\n\n");
    printf("1 - Demon\n");
    printf("2 - Humain\n");
    printf("3 - Elfe\n");
    printf("4 - Nain\n");
    printf("5 - Kobolt\n");
    printf("6 - Orc\n");
    printf("7 - Suppression Personnage précedent\n");
  }

}

/*Fonction de selection des personnages*/
/**
 * \fn personnage_t * select_pers(liste_perso_t * armee, equipe_t equipe, race_t * race2, int * nb_arc, int * nb_ass, int * nb_heal, int * nb_tank, int * nb_epe, int * nb_mage)
 * \brief fonction qui permet de choisir le personnage à mettre dans la liste en terminal
 *
 * \param liste_perso_t * armee
 * \param equipe_t equipe
 * \param race_t * race2,
 * \param int * nb_arc,
 * \param int * nb_ass,
 * \param int * nb_heal,
 * \param int * nb_tank,
 * \param int * nb_epe,
 * \param int * nb_mage
 * \return personnage_t *
 *
 */
static
personnage_t * select_pers(liste_perso_t * armee, equipe_t equipe, race_t * race2, int * nb_arc, int * nb_ass, int * nb_heal, int * nb_tank, int * nb_epe, int * nb_mage)
{
  archer_t * archer;
  mage_t * mage;
  healer_t* healer;
  assassin_t* assassin;
  tank_t * tank;
  epeiste_t* epe;
  personnage_t * pers = NULL;
  int valide = 1;

  /*Le choix des personnages*/
  int personnage = 1;

  printf("\nDebut des choix\n");
  afficher_menu(0);
  do{
    do{
        if(personnage<=0 || personnage>6)
          printf("\nErreur de saisie, veuillez choisir un personnage correct ! \n\n");
        scanf("%d", &personnage);
    }while((personnage<=0) || (personnage>=7));

    /*Va créer et ajouter à la liste un personnage choisis par l'utilisateur*/

    switch (personnage) {

      case(1):
        if((*nb_arc) != 2)
        {
          archer = creer_archer(equipe, race2, armee);
          (*nb_arc)++;
          return ((personnage_t*)archer);
        }
        printf("\nIl y a deja 2 archers, impossible d'en ajouter un autre \n\n");
        break;

      case(2):
        if((*nb_mage) != 2)
        {
          mage = creer_mage(equipe,  race2, armee);
          (*nb_mage)++;
          return ((personnage_t*)mage);
        }
        printf("\nIl y a deja 2 mages, impossible d'en ajouter un autre\n\n");
        break;


      case(3):
        if((*nb_heal) != 2)
        {
          healer = creer_healer(equipe,  race2, armee);
          (*nb_heal)++;
          return ((personnage_t*)healer);
        }
        printf("\nIl y a deja 2 healers, impossible d'en ajouter un autre\n\n");
        break;


      case(4):
        if((*nb_ass) != 2)
        {
          assassin = creer_assassin(equipe,  race2, armee);
          (*nb_ass)++;
          return ((personnage_t*)assassin);
        }
        printf("\nIl y a deja 2 assassins, impossible d'en ajouter un autre\n\n");
        break;


      case(5):
        if((*nb_tank) != 2)
        {
          tank = creer_tank(equipe,  race2, armee);
          (*nb_tank)++;
          return ((personnage_t*)tank);
        }
        printf("\nIl y a deja 2 tanks, impossible d'en ajouter un autre\n\n");
        break;


      case(6):
        if((*nb_epe) != 2)
        {
          epe = creer_epeiste(equipe,  race2, armee);
          (*nb_epe)++;
          return ((personnage_t*)epe);
        }
        printf("\nIl y a deja 2 epeistes, impossible d'en ajouter un autre\n\n");
        break;


      default:
        printf("\nErreur, la saisie des personnage ne s'est pas effectuée correctement\n");
        break;
    }
  }while(valide == 1);

  return pers;

}


/*Fonction des creation des equipes*/
/*Les menu des selection*/
/**
 * \fn void liste_perso_equipe_creer(liste_perso_t * armee1, equipe_t equipe)
 * \brief fonction qui permet la création d'une équipe en terminal
 *
 * \param armee1 equipe à complétée
 * \param equipe couleur de l'quipe (bleue ou rouge)
 * \return self
 *
 */
extern
void liste_perso_equipe_creer(liste_perso_t * armee1, equipe_t equipe)
{
  race_t * r_demon = race_creer_demon();
  race_t * r_kobolt = race_creer_orc();
  race_t * r_humain = race_creer_humain();
  race_t * r_elfe = race_creer_elfe();
  race_t * r_orc = race_creer_orc();
  race_t * r_nain = race_creer_nain();

  int nb_arc = 0, nb_ass = 0, nb_heal = 0, nb_tank = 0, nb_epe = 0, nb_mage = 0;
  int nb_demon = 0, nb_kobolt = 0, nb_humain = 0, nb_elfe = 0, nb_orc = 0, nb_nain = 0;

  personnage_t * perso = NULL;


  int selection = 0;
  int n = 0;
  printf("\n-----------------------------------------------------------------\nChoix des personnages\n---------------------------------------------------------------------------\n\n");

  do{
    afficher_menu(1);
    do{
        if(selection<0 || selection>7)
          printf("\nErreur de saisie, veuillez choisir une race correct ! \n\n");
        scanf("%d", &selection);
    }while(selection<=0 || selection>7);


    switch(selection)
    {
      case 1:
        perso = select_pers(armee1, equipe, r_demon, &nb_arc, &nb_ass, &nb_heal, &nb_tank, &nb_epe, &nb_mage);
        liste_ajouter(armee1, perso);
        n++;
        nb_demon++;
        break;

      case 2:
        perso = select_pers(armee1, equipe, r_humain, &nb_arc, &nb_ass, &nb_heal, &nb_tank, &nb_epe, &nb_mage);
        liste_ajouter(armee1, perso);
        n++;
        nb_humain++;
        break;

      case 3:
        perso = select_pers(armee1, equipe, r_elfe, &nb_arc, &nb_ass, &nb_heal, &nb_tank, &nb_epe, &nb_mage);
        liste_ajouter(armee1, perso);
        n++;
        nb_elfe++;
        break;

      case 4:
        perso = select_pers(armee1, equipe, r_nain, &nb_arc, &nb_ass, &nb_heal, &nb_tank, &nb_epe, &nb_mage);
        liste_ajouter(armee1, perso);
        n++;
        nb_nain++;
        break;

      case 5:
        perso = select_pers(armee1, equipe, r_kobolt, &nb_arc, &nb_ass, &nb_heal, &nb_tank, &nb_epe, &nb_mage);
        liste_ajouter(armee1, perso);
        n++;
        nb_kobolt++;
        break;

      case 6:
        perso = select_pers(armee1, equipe, r_orc, &nb_arc, &nb_ass, &nb_heal, &nb_tank, &nb_epe, &nb_mage);
        liste_ajouter(armee1, perso);
        n++;
        nb_orc++;
        break;

      case 7:
        if(n!=0)
        {
          if(armee1->liste[n-1]->classe == archer)
            nb_arc--;
          if(armee1->liste[n-1]->classe == mage)
            nb_mage--;
          if(armee1->liste[n-1]->classe == tank)
            nb_tank--;
          if(armee1->liste[n-1]->classe == assassin)
            nb_ass--;
          if(armee1->liste[n-1]->classe == healer)
            nb_heal--;
          if(armee1->liste[n-1]->classe == epeiste)
            nb_epe--;
          if(armee1->liste[n-1]->race->race == orc)
            nb_orc--;
          if(armee1->liste[n-1]->race->race == demon)
            nb_demon--;
          if(armee1->liste[n-1]->race->race == nain)
            nb_nain--;
          if(armee1->liste[n-1]->race->race == kobolt)
            nb_kobolt--;
          if(armee1->liste[n-1]->race->race == elfe)
            nb_elfe--;
          if(armee1->liste[n-1]->race->race == humain)
            nb_humain--;
          liste_enlever(armee1);
          n--;
        }
        else
        {
          printf("\nLa list de personnage est vide\n");
        }
        break;

      default:
        printf("\nErreur de selection des races\n");
        break;
    }
    printf("\nn : %d\n", n);
  }while(n<NB_PERS);

  if(nb_demon == 0)
  {
    r_demon->detruire(&r_demon);
  }
  if(nb_orc == 0)
  {
    r_orc->detruire(&r_orc);
  }
  if(nb_humain == 0)
  {
    r_humain->detruire(&r_humain);
  }
  if(nb_nain == 0)
  {
    r_nain->detruire(&r_nain);
  }
  if(nb_elfe == 0)
  {
    r_elfe->detruire(&r_elfe);
  }
  if(nb_kobolt == 0)
  {
    r_kobolt->detruire(&r_kobolt);
  }

}
