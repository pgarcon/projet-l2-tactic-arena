/**
 * \file final_SDL.c
 * \brief Fichier pour executer tout le jeu
 * \author Evan, Nicolas
 * \version 0.5
 * \date  11 avril 2021
 *
 * Jeu total tactic arena
 */


//#include "../include/map2D.h"
#include "../include/menu.h"
#include "../include/map2D.h"


/**
 * \fn int main(int argc, char** argv)
 * \brief Programme principale.
 *
 * \return EXIT_SUCCESS - Arrêt normal du programme.
 */
int main(int argc, char** argv){
    
    int play=1;
    while(play==1){
        if(!menu())
            play=game();
        else
            play=0;
    }
    


    return EXIT_SUCCESS;
}
