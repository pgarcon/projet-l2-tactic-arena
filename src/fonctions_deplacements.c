#include "../include/attaques.h"
#include "../include/map.h"
#include "../include/deplacements.h"
#include "../include/liste_positions.h"
/**
 * \file fonctions_deplacements.c
 * \brief Fonctions liées aux déplacements des personnages
 * \author Yohann Delacroix
 * \version 1.0
 * \date 16 Mars 2021
 */

/**
 * \fn int compter_cases_valides(int taille_zone, t_pos case_cible)
 * \brief Détermine le nombre de cases atteignables pour un déplacement
 * \param taille_zone Portée de déplacement du personnage
 * \param case_cible Position du personnage
 * \return Integer représentant le nombre de cases atteignables
 */
int compter_cases_valides(int taille_zone, t_pos case_cible)
{
	int cov_y;
	int seek_x;
	int seek_y;
	int x, y;
	int nombre_de_cases_possibles = 0;
	for(x = -taille_zone; x <= taille_zone; x++)
	{
		cov_y = taille_zone - abs(x);
		for(y = -cov_y; y <= cov_y ; y++)
		{
			seek_x = case_cible.x - x;
			seek_y = case_cible.y - y;

			if(seek_x >= 0 && seek_x < N && seek_y >=0 && seek_y < M)//Controler si les cases ne sont pas hors de la map
			{
				if(map[seek_x][seek_y].type_emplacement == VIDE)
				{
					nombre_de_cases_possibles++;
				}
			}
		}
	}
	return nombre_de_cases_possibles;
}



/**
 * \fn liste_pos_t* deplacement_valide(personnage_t * joueur)
 * \brief Recherche les cases atteignables pour un déplacement et les insere dans une liste
 * \param joueur Joueur qui demande un déplacement
 * \return pointeur sur liste_pos_t contenant les positions atteignables
 */
liste_pos_t* deplacement_valide(personnage_t * joueur)
{
	//Initialisations
	liste_pos_t * liste_de_positions = NULL;
	int taille_zone = joueur->portee.deplacement;
	t_pos position_joueur = {joueur->pos_x, joueur->pos_y};
	t_pos nouvelle_position;	

	//Recherche du nombre de cases ciblables pour l'allocation mémoire
	int nb_positions = compter_cases_valides(taille_zone, position_joueur);
	liste_de_positions = liste_positions_initialiser(nb_positions);
	

	//Remplissage de la liste
	int cov_y;
	int seek_x;
	int seek_y;
	int x, y;
	int indice = 0;
	for(x = -taille_zone; x <= taille_zone; x++)
	{
		cov_y = taille_zone - abs(x);
		for(y = -cov_y; y <= cov_y ; y++)
		{
			seek_x = position_joueur.x - x;
			seek_y = position_joueur.y - y;

			if(seek_x >= 0 && seek_x < N && seek_y >=0 && seek_y < M)//Controler si les cases ne sont pas hors de la map
			{
				if(map[seek_x][seek_y].type_emplacement == VIDE)
				{
					
					nouvelle_position.x = seek_x;
					nouvelle_position.y = seek_y;
					liste_positions_ajouter( liste_de_positions, nouvelle_position, indice );
					
					indice++;
				}
			}
		}
	}

	
	return liste_de_positions;
}


/**
 * \fn int effectuer_deplacement(personnage_t * joueur, t_pos case_cible) 
 * \brief Déplace un personnage sur le terrain de jeu
 * \param joueur Joueur qui demande un déplacement
 * \param case_cible Case ciblée par le joueur pour le déplacement
 * \return Integer code Erreur 1, 0 tout s'est bien passé
 * Controle si le déplacement est valide 
 * Change l'orientation du personnage après le déplacement
 */
int effectuer_deplacement(personnage_t * joueur, t_pos case_cible) 
{
	int i;	
	liste_pos_t * liste_de_positions = deplacement_valide(joueur);
	t_pos position_possible;	

	for(i = 0; i < liste_de_positions->nb_elem; i++)
	{
		position_possible = liste_position_lire(liste_de_positions, i);

		if(position_possible.x == case_cible.x && position_possible.y == case_cible.y)//Si la position est valide
		{
			//Déplacement du personnage
			changer_orientation_personnage(joueur, case_cible);
			map[joueur->pos_x][joueur->pos_y].type_emplacement = VIDE;
			map[joueur->pos_x][joueur->pos_y].perso = NULL;
			joueur->pos_x = case_cible.x;
			joueur->pos_y = case_cible.y;
			map[case_cible.x][case_cible.y].type_emplacement = PERSO;
			map[case_cible.x][case_cible.y].perso = joueur;

			
			return 0;			
		}
	}

	//Le personnage n'a pas pu se déplacer
	liste_positions_detruire( &liste_de_positions );
	return 1;
}


/**
 * \fn void proposer_deplacement_terminal(personnage_t * joueur)
 * \brief Proposer un déplacement en version terminale
 * \param joueur Joueur qui demande un déplacement
 * \return Aucune valeur de retour
 * Invite l'utilisateur à rentrer des coordonnées par entrée clavier
 */
void proposer_deplacement_terminal(personnage_t * joueur)
{
	int deplacement_effectue = 0;
	do
	{
		t_pos case_choisie;
		printf("Déplacement --- Choisir les coordonnées [x,y]. \n");
		printf("x : ");
		scanf("%i", &(case_choisie.x));
		printf("y : ");
		scanf("%i", &(case_choisie.y));
		deplacement_effectue = effectuer_deplacement(joueur, case_choisie);
		if( deplacement_effectue == 1 )
		{
			printf("Coordonnées invalides\n");
			do
			{
				printf("Tenter un nouveau déplacement ? [1: Oui. 0: Non.]\n");
				scanf("%i", &deplacement_effectue);
			}while(deplacement_effectue < 0 || deplacement_effectue > 1);
		}
	}while(deplacement_effectue == 1);
}


/**
 * \fn int changer_orientation_personnage(personnage_t * perso, t_pos arrivee)
 * \brief Change l'orientation d'un personnage
 * \param perso Joueur qui requiert le changement d'orientation
 * \param arrivee Case d'arrivée du personnage
 * \return Integer code Erreur 1, 0 tout s'est bien passé
 * Calcule selon la position du personnage et la position d'arrivée la direction dans laquelle le personnage regarde
 */
int changer_orientation_personnage(personnage_t * perso, t_pos arrivee) //Retourne 0 ou 1 pour réussite/succes
{
	t_pos depart;
	depart.x = perso->pos_x;
	depart.y = perso->pos_y;

	int difference_x = arrivee.x - depart.x;
	int difference_y = arrivee.y - depart.y;

	if( abs(difference_x) == abs(difference_y) )//En cas d'égalité on prend en compte par défaut la position y
	{
		if(difference_y > 0)
		{
			perso->direction = droite;
		}
		else
		{
			perso->direction = gauche;
		}
	}
	else
	{
		if( abs(difference_x) > abs(difference_y) )
		{
			if(difference_x > 0)
			{
				perso->direction = bas;
			}
			else
			{
				perso->direction = haut;
			}
		}
		else
		{
			if(difference_y > 0)
			{
				perso->direction = droite;
			}
			else
			{
				perso->direction = gauche;
			}
		}
	}
	return 0;
}



/**
 * \fn void ajouter_marqueurs_deplacements_carte(liste_pos_t * liste_positions)
 * \brief Ajoute des marqueurs sur la carte qui montrent les cases atteignables pour un déplacement
 * \param liste_positions Liste qui contient des positions atteignables
 * \return Aucune valeur de retour
 * Remplit le champ po_deplacement de la structure t_carte par la valeur 1
 */
void ajouter_marqueurs_deplacements_carte(liste_pos_t * liste_positions)
{
	int i;
	t_pos position;
	for(i = 0; i < liste_positions->nb_elem; i++)
	{
		position = liste_position_lire(liste_positions, i);
		map[position.x][position.y].po_deplacement = 1;
	}
}

/**
 * \fn void retirer_marqueurs_deplacements_carte(liste_pos_t * liste_positions)
 * \brief Retrait des marqueurs de déplacements sur la carte
 * \param liste_positions Liste qui contient des positions atteignables
 * \return Aucune valeur de retour
 * Remplit le champ po_deplacement de la structure t_carte par la valeur 0
 */
void retirer_marqueurs_deplacements_carte(liste_pos_t * liste_positions)
{
	int i;
	t_pos position;
	for(i = 0; i < liste_positions->nb_elem; i++)
	{
		position = liste_position_lire(liste_positions, i);
		map[position.x][position.y].po_deplacement = 0;
	}
}

