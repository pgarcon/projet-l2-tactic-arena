/**
 * \file generateur_carte.c
 * \brief Fichier pour generer des cartes
 * \author 
 * \version 1.0
 * \date 
 *
 * Foncion pour creer ou generer un fichier avec une map 0 et 1 
 *
 */

#include <stdio.h>
#include "../include/map.h"

/**
 * \fn void initialiser_plateau(void)
 * \brief fonction qui initialise le plateau de jeu avec des 3 pour etre sur deviter les erreur
 * 
 * \param self
 * 
 * \return void pas de retour
 * 
 */
void initialiser_plateau()
{
	for(int i = 0; i < N; i++)
	{
		for(int j = 0; j < M; j++)
		{
			plateau[i][j] = 4; //Initialisation à 4 pour être sur de ne pas avoir de 0 ou -1
		}
	}
}

/**
 * \fn void afficher_plateau(void)
 * \brief fonction qui affiche le plateau de jeu
 * 
 * \param self
 * 
 * \return void pas de retour
 * 
 */
void afficher_plateau_g() //Affiche le plateau pour le remplissage des cases
{
	for(int i = 0; i < N; i++)
	{
		for(int j = 0; j < M; j++)
		{
			switch(plateau[i][j])
			{
				case 0:
					printf(" 0 ");
					break;
				case -1:
					printf(" # ");
					break;
				case 2:
					printf(" R ");
					break;
				case 3:
					printf(" B ");
					break;
				default:
					printf(" . ");
					break;
			}
		}
		printf("\n");
	}
}

/**
 * \fn int main(void)
 * \brief fonction main
 * 
 * \param self
 * 
 * \return int 0
 * 
 */
int main()
{
	initialiser_plateau();
	srand (time(NULL));
	afficher_plateau_g();

	//CHOIX
	int choix =0;
	do
	{
		printf("1 : -- Mode de création de map aléatoire --\n2 : -- Mode de création de map personnalisé\n Choix : ");
		scanf("%i", &choix);
	}while(choix != 1 && choix != 2);

	int mi = -1;
	int ma = 0;
	switch(choix)
	{
		case 1: //Creation aleatoire
			
			for(int i = 0; i < N; i++)
			{
				for(int j = 0; j < M; j++)
				{
					plateau[i][j] = rand()%(ma-mi+1)+mi;
				}
			}
			afficher_plateau_g();
			break;
		case 2: //Creation personnalisée
			printf("EDITEUR DE MAP : Création \n 0 pour VIDE , -1 pour MUR : ");
			for(int i = 0; i < N; i++)
			{
				for(int j = 0; j < M; j++)
				{
					
					do
					{
						printf("EDITEUR DE MAP : Création \n 0 pour VIDE , 1 pour MUR , 2 pour POS_ROUGE, 3 pour POS_BLEUE: ");
						scanf("%i", &choix);
						
					}while(choix != 1 && choix != 0 && choix != 2 && choix != 3);

					switch(choix)
					{
						case 0:
							plateau[i][j] = 0;
							break;
						case 1:
							plateau[i][j] = -1;
							break;
						case 2:
							plateau[i][j] = 2;
							break;
						case 3:
							plateau[i][j] = 3;
							break;
					}
					afficher_plateau_g();
				}
			}
			
			break;	
	}

	
	
	//Ecriture dans le fichier
	FILE * new;
	new = fopen("new_map.txt", "w");
	if(new != NULL)
	{
		for(int i = 0; i < N; i++)
		{
			for(int j = 0; j < M; j++)
			{
				switch(plateau[i][j])
				{
					case 0:
						fprintf(new, "0 ");
						break;
					case -1:
						fprintf(new, "-1 ");
						break;
					case 2:
						fprintf(new, "2 ");
						break;
					case 3:
						fprintf(new, "3 ");
						break;
				}
				 
			}
		}
	}
	else
	{
		fprintf(stderr, "Probleme avec le FILE");
	}
	return 0;
}
