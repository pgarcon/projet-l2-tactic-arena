#include <stdio.h>
#include "../include/lignedevue.h"

/**
 * \file lignedevue.c
 * \brief Fonctions et mécanismes de détermination des lignes de vue
 * \author Yohann Delacroix
 * \version 1.0
 * \date 14 Avril 2021
 */



/**
 * \fn int round_left(float x)
 * \brief Arrondi la valeur par la borne entière inférieure
 * \param x Valeur réelle
 * \return Entier arrondi
 */
int round_left(float x)
{
	int x_entier = (int)x;
	
	return x_entier;
}



/**
 * \fn int round_right(float x)
 * \brief Arrondi la valeur par la borne entière supérieure
 * \param x Valeur réelle
 * \return Entier arrondi
 */
int round_right(float x)
{
	int x_entier = (int)x + 1;
	
	return x_entier;
}


/**
 * \fn int round_value(float x)
 * \brief Arrondi une valeur réelle
 * \param x Valeur réelle
 * \return Entier arrondi
 */
int round_value(float x)
{
	float new = x + 0.5;
	if(new > round_right(x) )
	{
		return round_right(x);
	}	
	else
	{
		return round_left(x);
	}

}

/**
 * \fn float affine(float coeff, float constante, float x)
 * \brief Retourne la valeur y d'une fonction affine y = ax + c
 * \param coeff Coefficient a de la droite
 * \param constante Constante c du polynôme
 * \param x Entrée de la fonction
 * \return Valeur réelle résultat de la fonction affine
 */
float affine(float coeff, float constante, float x)
{
	return coeff * x + constante;
}

/**
 * \fn float x_affine(float coeff, float constante, float y)
 * \brief Retourne la valeur x d'une fonction affine y = ax + c
 * \param coeff Coefficient a de la droite
 * \param constante Constante c du polynôme
 * \param y Résultat de la fonction
 * \return Valeur réelle résultat de l'entrée de la fonction affine
 */
float x_affine(float coeff, float constante, float y)
{
	return (y - constante)/coeff;
}

/**
 * \fn liste_pos_t * ligne_de_vue(t_pos perso, t_pos mur, int portee_max)
 * \brief Retourne une liste de positions inatteignables cachée par un mur
 * \param perso Position du personnage lanceur
 * \param mur Position de l'obstacle
 * \param portee_max Portée de l'attaque
 * \return pointeur sur liste de positions qui contient les positions non-atteignables
 */
liste_pos_t * ligne_de_vue(t_pos perso, t_pos mur, int portee_max)
{
	t_pos pos_d1;
	t_pos pos_d2;
	int i;
	int j;
	int norme_PM;
	float coeff_a_d1;
	float coeff_a_d2;
	float constante_d1;
	float constante_d2;
	t_pos pos_ajouter;
	int indice_liste = 0;

	liste_pos_t * liste_positions = liste_positions_initialiser(N*M);
	
	
	if(mur.x == perso.x) //Cas particulier ou le perso et le mur sont aligné sur y
	{	
		
		if(perso.y < mur.y)
		{
			//Norme PM
			norme_PM = (perso.y - mur.y)/2;
			//Définition des facteurs du polynome
			coeff_a_d1 = (float)norme_PM;
			coeff_a_d2 = -(float)norme_PM;
			constante_d1 = (float)(mur.y - coeff_a_d1 * mur.x);
			constante_d2 = (float)(mur.y - coeff_a_d2 * mur.x);

			//printf("Valeur du coeuf d1 = %f , d2 = %f \n", coeff_a_d1, coeff_a_d2);
			//printf("Valeur du constant cd1 = %f , cd2 = %f \n", constante_d1, constante_d2);

			//Tracage des segments
			segment_t d1;
			d1.pointA.x = (float)(mur.x);
			d1.pointA.y = (float)(d1.pointA.x * coeff_a_d1 + constante_d1); 
			d1.pointB.x = (float)(perso.x + portee_max);
			d1.pointB.y = (float)(d1.pointB.x * coeff_a_d1 + constante_d1);

			//printf("Segment d1 , point A [%f,%f], point B [%f,%f]\n", d1.pointA.x, d1.pointA.y, d1.pointB.x, d1.pointB.y);

			segment_t d2;
			d2.pointA.x = (float)(mur.x);
			d2.pointA.y = (float)(d2.pointA.x * coeff_a_d2 + constante_d2); 
			d2.pointB.x = (float)(perso.x + portee_max);
			d2.pointB.y = (float)(d2.pointB.x * coeff_a_d2 + constante_d2);			

			//printf("Segment d2 , point A [%f,%f], point B [%f,%f]\n", d2.pointA.x, d2.pointA.y, d2.pointB.x, d2.pointB.y);
			
			for(j = mur.y; j < perso.y + portee_max ; j++)
			{
				pos_d1.y = i;
				pos_d1.x = round_value( x_affine(coeff_a_d1, constante_d1, (float)j) );
				
				pos_d2.y = i;
				pos_d2.x = round_value( x_affine(coeff_a_d2, constante_d2, (float)j) );
				//printf("Chercher entre d1 : [%i,%i] , d2 : [%i,%i]\n", pos_d1.x, pos_d1.y, pos_d2.x, pos_d2.y);
	
				for(i = pos_d2.x; i <= pos_d1.x; i++)
				{	

					if(i >= 0 && i < N && j >= 0 && j < M)
					{
						if(map[i][j].type_emplacement != MUR) 
						{
							//map[i][j].type_emplacement = REPERE_LDV;
							pos_ajouter.x = i;
							pos_ajouter.y = j;
							liste_positions_ajouter( liste_positions, pos_ajouter, indice_liste );
							indice_liste++;
						}
					}
				}
			}
		}
		else
		{
			//Norme PM
			norme_PM = perso.y - mur.y;
			//Définition des facteurs du polynome
			coeff_a_d1 = (float)norme_PM;
			coeff_a_d2 = -(float)norme_PM;
			constante_d1 = (float)(mur.y - coeff_a_d1 * mur.x);
			constante_d2 = (float)(mur.y - coeff_a_d2 * mur.x);

			//printf("Valeur du coeuf d1 = %f , d2 = %f \n", coeff_a_d1, coeff_a_d2);
			//printf("Valeur du constant cd1 = %f , cd2 = %f \n", constante_d1, constante_d2);

			//Tracage des segments
			segment_t d1;
			d1.pointA.x = (float)(mur.x);
			d1.pointA.y = (float)(d1.pointA.x * coeff_a_d1 + constante_d1); 
			d1.pointB.x = (float)(perso.x + portee_max);
			d1.pointB.y = (float)(d1.pointB.x * coeff_a_d1 + constante_d1);

			//printf("Segment d1 , point A [%f,%f], point B [%f,%f]\n", d1.pointA.x, d1.pointA.y, d1.pointB.x, d1.pointB.y);

			segment_t d2;
			d2.pointA.x = (float)(mur.x);
			d2.pointA.y = (float)(d2.pointA.x * coeff_a_d2 + constante_d2); 
			d2.pointB.x = (float)(perso.x + portee_max);
			d2.pointB.y = (float)(d2.pointB.x * coeff_a_d2 + constante_d2);			

			//printf("Segment d2 , point A [%f,%f], point B [%f,%f]\n", d2.pointA.x, d2.pointA.y, d2.pointB.x, d2.pointB.y);
			
			for(j = mur.y; j > perso.y - portee_max ; j--)
			{
				pos_d1.y = i;
				pos_d1.x = round_value( x_affine(coeff_a_d1, constante_d1, (float)j) );
				
				pos_d2.y = i;
				pos_d2.x = round_value( x_affine(coeff_a_d2, constante_d2, (float)j) );
				//printf("Chercher entre d1 : [%i,%i] , d2 : [%i,%i]\n", pos_d1.x, pos_d1.y, pos_d2.x, pos_d2.y);
	
				for(i = pos_d1.x; i <= pos_d2.x; i++)
				{	

					if(i >= 0 && i < N && j >= 0 && j < M)
					{
						if(map[i][j].type_emplacement != MUR) 
						{
							//map[i][j].type_emplacement = REPERE_LDV;
							pos_ajouter.x = i;
							pos_ajouter.y = j;
							liste_positions_ajouter( liste_positions, pos_ajouter, indice_liste );
							indice_liste++;
						}
					}
				}
			}
		}
	}
	else if(perso.y == mur.y)//Cas particulier ou le perso et le mur sont aligné sur x
	{
		
		
		if(perso.x < mur.x)
		{
			//Norme PM
			norme_PM = mur.x - perso.x;
			//Définition des facteurs du polynome
			coeff_a_d1 = 1 / (float)norme_PM;
			coeff_a_d2 = -1 / (float)norme_PM;
			constante_d1 = (float)(mur.y - coeff_a_d1 * mur.x);
			constante_d2 = (float)(mur.y - coeff_a_d2 * mur.x);

			//printf("Valeur du coeuf d1 = %f , d2 = %f \n", coeff_a_d1, coeff_a_d2);
			//printf("Valeur du constant cd1 = %f , cd2 = %f \n", constante_d1, constante_d2);

			//Tracage des segments
			segment_t d1;
			d1.pointA.x = (float)(mur.x);
			d1.pointA.y = (float)(d1.pointA.x * coeff_a_d1 + constante_d1); 
			d1.pointB.x = (float)(perso.x + portee_max);
			d1.pointB.y = (float)(d1.pointB.x * coeff_a_d1 + constante_d1);

			//printf("Segment d1 , point A [%f,%f], point B [%f,%f]\n", d1.pointA.x, d1.pointA.y, d1.pointB.x, d1.pointB.y);

			segment_t d2;
			d2.pointA.x = (float)(mur.x);
			d2.pointA.y = (float)(d2.pointA.x * coeff_a_d2 + constante_d2); 
			d2.pointB.x = (float)(perso.x + portee_max);
			d2.pointB.y = (float)(d2.pointB.x * coeff_a_d2 + constante_d2);			

			//printf("Segment d2 , point A [%f,%f], point B [%f,%f]\n", d2.pointA.x, d2.pointA.y, d2.pointB.x, d2.pointB.y);
			
			
			for(i = mur.x; i < perso.x + portee_max ; i++)
			{
				pos_d1.x = i;
				pos_d1.y = round_value( affine(coeff_a_d1, constante_d1, (float)i) );
				
				pos_d2.x = i;
				pos_d2.y = round_value( affine(coeff_a_d2, constante_d2, (float)i) );
				//printf("Chercher entre d1 : [%i,%i] , d2 : [%i,%i]\n", pos_d1.x, pos_d1.y, pos_d2.x, pos_d2.y);
	
				//printf("cd1 : %f, cd2 : %f \n", coeff_a_d1, coeff_a_d2);
				

				for(j = pos_d2.y; j <= pos_d1.y; j++)
				{	

					if(i >= 0 && i < N && j >= 0 && j < M)
					{
						if(map[i][j].type_emplacement != MUR) 
						{
							//map[i][j].type_emplacement = REPERE_LDV;
							pos_ajouter.x = i;
							pos_ajouter.y = j;
							liste_positions_ajouter( liste_positions, pos_ajouter, indice_liste );
							indice_liste++;
						}
					}
				}
			}
		}
		else
		{
			//Norme PM
			norme_PM = perso.x - mur.x;
			//Définition des facteurs du polynome
			coeff_a_d1 = 1 / (float)norme_PM;
			coeff_a_d2 = -1 / (float)norme_PM;
			constante_d1 = (float)(mur.y - coeff_a_d1 * mur.x);
			constante_d2 = (float)(mur.y - coeff_a_d2 * mur.x);

			//printf("Valeur du coeuf d1 = %f , d2 = %f \n", coeff_a_d1, coeff_a_d2);
			//printf("Valeur du constant cd1 = %f , cd2 = %f \n", constante_d1, constante_d2);

			//Tracage des segments
			segment_t d1;
			d1.pointA.x = (float)(mur.x);
			d1.pointA.y = (float)(d1.pointA.x * coeff_a_d1 + constante_d1); 
			d1.pointB.x = (float)(perso.x + portee_max);
			d1.pointB.y = (float)(d1.pointB.x * coeff_a_d1 + constante_d1);

			//printf("Segment d1 , point A [%f,%f], point B [%f,%f]\n", d1.pointA.x, d1.pointA.y, d1.pointB.x, d1.pointB.y);

			segment_t d2;
			d2.pointA.x = (float)(mur.x);
			d2.pointA.y = (float)(d2.pointA.x * coeff_a_d2 + constante_d2); 
			d2.pointB.x = (float)(perso.x + portee_max);
			d2.pointB.y = (float)(d2.pointB.x * coeff_a_d2 + constante_d2);			

			//printf("Segment d2 , point A [%f,%f], point B [%f,%f]\n", d2.pointA.x, d2.pointA.y, d2.pointB.x, d2.pointB.y);
			
			for(i = mur.x; i > perso.x - portee_max ; i--)
			{
				pos_d1.x = i;
				pos_d1.y = round_value( affine(coeff_a_d1, constante_d1, (float)i) );
				
				pos_d2.x = i;
				pos_d2.y = round_value( affine(coeff_a_d2, constante_d2, (float)i) );
				//printf("Chercher entre d1 : [%i,%i] , d2 : [%i,%i]\n", pos_d1.x, pos_d1.y, pos_d2.x, pos_d2.y);
					
				for(j = pos_d1.y; j <= pos_d2.y; j++)
				{	
					if(i >= 0 && i < N && j >= 0 && j < M)
					{
						if(map[i][j].type_emplacement != MUR) 
						{
							//map[i][j].type_emplacement = REPERE_LDV;
							pos_ajouter.x = i;
							pos_ajouter.y = j;
							liste_positions_ajouter( liste_positions, pos_ajouter, indice_liste );
							indice_liste++;
						}
					}
				}
			}
		}	
	}
	else
	{
		//Définition des vecteurs
		vecteur_t v;
		v.x = (float)(mur.x - perso.x);
		v.y = (float)(mur.y - perso.y);

		vecteur_t w;
		w.x = v.x / 2;
		w.y = v.y;

		vecteur_t u;
		u.x = v.x;
		u.y = v.y / 2;

		//Définition des facteurs du polynome
		coeff_a_d1 = w.y * (1/w.x);
		coeff_a_d2 = u.y * (1/u.x);
		constante_d1 = (float)(mur.y - coeff_a_d1 * mur.x);
		constante_d2 = (float)(mur.y - coeff_a_d2 * mur.x);
			
		//Fonctionne pour ce cas là	
		if( (coeff_a_d1 > 0 && coeff_a_d2 > 0) || coeff_a_d1 == coeff_a_d2)
		{
			if(perso.y < mur.y)
			{
				//Tracage des segments
				segment_t d1;
				d1.pointA.x = (float)(mur.x);
				d1.pointA.y = (float)(d1.pointA.x * coeff_a_d1 + constante_d1); 
				d1.pointB.x = (float)(perso.x + portee_max);
				d1.pointB.y = (float)(d1.pointB.x * coeff_a_d1 + constante_d1);

				//printf("Segment d1 , point A [%f,%f], point B [%f,%f]\n", d1.pointA.x, d1.pointA.y, d1.pointB.x, d1.pointB.y);

				segment_t d2;
				d2.pointA.x = (float)(mur.x);
				d2.pointA.y = (float)(d2.pointA.x * coeff_a_d2 + constante_d2); 
				d2.pointB.x = (float)(perso.x + portee_max);
				d2.pointB.y = (float)(d2.pointB.x * coeff_a_d2 + constante_d2);

				//printf("Segment d2 , point A [%f,%f], point B [%f,%f]\n", d2.pointA.x, d2.pointA.y, d2.pointB.x, d2.pointB.y);
				//printf("Cas 1\n");

							

				for(i = mur.x; i < perso.x + portee_max; i++)
				{
					pos_d1.x = i;
					pos_d1.y = round_value( affine(coeff_a_d1, constante_d1, (float)i) );
					
					pos_d2.x = i;
					pos_d2.y = round_value( affine(coeff_a_d2, constante_d2, (float)i) );
					//printf("Chercher entre d1 : [%i,%i] , d2 : [%i,%i]\n", pos_d1.x, pos_d1.y, pos_d2.x, pos_d2.y);
					
					if(mur.x - perso.x > mur.y - perso.y)
					{
						//printf("TST\n");
						pos_d2.y = pos_d2.y + 0;
					}
					else if(mur.x - perso.x < mur.y - perso.y)
					{
						//printf("No\n");
						pos_d1.y = pos_d1.y + 1;
					}	
					else
					{
						pos_d2.y = pos_d2.y + 1;
					}

					for(j = pos_d2.y; j <= pos_d1.y; j++)
					{
						if(i >= 0 && i < N && j >= 0 && j < M)//Sécurité bord de MAP
						{
							//TRAITEMENT A EFFECTUER
							if(map[i][j].type_emplacement != MUR) 
							{
								//map[i][j].type_emplacement = REPERE_LDV;
								pos_ajouter.x = i;
								pos_ajouter.y = j;
								liste_positions_ajouter( liste_positions, pos_ajouter, indice_liste );
								indice_liste++;
							}
						}
					}
				}
			}
			
			if(perso.y > mur.y)
			{
				//Tracage des segments
				segment_t d1;
				d1.pointA.x = (float)(mur.x);
				d1.pointA.y = (float)(d1.pointA.x * coeff_a_d1 + constante_d1); 
				d1.pointB.x = (float)(perso.x - portee_max);
				d1.pointB.y = (float)(d1.pointB.x * coeff_a_d1 + constante_d1);

				//printf("Segment d1 , point A [%f,%f], point B [%f,%f]\n", d1.pointA.x, d1.pointA.y, d1.pointB.x, d1.pointB.y);

				segment_t d2;
				d2.pointA.x = (float)(mur.x);
				d2.pointA.y = (float)(d2.pointA.x * coeff_a_d2 + constante_d2); 
				d2.pointB.x = (float)(perso.x - portee_max);
				d2.pointB.y = (float)(d2.pointB.x * coeff_a_d2 + constante_d2);

				//printf("Segment d2 , point A [%f,%f], point B [%f,%f]\n", d2.pointA.x, d2.pointA.y, d2.pointB.x, d2.pointB.y);

				for(i = mur.x; i > perso.x - portee_max; i--)
				{
					pos_d1.x = i;
					pos_d1.y = round_value( affine(coeff_a_d1, constante_d1, (float)i) );
					
					pos_d2.x = i;
					pos_d2.y = round_value( affine(coeff_a_d2, constante_d2, (float)i) );
					//printf("Cas 2\n");
					
					//printf("pos_d1.x = %i, pos_d2.x = %i\n", pos_d1.x, pos_d2.x);
					//printf("pos_d1.y = %i, pos_d2.y = %i\n", pos_d1.y, pos_d2.y);
					if(perso.x - mur.x > perso.y - mur.y)
					{
						//printf("TST\n");
						pos_d1.y = pos_d1.y + 0;
					}
					else if(perso.x - mur.x < perso.y - mur.y)
					{
						//printf("No\n");
						pos_d1.y = pos_d1.y - 1;
					}
					

					for(j = pos_d1.y; j <= pos_d2.y; j++)
					{
						
						if(i >= 0 && i < N && j >= 0 && j < M)//Sécurité bord de MAP
						{
							if(map[i][j].type_emplacement != MUR) 
							{
								//map[i][j].type_emplacement = REPERE_LDV;
								pos_ajouter.x = i;
								pos_ajouter.y = j;
								liste_positions_ajouter( liste_positions, pos_ajouter, indice_liste );
								indice_liste++;
							}
						}
					}
					
				
				}
			}
	
			
			
		}	
		
		if( (coeff_a_d1 < 0 && coeff_a_d2 < 0) || coeff_a_d1 == coeff_a_d2)
		{
			if(perso.y < mur.y)
			{
				
				//Tracage des segments
				segment_t d1;
				d1.pointA.x = (float)(mur.x);
				d1.pointA.y = (float)(d1.pointA.x * coeff_a_d1 + constante_d1); 
				d1.pointB.x = (float)(perso.x - portee_max);
				d1.pointB.y = (float)(d1.pointB.x * coeff_a_d1 + constante_d1);

				//printf("Segment d1 , point A [%f,%f], point B [%f,%f]\n", d1.pointA.x, d1.pointA.y, d1.pointB.x, d1.pointB.y);

				segment_t d2;
				d2.pointA.x = (float)(mur.x);
				d2.pointA.y = (float)(d2.pointA.x * coeff_a_d2 + constante_d2); 
				d2.pointB.x = (float)(perso.x - portee_max);
				d2.pointB.y = (float)(d2.pointB.x * coeff_a_d2 + constante_d2);

				//printf("Segment d2 , point A [%f,%f], point B [%f,%f]\n", d2.pointA.x, d2.pointA.y, d2.pointB.x, d2.pointB.y);

				for(i = mur.x; i > perso.x - portee_max; i--)
				{
					pos_d1.x = i;
					pos_d1.y = round_value( affine(coeff_a_d1, constante_d1, (float)i) );
					
					pos_d2.x = i;
					pos_d2.y = round_value( affine(coeff_a_d2, constante_d2, (float)i) );
					//printf("Cas 2\n");

					if(perso.x - mur.x > mur.y - perso.y)
					{
						
						pos_d2.y = pos_d2.y + 0;
					}
					else if(perso.x - mur.x < mur.y - perso.y)
					{
						
						pos_d1.y = pos_d1.y + 1;
					}	
					else
					{
						pos_d2.y = pos_d2.y + 1;
					}

					for(j = pos_d2.y; j <= pos_d1.y; j++)
					{
						if(i >= 0 && i < N && j >= 0 && j < M)//Sécurité bord de MAP
						{
							if(map[i][j].type_emplacement != MUR) 
							{
								//map[i][j].po_attaque = 1;
								pos_ajouter.x = i;
								pos_ajouter.y = j;
								liste_positions_ajouter( liste_positions, pos_ajouter, indice_liste );
								indice_liste++;
							}
						}
					}
				}
			}
			
			if(perso.y > mur.y)
			{
				//Tracage des segments
				//printf("CAS 3\n");
				segment_t d1;
				d1.pointA.x = (float)(mur.x);
				d1.pointA.y = (float)(d1.pointA.x * coeff_a_d1 + constante_d1); 
				d1.pointB.x = (float)(perso.x + portee_max);
				d1.pointB.y = (float)(d1.pointB.x * coeff_a_d1 + constante_d1);

				//printf("Segment d1 , point A [%f,%f], point B [%f,%f]\n", d1.pointA.x, d1.pointA.y, d1.pointB.x, d1.pointB.y);

				segment_t d2;
				d2.pointA.x = (float)(mur.x);
				d2.pointA.y = (float)(d2.pointA.x * coeff_a_d2 + constante_d2); 
				d2.pointB.x = (float)(perso.x + portee_max);
				d2.pointB.y = (float)(d2.pointB.x * coeff_a_d2 + constante_d2);

				//printf("Segment d2 , point A [%f,%f], point B [%f,%f]\n", d2.pointA.x, d2.pointA.y, d2.pointB.x, d2.pointB.y);

				for(i = mur.x; i < perso.x + portee_max; i++)
				{
					pos_d1.x = i;
					pos_d1.y = round_value( affine(coeff_a_d1, constante_d1, (float)i) );
					
					pos_d2.x = i;
					pos_d2.y = round_value( affine(coeff_a_d2, constante_d2, (float)i) );
					//printf("Cas 2\n");

					if(mur.x - perso.x > perso.y - mur.y)
					{
						pos_d1.y = pos_d1.y + 0;
					}
					else if(mur.x - perso.x < perso.y - mur.y)
					{
						pos_d1.y = pos_d1.y - 1;
					}
					for(j = pos_d1.y; j <= pos_d2.y; j++)
					{
						if(i >= 0 && i < N && j >= 0 && j < M)//Sécurité bord de MAP
						{
							if(map[i][j].type_emplacement != MUR) 
							{
								//map[i][j].po_attaque = 1;
								pos_ajouter.x = i;
								pos_ajouter.y = j;
								liste_positions_ajouter( liste_positions, pos_ajouter, indice_liste );
								indice_liste++;
							}
						}
					}
				}
			}
		}
	}
	
	liste_positions = ajuster_taille_liste(liste_positions, indice_liste);
	//afficher_liste_positions_terminal(liste_positions);  //Test
	return liste_positions;
	
}

