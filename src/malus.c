/**
 * \file malus.c
 * \brief Fichier pour gerer les malus
 * \author
 * \version 1.0
 * \date
 *
 * Permet la creation la destrcutionet l'altération des malus
 *
 */

#include "../include/malus.h"

/*Définition des methodes*/


/*décremetation du temps des malus*/
/**
 * \fn int malus_decrementer(malus_t * malus)
 * \brief fonction qui decremente la duree du malus
 *
 * \param malus Type du malus
 *
 * \return int malus->duree
 *
 */
static
int malus_decrementer(malus_t * malus)
{
  /*Si le type de malus est dégats, alors j'enlève des pv à chaques decrementation*/
  if(malus->type_mal == poison)
    malus->cible->pv -= malus->malus;

  malus->duree--;
  return malus->duree;
}

/**
 * \fn err_t malus_detruire(malus_t ** malus)
 * \brief fonction qui detruit le malus
 *
 * \param malus Type du malus
 *
 * \return Ok
 *
 */
static
err_t malus_detruire(malus_t ** malus)
{
  /*Je libère la memoire et affecte NULL au pointeur*/
  (*malus)->cible = NULL;
  free(*malus);
  *malus = NULL;

  return (Ok);
}

static
void malus_afficher(malus_t * malus)
{
  printf("\nMalus\n");
  printf("\nduree : %d", malus->duree);
  printf("\nmalus : %d", malus->malus);
}

/*Définition des fonctions*/


/*Création d'un nouveau malus*/
/**
 * \fn malus_t * creer_malus(type_malus_t type, int duree, personnage_t * cible, int malus)
 * \brief fonction qui creer le malus et l'affecte a la cible
 *
 * \param type Type du malus
 * \param duree Duree du malus
 * \param cible Cible sur qui on applique le malus
 * \param malus dégats du malus
 *
 * \return nouveau_malus
 *
 */
extern
malus_t * creer_malus(type_malus_t type, int duree, personnage_t * cible, int malus)
{
  malus_t * nouveau_malus = malloc(sizeof(malus_t));
  nouveau_malus->type_mal = type;
  nouveau_malus->duree = duree;
  nouveau_malus->cible = cible;
  nouveau_malus->malus = malus;

  /*j'applique le malus dès que celui-ci est créé*/
  switch(type)
  {
    case bouclier:
      cible->resistance += malus;
      break;

    case poison:
      cible->pv -= malus;
      break;

    case degats:
      cible->degat += malus;
      break;

    case deplacement:
      cible->portee.deplacement += malus;
      break;

  }

  nouveau_malus->decrementer = malus_decrementer;
  nouveau_malus->detruire = malus_detruire;
  nouveau_malus->print = malus_afficher;

  return nouveau_malus;
}
