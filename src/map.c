#include "../include/map.h"
#include "../include/deplacements.h"
/**
 * \file map.c
 * \brief Fonctions liées à l'affichage de la carte et au placement des personnages
 * \author Yohann Delacroix
 * \version 1.0
 * \date 12 Février 2021
 * Le fichier map.c utilise la variable globale map définie dans map.h
 */


/**
 * \fn int tirage(void)
 * \brief Tirage au sort de la map
 * \return Un nombre aléatoire entre 0 et 4, qui correspond aux 5 maps disponibles
 */
int tirage()
{/*fonction de tirage d'une map premier tirage avec 1% de chance de tomber sur une map secrête de code 0 si on tombe pas dessus alors dexième tirage avec les maps normals
    0 code de la map secrete*/
	int min = 1;
	int max = 4;
	if ((rand()%(100-min+min)+min)==min)//si le tirage vaut 1 donc 1%
	{
		return 0;//renvoie du code de la map secrete
	}

	return rand()%(max-min+1)+min;
}

/**
 * \fn t_pos premiere_position_equipe_adverse(t_pos position, equipe_t couleur)
 * \brief Trouver la position de l'adversaire le plus proche
 * \param position Position du personnage servant de réference à la recherche
 * \param couleur Code couleur de l'équipe du personnage
 * \return Position de l'adversaire le plus proche dans le type t_pos
 */
t_pos premiere_position_equipe_adverse(t_pos position, equipe_t couleur)
{
	t_pos position_adversaire = {0,0};
	int taille_zone = 50;
	//Algorithme de parcours recherche
	int cov_y;
	int seek_x;
	int seek_y;
	int x, y;

	/* Algorithme de recherche des cases comprises dans la zone */
	for(x = -taille_zone; x <= taille_zone; x++)
	{
		cov_y = taille_zone - abs(x);
		for(y = -cov_y; y <= cov_y ; y++)
		{
			seek_x = position.x - x;
			seek_y = position.y - y;
			//printf("Recherche sur la case [%i,%i]\n", seek_x, seek_y);	//Debug

			if(seek_x >= 0 && seek_x < N && seek_y >=0 && seek_y < M)
			{
				if(map[seek_x][seek_y].type_emplacement == PERSO)
				{
					if(map[seek_x][seek_y].perso->equipe != couleur)
					{
						position_adversaire.x = map[seek_x][seek_y].perso->pos_x;
						position_adversaire.y = map[seek_x][seek_y].perso->pos_y;
						//printf("Position adversaire de [%i,%i] est [%i,%i]\n", position.x, position.y, position_adversaire.x, position_adversaire.y);
						return position_adversaire;
					}
				}
			}
		}
	}
	//Recherche un personnage de couleur opposée

	return position_adversaire;
}


/**
 * \fn void initialiser_orientation_personnages(liste_perso_t * equipe)
 * \brief Oriente les personnages d'une équipe face à l'équipe adverse
 * \param equipe Liste de personnages d'une équipe
 * \return Aucune valeur de retour
 */
void initialiser_orientation_personnages(liste_perso_t * equipe)
{
	int i;
	t_pos pos_perso;
	personnage_t * perso;
	t_pos pos_adversaire;

	for(i = 0; i < equipe->nb; i++)
	{
		perso = equipe->liste[i];
		pos_perso.x = perso->pos_x;
		pos_perso.y = perso->pos_y;
		pos_adversaire = premiere_position_equipe_adverse(pos_perso, perso->equipe);
		//printf("Position adversaire de [%i,%i] est [%i,%i]\n", pos_perso.x, pos_perso.y, pos_adversaire.x, pos_adversaire.y);
		changer_orientation_personnage(equipe->liste[i], pos_adversaire);
	}

}

/**
 * \fn void placer_equipes(liste_perso_t * equipe_rouge, liste_perso_t * equipe_bleue)
 * \brief Place les personnages sur la map
 * \param equipe_rouge Liste de personnages de l'équipe rouge
 * \param equipe_bleue Liste de personnages de l'équipe bleue
 * \return Aucune valeur de retour
 *
 * Cette fonction place les personnages de chaque équipe sur les cases de départ qui leur est destinée.
 * Elle agit sur le champ type_emplacement de la structure t_carte en remplaçant POSITION_ROUGE et POSITION_BLEUE par PERSO
 * Elle appelle la fonction d'orientation des personnages pour que les personnages se fassent face
 */
void placer_equipes(liste_perso_t * equipe_rouge, liste_perso_t * equipe_bleue)
{
	int i = 0;
	int j = 0;
	int indice_bleue = 0;
	int indice_rouge = 0;

	for(i = 0; i < N; i++)
	{
		for(j = 0; j < M; j++)
		{
			switch(map[i][j].type_emplacement)
			{

				case POSITION_ROUGE:
					if(equipe_rouge->nb > indice_rouge)
					{
						equipe_rouge->liste[indice_rouge]->pos_x = i;
						equipe_rouge->liste[indice_rouge]->pos_y = j;
						map[i][j].type_emplacement = PERSO;
						equipe_rouge->liste[indice_rouge]->equipe=Rouge;
						map[i][j].perso = equipe_rouge->liste[indice_rouge];
					}
					indice_rouge++;
					break;
				case POSITION_BLEUE:
					if(equipe_bleue->nb > indice_bleue)
					{
						equipe_bleue->liste[indice_bleue]->pos_x = i;
						equipe_bleue->liste[indice_bleue]->pos_y = j;
						map[i][j].type_emplacement = PERSO;
						equipe_bleue->liste[indice_bleue]->equipe=Bleu;
						map[i][j].perso = equipe_bleue->liste[indice_bleue];
					}
					indice_bleue++;
					break;
				default:
				  //Ne rien faire
					break;
			}
		}
	}

	initialiser_orientation_personnages(equipe_rouge);
	initialiser_orientation_personnages(equipe_bleue);
}


/**
 * \fn void afficher_plateau(t_affichage_map mode_affichage)
 * \brief Affiche le terrain de jeu en mode console
 * \param mode_affichage Définit le type d'affichage de la map
 * \return Aucune valeur de retour
 *
 * En mode BINAIRE, affiche les valeurs entieres du champ type_emplacement contenue dans les variables
 * En mode VISUEL, interprete les valeurs de la variable map et les adapte visuellement
 */
void afficher_plateau(t_affichage_map mode_affichage)
{
	int i, j;
	//Affichage du bord supérieur
	printf("      ");
	for(i = 0; i < M; i++)
	{
		if(i > 9) printf(" %i", i);
		else printf(" %i ", i);
	}
	printf("\n\n");
	printf("      ");
	for(i = 0; i < M; i++)
	{
		printf("---");
	}
	printf("\n");
	//Fin affichage du bord supérieur



	for(i = 0; i < N; i++)
	{
		//Affichage du bord gauche
		if(i < 10) printf(" %i   |", i);
		else printf(" %i  |", i);
		//Fin affichage du bord gauche

		for(j = 0; j < M; j++)
		{
			switch(map[i][j].type_emplacement)
			{
				case VIDE:
					if(mode_affichage == BINAIRE) printf(" %i ", map[i][j].type_emplacement);
					else
					{
						if(map[i][j].po_deplacement == 1 || map[i][j].po_attaque == 1)
						{
							printf(" . ");
						}
						else
						{
							printf("   ");

						}

					}
					break;
				case MUR:
					if(mode_affichage == BINAIRE) printf("%i ", map[i][j].type_emplacement);
					else printf(" x ");
					break;
				case PERSO:
					if(mode_affichage == VISUEL)
					{
						if(map[i][j].perso->equipe == Rouge)
						{
							printf(" R ");
						}
						else
						{
							printf(" B ");
						}
					}
					else printf("%i ", map[i][j].type_emplacement);
					break;
				case POSITION_ROUGE:
					if(mode_affichage == VISUEL) printf(" R ");
					else printf("%i ", map[i][j].type_emplacement);
					break;
				case POSITION_BLEUE:
					if(mode_affichage == VISUEL) printf(" B ");
					else printf("%i ", map[i][j].type_emplacement);
					break;
				default:
					printf(" . ");
					break;
			}
		}
		//Affichage du bord droit
		if(i < 10) printf(" |   %i", i);
		else printf(" |  %i", i);
		//Fin affichage du bord droit
		printf("\n");
	}
}


/**
 * \fn int chargement_carte()
 * \brief Charge une carte en mémoire à partir d'un fichier texte
 * \return En cas d'erreur, retourne le code erreur 1, sinon retourne 0
 *
 * Utilise la fonction de tirage des maps pour définir quelle est la map à charger
 */
int chargement_carte() //Renvoie 0 si ok, 1 si erreur
{
	FILE * map_file;
	map_file = NULL;

	//Récupération du tirage au sort de la map
	int tirage_map = tirage();//PENSEZ A REMETRE TIRAGE APRES FIN DES TEST tirage()
	printf("Choix de la map %i\n", tirage_map);

	switch(tirage_map) //Ouverture du fichier map correspondant
	{
		case 0:
			map_file = fopen("../maps/special_map.txt", "r");
			printf("Map Special\n");
			break;
		case 1:
			map_file = fopen("../maps/evan_map.txt", "r");
			printf("Map Evan\n");
			break;
		case 2:
			map_file = fopen("../maps/nico_map.txt", "r");
			printf("Map Nico\n");
			break;
		case 3:
			map_file = fopen("../maps/pierre_map.txt", "r");
			printf("Map Pierre\n");
			break;
		case 4:
			map_file = fopen("../maps/yohann_map.txt", "r");
			printf("Map Yohann\n");
			break;
		default:
			fprintf(stderr, "La fonction de tirage n'a pas renvoyé une valeur correcte : %d , elle doit être comprise entre 1 et 4 \n", tirage_map);
			exit(0);
			break;
	}


	if(map_file != NULL)
	{
		int valeur;
		int i =0;
		int j= 0;
		fscanf(map_file, "%i", &valeur);
		while(!feof(map_file))
		{
			map[i][j].type_emplacement = valeur;
			map[i][j].po_deplacement = 0;
			map[i][j].po_attaque = 0;
			if(i < N)
			{
				j++;
			}
			else
			{
				i++;
				j = 0;
			}
			fscanf(map_file,"%i", &valeur);
		}
	}
	else
	{
		fprintf(stderr, "L'ouverture du fichier a échoué\n");

		return(1);
	}

	fclose(map_file);
	return 0;

}
