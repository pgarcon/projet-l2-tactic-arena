/**
*  \file archer.c
*  \brief classe archer
*  \details cette classe contient les fonctions et methodes disponibles pour la classe archer
*  Un archer contient les caractéristiques suivantes :
*   - pv = 45;
*   - degat = 10;
*   - portee de deplacement = 3
*   - portee d'attaques = 5
*   - esquive = 0.60
*   - crit = 0.25
*   - resistance = 3
*   - precision = 1.00
*  \author Pierre garcon
*  \date 06 fevrier
*  \fn int existe_archer, void afficher_archer, void position_archer, err_t detruire_archer, archer_t creer_archer
*/




/*

  IMPORTANT : definir une fonction qui remet à 1 les points d'attaques et de deplacements

*/

#include <stdio.h>
#include <stdlib.h>
#include "../../include/archer.h"
#include "../../include/personnage.h"

/*On défini la portée maximale des attaques et des déplacements*/
#define DEP 3
#define ATT 5


/*Définition des fonctions et méthodes*/
/**
 * \fn int existe_archer(archer_t * arc)
 * \brief fonction qui test si un archer existe
 *
 * \param arc Archer à tester
 *
 * \return int 0 | 1
 *
 */
extern
int existe_archer(archer_t * arc)
{
  if(arc == NULL)
    return(0);
  else
    return(1);
}

/*S'occupe de l'affuche de tous les parametres d'un archer*/
/**
 * \fn void afficher_archer(archer_t * arc)
 * \brief fonction qui affiche toute les caracteristique d'un archer
 *
 * \param arc Archer à afficher
 *
 * \return void pas de retour
 *
 */
static
void afficher_archer(archer_t * arc)
{
  if(existe_archer(arc))
  {
    if(arc->etat == Vivant)
    {
      printf("\nNom classe : Archer");
      printf("\nNom du personnage : %s", arc->nom);
      printf("\nPV : %d", arc->pv);
      printf("\nDegats : %d", arc->degat);
      printf("\nDeplacements : %d", arc->portee.deplacement);
      printf("\nattaque : %d", arc->portee.attaques);
      printf("\nEsquive : %f", arc->esquive);
      printf("\ncritique : %f", arc->crit);
      printf("\nResistance : %d", arc->resistance);
      printf("\nPrecision : %f", arc->precision);
      if(arc->equipe == Bleu)
      {
        printf("\nL'archer est dans l'equipe des bleus");
      }
      else
      {
        printf("\nL'archer est dans l'equipe des rouges");
      }
      printf("\nFin des stat\n");
    }
    else
    {
      printf("\n\nL'archer est mort \n\n");
    }
  }
  else
  {
    printf("\nL'archer n'existe plus");
  }

}




/*Récuperer la vie d'un archer*/
/**
 * \fn int vie_archer(archer_t * arc)
 * \brief fonction qui recupere le nombre de pv d'un archer
 *
 * \param arc Archer à tester
 *
 * \return int arc->pv
 *
 */
static
int vie_archer(archer_t * arc)
{
  return arc->pv;
}





/*
*Fonctions pour changer la direction des personnages
*/

/**
 * \fn void assassin_haut(assassin_t * ass)
 * \brief fonction qui met la direction de l'assassin à haut
 *
 * \param ass archer à tester
 *
 * \return self
 *
 */
static
void archer_haut(archer_t * arc)
{
  arc->direction = haut;
}


/**
 * \fn void assassin_bas(assassin_t * ass)
 * \brief fonction qui met la direction de l'assassin à bas
 *
 * \param ass archer à tester
 *
 * \return self
 *
 */
static
void archer_bas(archer_t * arc)
{
  arc->direction = bas;
}


/**
 * \fn void assassin_droite(assassin_t * ass)
 * \brief fonction qui met la direction de l'assassin à droite
 *
 * \param ass archer à tester
 *
 * \return self
 *
 */
static
void archer_droite(archer_t * arc)
{
  arc->direction = droite;
}

/**
 * \fn void assassin_gauche(assassin_t * ass)
 * \brief fonction qui met la direction de l'assassin à gauche
 *
 * \param ass archer à tester
 *
 * \return self
 *
 */
static
void archer_gauche(archer_t * arc)
{
  arc->direction = gauche;
}




/*Récuperer les points d'attaque*/
/**
 * \fn int points_attaque_archer(archer_t * arc)
 * \brief fonction qui recupere le nombre de point d'attaque d'un archer
 *
 * \param arc Archer à tester
 *
 * \return int arc->pt_attaque
 *
 */
static
int points_attaque_archer(archer_t * arc)
{
  return arc->pt_attaque;
}





/*Récuperer les points de deplacement*/
/**
 * \fn int points_deplacement_archer(archer_t * arc)
 * \brief fonction qui recupere le nombre de point de deplacement d'un archer
 *
 * \param arc Archer à tester
 *
 * \return int arc->pt_deplacement
 *
 */
static
int points_deplacement_archer(archer_t * arc)
{
  return arc->pt_deplacement;
}


/*Récuperer les points de deplacement*/
/**
 * \fn int etat_archer(archer_t * arc)
 * \brief fonction qui recupere si un archer est vivant ou pas
 *
 * \param arc Archer à tester
 *
 * \return int arc->etat
 *
 */
static
int etat_archer(archer_t * arc)
{
  return arc->etat;
}





/*Récuperer les points de deplacement*/
/**
 * \fn int portee_deplacement_archer(archer_t * arc)
 * \brief fonction qui recupere la portee de deplacement d'un archer
 *
 * \param arc Archer à tester
 *
 * \return int arc->portee.deplacement
 *
 */
static
int portee_deplacement_archer(archer_t * arc)
{
  return arc->portee.deplacement;
}


/*Récuperer les points de deplacement*/
/**
 * \fn int portee_attaque_archer(archer_t * arc)
 * \brief fonction qui recupere la portee de d'attaque d'un archer
 *
 * \param arc Archer à tester
 *
 * \return int arc->portee.attaques
 *
 */
static
int portee_attaque_archer(archer_t * arc)
{
  return arc->portee.attaques;
}



/*Récuperer l'esquive de l'archer*/
/**
 * \fn float esquive_archer(archer_t * arc)
 * \brief fonction qui recupere la chance d'esquive d'un archer
 *
 * \param arc Archer à tester
 *
 * \return float arc->esquive
 *
 */
static
float esquive_archer(archer_t * arc)
{
  return arc->esquive;
}




/*savoir si la compétence spéciale de l'archer est disponible*/
/**
 * \fn bool comp_spe_archer(archer_t * arc)
 * \brief fonction qui recupere si la compétance spéciale d'un archer est disponible
 *
 * \param arc Archer à tester
 *
 * \return bool arc->comp_special
 *
 */
static
bool comp_spe_archer(archer_t * arc)
{
  return arc->comp_special;
}




/*l'archer attaque*/







/*Récuperer la position de l'acher*/
/**
 * \fn void position_archer(int *x, int *y, archer_t * arc)
 * \brief fonction qui recupere la position d'un archer
 *
 * \param x Position X d'un archer
 * \param y Position Y d'un archer
 * \param arc Archer à tester
 *
 * \return void pas de retour
 *
 */
static
t_pos position_archer(archer_t * arc)
{
  t_pos pos;
  pos.x = 0;
  pos.y = 0;
  if(existe_archer(arc))
  {
    if(arc->etat == Vivant)
    {
      pos.x = arc->pos_x;
      pos.y = arc->pos_y;
    }
  }
  return pos;
}





/*Récuperer l'équipe d'un archer'*/
/**
 * \fn equipe_t * equipe_archer(archer_t * arc)
 * \brief fonction qui recupere l'équipe d'un archer
 *
 * \param arc Archer à tester
 *
 * \return arc->equipe
 *
 */
static
equipe_t equipe_archer(archer_t * arc)
{
  return arc->equipe;
}



/*Récuperer la precision d'un archer*/
/**
 * \fn float precision_archer(archer_t * arc)
 * \brief fonction qui recupere la precision d'un archer
 *
 * \param arc Archer à tester
 *
 * \return float arc->precision
 *
 */
static
float precision_archer(archer_t * arc)
{
  return arc->precision;
}







/*Modifier la position de l'acher*/
/**
 * \fn void modifier_position_archer(int x, int y, archer_t * arc)
 * \brief fonction qui permet de deplacer un archer
 *
 * \param x Position X d'un archer
 * \param y Position Y d'un archer
 * \param arc Archer à tester
 *
 * \return void pas de retour
 *
 */
static
void modifier_position_archer(int x, int y, archer_t * arc)
{
  if(existe_archer(arc))
  {
    if(arc->etat == Vivant)
    {
      if(arc->pt_deplacement != 0)
      {
        arc->pos_x = x;
        arc->pos_y = y;
        arc->pt_deplacement--;
      }
    }
  }
}








/*Diminue les PV d'un archer selon les dégats subis*/
/**
 * \fn etat_t perdre_vie_archer(int degat, archer_t * arc)
 * \brief fonction qui effectue des degat sur un archer et diminue leur pv
 *
 * \param degat Nombre de degat a inflicher au archer
 * \param arc Archer à tester
 *
 * \return Vivant | Mort | Rien
 *
 */
static
etat_t perdre_vie_archer(int degat, archer_t * arc)
{
  if(existe_archer(arc))
  {
    arc->pv = (arc->pv)-(degat-(arc->resistance));
    if((arc->pv)<=0)
    {
        arc->etat = Mort;
        return Mort;
    }
    else
    {
        return Vivant;
    }

  }
  return  Rien;
}

/*permet de detruire un archer*/
/**
 * \fn err_t detruire_archer(archer_t ** arc)
 * \brief fonction qui detruit une structure archer
 *
 * \param arc Archer à detruire
 *
 * \return Ok
 *
 */
static
err_t detruire_archer(archer_t ** arc)
{
  free((*arc));
  (*arc) = NULL;

  return(Ok);
}


/*On créer un nouvel archer, il n'y a pas de parametres car toutes les stats sont pré-défini*/
/**
 * \fn archer_t * creer_archer(equipe_t * equipe, race_t * race)
 * \brief fonction qui creer une structure archer
 *
 * \param equipe Dans lequel ajouter l'archer
 * \param race De l'archer
 *
 * \return nouveau_arc
 *
 */
extern
archer_t * creer_archer(equipe_t equipe, race_t * race, liste_perso_t * liste)
{
  archer_t * nouveau_arc = malloc(sizeof(archer_t));
  char nom[25];

  donner_nom(nom);

  /*Création des caractéristiques des archers*/
  nouveau_arc->classe = archer;
  strcpy(nouveau_arc->nom, nom);
  nouveau_arc->pv = 45;
  nouveau_arc->pv_max = 45;
  nouveau_arc->degat = 10;
  nouveau_arc->portee.deplacement = DEP;
  nouveau_arc->portee.attaques = ATT;
  nouveau_arc->esquive = 0.20;
  nouveau_arc->crit = 0.25;
  nouveau_arc->resistance = 3;
  nouveau_arc->precision = 1.00;
  nouveau_arc->pos_x = 0;
  nouveau_arc->pos_y = 0;
  nouveau_arc->parade = 0.0;
  nouveau_arc->etat = Vivant;
  nouveau_arc->equipe = equipe;
  nouveau_arc->pt_deplacement = 1;
  nouveau_arc->pt_attaque = 1;
  nouveau_arc->comp_special = true;
  nouveau_arc->race = race;
  nouveau_arc->direction = haut;
  nouveau_arc->liste = liste;
  nouveau_arc->type_race = race->race;

  /*Définition des compétences*/

  nouveau_arc->competence1 = tirdelaigle;
  nouveau_arc->competence2 = tirdecombat;
  nouveau_arc->competence3 = barrageabsolu;

  nouveau_arc->info_comp1 = tirdelaigle_info;
  nouveau_arc->info_comp2 = tirdecombat_info;
  nouveau_arc->info_comp3 = barrageabsolu_info;


  /*j'ai défini le type personnage_t qui va regrouper tous les types de personnages et permettre de faire des listes*/
  nouveau_arc->print =(void(*)(personnage_t* const)) afficher_archer;
  nouveau_arc->detruire = (err_t(*)(personnage_t**))detruire_archer;
  nouveau_arc->position_pers = (t_pos(*)(personnage_t*))position_archer;
  nouveau_arc->deplacer = (void(*)(int , int , personnage_t*))modifier_position_archer;
  nouveau_arc->f_subir_degats =(etat_t(*)(int, personnage_t*)) perdre_vie_archer;
  nouveau_arc->f_pt_deplacement = (int(*)(personnage_t *)) points_deplacement_archer;
  nouveau_arc->f_pt_attaque = (int(*)(personnage_t *)) points_attaque_archer;
  nouveau_arc->f_etat_pers = (etat_t(*)(personnage_t *)) etat_archer;
  nouveau_arc->f_vie = (int(*)(personnage_t *))vie_archer;
  nouveau_arc->f_esquive_parade = (float(*)(personnage_t *))esquive_archer;
  nouveau_arc->f_portee_attaque = (int(*)(personnage_t *))portee_attaque_archer;
  nouveau_arc->f_portee_deplacement = (int(*)(personnage_t *))portee_deplacement_archer;
  nouveau_arc->f_equipe = (equipe_t(*)(personnage_t *))equipe_archer;
  nouveau_arc->f_precision = (float(*)(personnage_t *))precision_archer;
  nouveau_arc->f_comp_spe = (bool(*)(personnage_t *))comp_spe_archer;
  nouveau_arc->tourner_haut = (void(*)(personnage_t *))archer_haut;
  nouveau_arc->tourner_bas = (void(*)(personnage_t *))archer_bas;
  nouveau_arc->tourner_droite = (void(*)(personnage_t *))archer_droite;
  nouveau_arc->tourner_gauche = (void(*)(personnage_t *))archer_gauche;

  return(nouveau_arc);
}
