/**
*  \file assassin.c
*  \brief classe assassin
*  \details cette classe contient les fonctions et methodes disponibles pour la classe assassin
*  Un assassin contient les caractéristiques suivantes :
*   - pv = 55;
*   - degat = 40;
*   - portee de deplacement = 3
*   - portee d'attaques = 5
*   - esquive = 0.25
*   - crit = 0.60
*   - resistance = 3
*   - precision = 1.00
*  \author Pierre garçon
*  \date 06 fevrier
*  \fn int existe_assassin, void afficher_assassin, void position_assassin, err_t detruire_assassin, assassin_t creer_assassin
*/




/*

  IMPORTANT : definir une fonction qui remet à 1 les points d'attaques et de deplacements

*/

#include <stdio.h>
#include <stdlib.h>
#include "../../include/assassin.h"
#include "../../include/nom_alea.h"

/*On défini la portée maximale des attaques et des déplacements*/
#define DEP 3
#define ATT 5


/*Définition des fonctions et méthodes*/
/**
 * \fn int existe_assassin(assassin_t * ass)
 * \brief fonction qui test si un assassin existe
 *
 * \param ass Assasin à tester
 *
 * \return int 0 | 1
 *
 */
extern
int existe_assassin(assassin_t * ass)
{
  if(ass == NULL)
    return(0);
  else
    return(1);
}

/*S'occupe de l'affuche de tous les parametres d'un assassin*/
/**
 * \fn void afficher_assassin(assassin_t * ass)
 * \brief fonction qui affiche toute les caracteristique d'un assassin
 *
 * \param ass Assassin à afficher
 *
 * \return void pas de retour
 *
 */
static
void afficher_assassin(assassin_t * ass)
{
  if(existe_assassin(ass))
  {
    if(ass->etat == Vivant)
    {
      printf("\nNom classe : assassin");
      printf("\nNom : %s", ass->nom);
      printf("\nPV : %d", ass->pv);
      printf("\nDegats : %d", ass->degat);
      printf("\nDeplacements : %d", ass->portee.deplacement);
      printf("\nattaque : %d", ass->portee.attaques);
      printf("\nEsquive : %f", ass->esquive);
      printf("\ncritique : %f", ass->crit);
      printf("\nResistance : %d", ass->resistance);
      printf("\nPrecision : %f", ass->precision);
      if(ass->equipe == Bleu)
      {
        printf("\nL'assassin est dans l'equipe des bleus");
      }
      else
      {
        printf("\nL'assassin est dans l'equipe des rouges");
      }
      printf("\nFin des stat\n");
    }
    else
    {
      printf("\n\nL'assassin est mort \n\n");
    }
  }
  else
  {
    printf("\nL'assassin n'existe plus");
  }

}




/*Récuperer la vie d'un assassin*/
/**
 * \fn int vie_assassin(assassin_t * ass)
 * \brief fonction qui recupere le nombre de pv d'un assassin
 *
 * \param ass Assassin à tester
 *
 * \return int ass->pv
 *
 */
static
int vie_assassin(assassin_t * ass)
{
  return ass->pv;
}




/*Récuperer les points d'attaque*/
/**
 * \fn int points_attaque_assassin(assassin_t * ass)
 * \brief fonction qui recupere le nombre de point d'attaque d'un assassin
 *
 * \param ass Assassin à tester
 *
 * \return int ass->pt_attaque
 *
 */
static
int points_attaque_assassin(assassin_t * ass)
{
  return ass->pt_attaque;
}





/*Récuperer les points de deplacement*/
/**
 * \fn int points_deplacement_assassin(assassin_t * ass)
 * \brief fonction qui recupere le nombre de point de deplacement d'un assassin
 *
 * \param ass Assassin à tester
 *
 * \return int ass->pt_deplacement
 *
 */
static
int points_deplacement_assassin(assassin_t * ass)
{
  return ass->pt_deplacement;
}


/*Récuperer les points de deplacement*/
/**
 * \fn int etat_assassin(assassin_t * ass)
 * \brief fonction qui recupere si un assassin est vivant ou pas
 *
 * \param ass Assassin à tester
 *
 * \return int ass->etat
 *
 */
static
int etat_assassin(assassin_t * ass)
{
  return ass->etat;
}



/*
*Fonctions pour changer la direction des personnages
*/

/**
 * \fn void assassin_bas(assassin_t * ass)
 * \brief fonction qui met la direction de l'assassin à bas
 *
 * \param ass Assassin à tester
 *
 * \return self
 *
 */
static
void assassin_bas(assassin_t * ass)
{
  ass->direction = bas;
}

/**
 * \fn void assassin_haut(assassin_t * ass)
 * \brief fonction qui met la direction de l'assassin à haut
 *
 * \param ass Assassin à tester
 *
 * \return self
 *
 */
static
void assassin_haut(assassin_t * ass)
{
  ass->direction = haut;
}

/**
 * \fn void assassin_droite(assassin_t * ass)
 * \brief fonction qui met la direction de l'assassin à droite
 *
 * \param ass Assassin à tester
 *
 * \return self
 *
 */
static
void assassin_droite(assassin_t * ass)
{
  ass->direction = droite;
}

/**
 * \fn void assassin_gauche(assassin_t * ass)
 * \brief fonction qui met la direction de l'assassin à gauche
 *
 * \param ass Assassin à tester
 *
 * \return self
 *
 */
static
void assassin_gauche(assassin_t * ass)
{
  ass->direction = gauche;
}





/*Récuperer les points de deplacement*/
/**
 * \fn int portee_deplacement_assassin(assassin_t * ass)
 * \brief fonction qui recupere la portee de deplacement d'un assassin
 *
 * \param ass Assassin à tester
 *
 * \return int ass->portee.deplacement
 *
 */
static
int portee_deplacement_assassin(assassin_t * ass)
{
  return ass->portee.deplacement;
}


/*Récuperer les points de deplacement*/
/**
 * \fn int portee_attaque_assassin(assassin_t * ass)
 * \brief fonction qui recupere la portee de d'attaque d'un assassin
 *
 * \param ass Assassin à tester
 *
 * \return int ass->portee.attaques
 *
 */
static
int portee_attaque_assassin(assassin_t * ass)
{
  return ass->portee.attaques;
}



/*Récuperer l'esquive de l'assassin*/
/**
 * \fn float esquive_assassin(assassin_t * ass)
 * \brief fonction qui recupere la chance d'esquive d'un assassin
 *
 * \param ass Assassin à tester
 *
 * \return float ass->esquive
 *
 */
static
float esquive_assassin(assassin_t * ass)
{
  return ass->esquive;
}




/*savoir si la compétence spéciale de l'assassin est disponible*/
/**
 * \fn bool comp_spe_assassin(assassin_t * ass)
 * \brief fonction qui recupere si la compétance spéciale d'un assassin est disponible
 *
 * \param ass Assassin à tester
 *
 * \return bool ass->comp_special
 *
 */
static
bool comp_spe_assassin(assassin_t * ass)
{
  return ass->comp_special;
}






/*Récuperer la position de l'acher*/
/**
 * \fn void position_assassin(int *x, int *y, assassin_t * ass)
 * \brief fonction qui recupere la position d'un assassin
 *
 * \param x Position X d'un assassin
 * \param y Position Y d'un assassin
 * \param ass Assassin à tester
 *
 * \return void pas de retour
 *
 */
static
t_pos position_assassin(assassin_t * ass)
{
  t_pos pos;
  pos.x = 0;
  pos.y = 0;
  if(existe_assassin(ass))
  {
    if(ass->etat == Vivant)
    {
      pos.x = ass->pos_x;
      pos.y = ass->pos_y;
    }
  }
  return pos;
}





/*Récuperer l'équipe d'un assassin'*/
/**
 * \fn equipe_t * equipe_assassin(assassin_t * ass)
 * \brief fonction qui recupere l'équipe d'un assassin
 *
 * \param ass Assassin à tester
 *
 * \return ass->equipe
 *
 */
static
equipe_t equipe_assassin(assassin_t * ass)
{
  return ass->equipe;
}



/*Récuperer la precision d'un assassin*/
/**
 * \fn float precision_assassin(assassin_t * ass)
 * \brief fonction qui recupere la precision d'un assassin
 *
 * \param ass Assassin à tester
 *
 * \return float ass->precision
 *
 */
static
float precision_assassin(assassin_t * ass)
{
  return ass->precision;
}








/*Récuperer la position de l'acher*/
/**
 * \fn void modifier_position_assassin(int x, int y, assassin_t * ass)
 * \brief fonction qui permet de deplacer un assassin
 *
 * \param x Position X d'un assassin
 * \param y Position Y d'un assassin
 * \param ass Assassin à tester
 *
 * \return void pas de retour
 *
 */

static
void modifier_position_assassin(int x, int y, assassin_t * ass)
{
  if(existe_assassin(ass))
  {
    if(ass->etat == Vivant)
    {
      if(ass->pt_deplacement != 0)
      {
        ass->pos_x = x;
        ass->pos_y = y;
        ass->pt_deplacement--;
      }
    }
  }
}








/*Diminue les PV d'un assassin selon les dégats subis*/
/**
 * \fn etat_t perdre_vie_assassin(int degat, assassin_t * ass)
 * \brief fonction qui effectue des degat sur un assassin et diminue leur pv
 *
 * \param degat Nombre de degat a inflicher au assassin
 * \param ass Assassin à tester
 *
 * \return Vivant | Mort | Rien
 *
 */
static
etat_t perdre_vie_assassin(int degat, assassin_t * ass)
{
  if(existe_assassin(ass))
  {
    ass->pv = (ass->pv)-(degat-(ass->resistance));
    if((ass->pv)<=0)
    {
        ass->etat = Mort;
        return Mort;
    }
    else
    {
        return Vivant;
    }

  }
  return  Rien;
}

/*permet de detruire un assassin*/
/**
 * \fn err_t detruire_assassin(assassin_t ** ass)
 * \brief fonction qui detruit une structure assassin
 *
 * \param ass Assassin à detruire
 *
 * \return Ok
 *
 */
static
err_t detruire_assassin(assassin_t ** ass)
{

  free((*ass));
  (*ass) = NULL;

  return(Ok);
}


/*On créer un nouvel assassin, il n'y a pas de parametres car toutes les stats sont pré-défini*/
/**
 * \fn assassin_t * creer_assassin(equipe_t * equipe, race_t * race)
 * \brief fonction qui creer une structure assassin
 *
 * \param equipe Dans lequel ajouter le assassin
 * \param race De l' assassin
 *
 * \return nouveau_ass
 *
 */
extern
assassin_t * creer_assassin(equipe_t equipe, race_t * race, liste_perso_t * liste)
{
  assassin_t * nouveau_ass = malloc(sizeof(assassin_t));
  char nom[25];

  donner_nom(nom);
  /*Création des caractéristiques des assassins*/
  nouveau_ass->classe = assassin;
  strcpy(nouveau_ass->nom, nom);
  nouveau_ass->pv = 55;
  nouveau_ass->pv_max = 55;
  nouveau_ass->degat = 40;
  nouveau_ass->portee.deplacement = DEP;
  nouveau_ass->portee.attaques = ATT;
  nouveau_ass->esquive = 0.25;
  nouveau_ass->crit = 0.60;
  nouveau_ass->resistance = 3;
  nouveau_ass->precision = 1.00;
  nouveau_ass->pos_x = 0;
  nouveau_ass->pos_y = 0;
  nouveau_ass->parade = 0.0;
  nouveau_ass->etat = Vivant;
  nouveau_ass->equipe = equipe;
  nouveau_ass->pt_deplacement = 1;
  nouveau_ass->pt_attaque = 1;
  nouveau_ass->comp_special = true;
  nouveau_ass->race = race;
  nouveau_ass->direction = bas;
  nouveau_ass->liste = liste;
  nouveau_ass->type_race = race->race;

  /*Définition des compétences*/
  nouveau_ass->competence1 = coupdeDague;
  nouveau_ass->competence2 = lancerdedagues;
  nouveau_ass->competence3 = beniparlesdieuxdusang;

  nouveau_ass->info_comp1 = coupdeDague_info;
  nouveau_ass->info_comp2 = lancerdedagues_info;
  nouveau_ass->info_comp3 = beniparlesdieuxdusang_info;

  /*j'ai défini le type personnage_t qui va regrouper tous les types de personnages et permettre de faire des listes*/
  nouveau_ass->print =(void(*)(personnage_t* const)) afficher_assassin;
  nouveau_ass->detruire = (err_t(*)(personnage_t**))detruire_assassin;
  nouveau_ass->position_pers = (t_pos(*)(personnage_t*))position_assassin;
  nouveau_ass->deplacer = (void(*)(int , int , personnage_t*))modifier_position_assassin;
  nouveau_ass->f_subir_degats =(etat_t(*)(int, personnage_t*)) perdre_vie_assassin;
  nouveau_ass->f_pt_deplacement = (int(*)(personnage_t *)) points_deplacement_assassin;
  nouveau_ass->f_pt_attaque = (int(*)(personnage_t *)) points_attaque_assassin;
  nouveau_ass->f_etat_pers = (etat_t(*)(personnage_t *)) etat_assassin;
  nouveau_ass->f_vie = (int(*)(personnage_t *))vie_assassin;
  nouveau_ass->f_esquive_parade = (float(*)(personnage_t *))esquive_assassin;
  nouveau_ass->f_portee_attaque = (int(*)(personnage_t *))portee_attaque_assassin;
  nouveau_ass->f_portee_deplacement = (int(*)(personnage_t *))portee_deplacement_assassin;
  nouveau_ass->f_equipe = (equipe_t(*)(personnage_t *))equipe_assassin;
  nouveau_ass->f_precision = (float(*)(personnage_t *))precision_assassin;
  nouveau_ass->f_comp_spe = (bool(*)(personnage_t *))comp_spe_assassin;
  nouveau_ass->tourner_bas = (void(*)(personnage_t *))assassin_bas;
  nouveau_ass->tourner_haut = (void(*)(personnage_t *))assassin_haut;
  nouveau_ass->tourner_droite = (void(*)(personnage_t *))assassin_droite;
  nouveau_ass->tourner_gauche = (void(*)(personnage_t *))assassin_gauche;



  return(nouveau_ass);
}
