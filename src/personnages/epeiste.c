/**
*  \file epeiste.c
*  \brief clepee epeiste
*  \details cette clepee contient les fonctions et methodes disponibles pour la classe epeiste
*  Un epeiste contient les caractéristiques suivantes :
*   - pv = 70;
*   - degat = 25;
*   - portee de deplacement = 5
*   - portee d'attaques = 1
*   - parade = 0.40
*   - crit = 0.15
*   - resistance = 4
*   - precision = 1.00
*  \author Pierre garcon
*  \date 06 fevrier
*  \fn int existe_epeiste, void afficher_epeiste, void position_epeiste, err_t detruire_epeiste, epeiste_t creer_epeiste
*/




/*

  IMPORTANT : definir une fonction qui remet à 1 les points d'attaques et de deplacements

*/

#include <stdio.h>
#include <stdlib.h>
#include "../../include/epeiste.h"
#include "../../include/attaques.h"
#include "../../include/nom_alea.h"

/*On défini la portée maximale des attaques et des déplacements*/
#define DEP 5
#define ATT 1


/*Définition des fonctions et méthodes*/
/**
 * \fn int existe_epeiste(epeiste_t * epe)
 * \brief fonction qui test si un epeepein existe
 *
 * \param epe Assasin à tester
 *
 * \return int 0 | 1
 *
 */
extern
int existe_epeiste(epeiste_t * epe)
{
  if(epe == NULL)
    return(0);
  else
    return(1);
}

/*S'occupe de l'affuche de tous les parametres d'un epeiste*/
/**
 * \fn void afficher_epeiste(epeiste_t * epe)
 * \brief fonction qui affiche toute les caracteristique d'un épéiste
 *
 * \param epe Epéiste à afficher
 *
 * \return void pas de retour
 *
 */
static
void afficher_epeiste(epeiste_t * epe)
{
  if(existe_epeiste(epe))
  {
    if(epe->etat == Vivant)
    {
      printf("\nNom clepee : epeiste");
      printf("\nNom : %s", epe->nom);
      printf("\nPV : %d", epe->pv);
      printf("\nDegats : %d", epe->degat);
      printf("\nDeplacements : %d", epe->portee.deplacement);
      printf("\nattaque : %d", epe->portee.attaques);
      printf("\nEsquive : %f", epe->esquive);
      printf("\ncritique : %f", epe->crit);
      printf("\nResistance : %d", epe->resistance);
      printf("\nPrecision : %f", epe->precision);
      if(epe->equipe == Bleu)
      {
        printf("\nL'epeiste est dans l'equipe des bleus");
      }
      else
      {
        printf("\nL'epeiste est dans l'equipe des rouges");
      }
      printf("\nFin des stat\n");
    }
    else
    {
      printf("\n\nL'epeiste est mort \n\n");
    }
  }
  else
  {
    printf("\nL'epeiste n'existe plus");
  }

}




/*Récuperer la vie d'un epeiste*/
/**
 * \fn int vie_epeiste(epeiste_t * epe)
 * \brief fonction qui recupere le nombre de pv d'un épéiste
 *
 * \param epe Epéiste à tester
 *
 * \return int epe->pv
 *
 */
static
int vie_epeiste(epeiste_t * epe)
{
  return epe->pv;
}




/*Récuperer les points d'attaque*/
/**
 * \fn int points_attaque_epeiste(epeiste_t * epe)
 * \brief fonction qui recupere le nombre de point d'attaque d'un épéiste
 *
 * \param epe Epéiste à tester
 *
 * \return int epe->pt_attaque
 *
 */
static
int points_attaque_epeiste(epeiste_t * epe)
{
  return epe->pt_attaque;
}





/*Récuperer les points de deplacement*/
/**
 * \fn int points_deplacement_epeiste(epeiste_t * epe)
 * \brief fonction qui recupere le nombre de point de deplacement d'un épéiste
 *
 * \param epe Epéiste à tester
 *
 * \return int epe->pt_deplacement
 *
 */
static
int points_deplacement_epeiste(epeiste_t * epe)
{
  return epe->pt_deplacement;
}


/*Récuperer les points de deplacement*/
/**
 * \fn int etat_epeiste(epeiste_t * epe)
 * \brief fonction qui recupere si un épéiste est vivant ou pas
 *
 * \param epe Epéiste à tester
 *
 * \return int epe->etat
 *
 */
static
int etat_epeiste(epeiste_t * epe)
{
  return epe->etat;
}





/*Récuperer les points de deplacement*/
/**
 * \fn int portee_deplacement_epeiste(epeiste_t * epe)
 * \brief fonction qui recupere la portee de deplacement d'un épéiste
 *
 * \param epe Epéiste à tester
 *
 * \return int epe->portee.deplacement
 *
 */
static
int portee_deplacement_epeiste(epeiste_t * epe)
{
  return epe->portee.deplacement;
}


/*Récuperer les points de deplacement*/
/**
 * \fn int portee_attaque_epeiste(epeiste_t * epe)
 * \brief fonction qui recupere la portee de d'attaque d'un épéiste
 *
 * \param epe Epéiste à tester
 *
 * \return int epe->portee.attaques
 *
 */
static
int portee_attaque_epeiste(epeiste_t * epe)
{
  return epe->portee.attaques;
}



/*
*Fonctions pour changer la direction des personnages
*/
/**
 * \fn void epeiste_haut(epeiste_t * epe)
 * \brief fonction qui met la position d'un épéiste à haut
 *
 * \param epe Epéiste à tester
 *
 * \return self
 *
 */
static
void epeiste_haut(epeiste_t * epe)
{
  epe->direction = haut;
}

/**
 * \fn void epeiste_bas(epeiste_t * epe)
 * \brief fonction qui met la position d'un épéiste à bas
 *
 * \param epe Epéiste à tester
 *
 * \return self
 *
 */
static
void epeiste_bas(epeiste_t * epe)
{
  epe->direction = bas;
}

/**
 * \fn void epeiste_droite(epeiste_t * epe)
 * \brief fonction qui met la position d'un épéiste à droite
 *
 * \param epe Epéiste à tester
 *
 * \return self
 *
 */
static
void epeiste_droite(epeiste_t * epe)
{
  epe->direction = droite;
}

/**
 * \fn void epeiste_gauche(epeiste_t * epe)
 * \brief fonction qui met la position d'un épéiste à gauche
 *
 * \param epe Epéiste à tester
 *
 * \return self
 *
 */
static
void epeiste_gauche(epeiste_t * epe)
{
  epe->direction = gauche;
}



/*Récuperer l'esquive de l'epeiste*/
/**
 * \fn float parade_epeiste(epeiste_t * epe)
 * \brief fonction qui recupere la chance d'esquive d'un épéiste
 *
 * \param epe Epéiste à tester
 *
 * \return float epe->parade
 *
 */
static
float esquive_epeiste(epeiste_t * epe)
{
  return epe->esquive;
}




/*savoir si la compétence spéciale de l'epeiste est disponible*/
/**
 * \fn bool comp_spe_epeiste(epeiste_t * epe)
 * \brief fonction qui recupere si la compétance spéciale d'un épéiste est disponible
 *
 * \param epe Epéiste à tester
 *
 * \return bool epe->comp_special
 *
 */
static
bool comp_spe_epeiste(epeiste_t * epe)
{
  return epe->comp_special;
}








/*Récuperer la position de l'acher*/
/**
 * \fn void position_epeiste(int *x, int *y, epeiste_t * epe)
 * \brief fonction qui recupere la position d'un épéiste
 *
 * \param x Position X d'un épéiste
 * \param y Position Y d'un épéiste
 * \param epe Epéiste à tester
 *
 * \return void pas de retour
 *
 */
static
t_pos position_epeiste(epeiste_t * epe)
{
  t_pos position;
  position.x = 0;
  position.y = 0;
  if(existe_epeiste(epe))
  {
    if(epe->etat == Vivant)
    {
      position.x = epe->pos_x;
      position.y = epe->pos_y;
    }
  }
  return position;
}





/*Récuperer l'équipe d'un epeiste'*/
/**
 * \fn equipe_t * equipe_epeiste(epeiste_t * epe)
 * \brief fonction qui recupere l'équipe d'un épéiste
 *
 * \param epe Epéiste à tester
 *
 * \return epe->equipe
 *
 */
static
equipe_t equipe_epeiste(epeiste_t * epe)
{
  return epe->equipe;
}



/*Récuperer la precision d'un epeiste*/
/**
 * \fn float precision_epeiste(epeiste_t * epe)
 * \brief fonction qui recupere la precision d'un épéiste
 *
 * \param epe Epéiste à tester
 *
 * \return float epe->precision
 *
 */
static
float precision_epeiste(epeiste_t * epe)
{
  return epe->precision;
}








/*Récuperer la position de l'acher*/
/**
 * \fn void modifier_position_epeiste(int x, int y, epeiste_t * epe)
 * \brief fonction qui permet de deplacer un épéiste
 *
 * \param x Position X d'un épéiste
 * \param y Position Y d'un épéiste
 * \param epe Epéiste à tester
 *
 * \return void pas de retour
 *
 */
static
void modifier_position_epeiste(int x, int y, epeiste_t * epe)
{
  if(existe_epeiste(epe))
  {
    if(epe->etat == Vivant)
    {
      if(epe->pt_deplacement != 0)
      {
        epe->pos_x = x;
        epe->pos_y = y;
        epe->pt_deplacement--;
      }
    }
  }
}








/*Diminue les PV d'un epeiste selon les dégats subis*/
/**
 * \fn etat_t perdre_vie_epeiste(int degat, epeiste_t * epe)
 * \brief fonction qui effectue des degat sur un épéiste et diminue leur pv
 *
 * \param degat Nombre de degat a inflicher au épéiste
 * \param epe Epéiste à tester
 *
 * \return Vivant | Mort | Rien
 *
 */
static
etat_t perdre_vie_epeiste(int degat, epeiste_t * epe)
{
  if(existe_epeiste(epe))
  {
    epe->pv = (epe->pv)-(degat-(epe->resistance));
    if((epe->pv)<=0)
    {
        epe->etat = Mort;
        return Mort;
    }
    else
    {
        return Vivant;
    }

  }
  return  Rien;
}


/*permet de detruire un epeiste*/
/**
 * \fn err_t detruire_epeiste(epeiste_t ** epe)
 * \brief fonction qui detruit une structure épéiste
 *
 * \param epe Epéiste à detruire
 *
 * \return Ok
 *
 */
static
err_t detruire_epeiste(epeiste_t ** epe)
{

  free((*epe));
  (*epe) = NULL;

  return(Ok);
}


/*On créer un nouvel epeiste, il n'y a pas de parametres car toutes les stats sont pré-défini*/
/**
 * \fn epeiste_t * creer_epeiste(equipe_t * equipe, race_t * race)
 * \brief fonction qui creer une structure épéiste
 *
 * \param equipe Dans lequel ajouter le épéiste
 * \param race De l' épéiste
 *
 * \return nouveau_epe
 *
 */
extern
epeiste_t * creer_epeiste(equipe_t equipe, race_t * race, liste_perso_t * liste)
{
  epeiste_t * nouveau_epe = malloc(sizeof(epeiste_t));

  char nom[25];

  donner_nom(nom);
  /*Création des caractéristiques des epeistes*/
  nouveau_epe->classe = epeiste;
  strcpy(nouveau_epe->nom, nom);
  nouveau_epe->pv = 70;
  nouveau_epe->pv_max = 70;
  nouveau_epe->degat = 25;
  nouveau_epe->portee.deplacement = DEP;
  nouveau_epe->portee.attaques = ATT;
  nouveau_epe->esquive = 0.00;
  nouveau_epe->crit = 0.15;
  nouveau_epe->resistance = 4;
  nouveau_epe->precision = 1.00;
  nouveau_epe->pos_x = 0;
  nouveau_epe->pos_y = 0;
  nouveau_epe->parade = 40.0;
  nouveau_epe->etat = Vivant;
  nouveau_epe->equipe = equipe;
  nouveau_epe->pt_deplacement = 1;
  nouveau_epe->pt_attaque = 1;
  nouveau_epe->comp_special = true;
  nouveau_epe->race = race;
  nouveau_epe->direction = haut;
  nouveau_epe->liste = liste;
  nouveau_epe->type_race = race->race;

  /*Définition des compétences*/
  nouveau_epe->competence1 = voleedepee;
  nouveau_epe->competence2 = ragedudueliste;
  nouveau_epe->competence3 = barrageabsolu;

  nouveau_epe->info_comp1 = voleedepee_info;
  nouveau_epe->info_comp2 = ragedudueliste_info;
  nouveau_epe->info_comp3 = dansedelamort_info;

  /*j'ai défini le type personnage_t qui va regrouper tous les types de personnages et permettre de faire des listes*/
  nouveau_epe->print =(void(*)(personnage_t* const)) afficher_epeiste;
  nouveau_epe->detruire = (err_t(*)(personnage_t**))detruire_epeiste;
  nouveau_epe->position_pers = (t_pos(*)(personnage_t*))position_epeiste;
  nouveau_epe->deplacer = (void(*)(int , int , personnage_t*))modifier_position_epeiste;
  nouveau_epe->f_subir_degats =(etat_t(*)(int, personnage_t*)) perdre_vie_epeiste;
  nouveau_epe->f_pt_deplacement = (int(*)(personnage_t *)) points_deplacement_epeiste;
  nouveau_epe->f_pt_attaque = (int(*)(personnage_t *)) points_attaque_epeiste;
  nouveau_epe->f_etat_pers = (etat_t(*)(personnage_t *)) etat_epeiste;
  nouveau_epe->f_vie = (int(*)(personnage_t *))vie_epeiste;
  nouveau_epe->f_esquive_parade = (float(*)(personnage_t *))esquive_epeiste;
  nouveau_epe->f_portee_attaque = (int(*)(personnage_t *))portee_attaque_epeiste;
  nouveau_epe->f_portee_deplacement = (int(*)(personnage_t *))portee_deplacement_epeiste;
  nouveau_epe->f_equipe = (equipe_t(*)(personnage_t *))equipe_epeiste;
  nouveau_epe->f_precision = (float(*)(personnage_t *))precision_epeiste;
  nouveau_epe->f_comp_spe = (bool(*)(personnage_t *))comp_spe_epeiste;
  nouveau_epe->tourner_haut = (void(*)(personnage_t *))epeiste_haut;
  nouveau_epe->tourner_bas = (void(*)(personnage_t *))epeiste_bas;
  nouveau_epe->tourner_droite = (void(*)(personnage_t *))epeiste_droite;
  nouveau_epe->tourner_gauche = (void(*)(personnage_t *))epeiste_gauche;



  return(nouveau_epe);
}
