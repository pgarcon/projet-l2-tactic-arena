/**
*  \file healer.c
*  \brief clheale healer
*  \details cette clheale contient les fonctions et methodes disponibles pour la classe healer
*  Un healer contient les caractéristiques suivantes :
*   - pv = 65;
*   - degat = 0;
*   - portee de deplacement = 3
*   - portee d'attaques = 5
*   - parade = 0.30
*   - crit = 0.0
*   - resistance = 3
*   - precision = 1.00
*  \author Pierre garcon
*  \date 06 fevrier
*  \fn int existe_healer, void afficher_healer, void position_healer, err_t detruire_healer, healer_t creer_healer
*/




/*

  IMPORTANT : definir une fonction qui remet à 1 les points d'attaques et de deplacements

*/

#include <stdio.h>
#include <stdlib.h>
#include "../../include/healer.h"
#include "../../include/attaques.h"
#include "../../include/nom_alea.h"

/*On défini la portée maximale des attaques et des déplacements*/
#define DEP 3
#define ATT 5


/*Définition des fonctions et méthodes*/
/**
 * \fn int existe_healer(healer_t * heal)
 * \brief fonction qui test si un assassin existe
 *
 * \param heal Healer à tester
 *
 * \return int 0 | 1
 *
 */
extern
int existe_healer(healer_t * heal)
{
  if(heal == NULL)
    return(0);
  else
    return(1);
}

/*S'occupe de l'affuche de tous les parametres d'un healer*/
/**
 * \fn vvoid afficher_healer(healer_t * heal)
 * \brief fonction qui affiche toute les caracteristique d'un healer
 *
 * \param heal Healer à afficher
 *
 * \return void pas de retour
 *
 */
static
void afficher_healer(healer_t * heal)
{
  if(existe_healer(heal))
  {
    if(heal->etat == Vivant)
    {
      printf("\nNom clheale : healer");
      printf("\nNom : %s", heal->nom);
      printf("\nPV : %d", heal->pv);
      printf("\nDegats : %d", heal->degat);
      printf("\nDeplacements : %d", heal->portee.deplacement);
      printf("\nattaque : %d", heal->portee.attaques);
      printf("\nEsquive : %f", heal->esquive);
      printf("\ncritique : %f", heal->crit);
      printf("\nResistance : %d", heal->resistance);
      printf("\nPrecision : %f", heal->precision);
      if(heal->equipe == Bleu)
      {
        printf("\nL'healer est dans l'equipe des bleus");
      }
      else
      {
        printf("\nL'healer est dans l'equipe des rouges");
      }
      printf("\nFin des stat\n");
    }
    else
    {
      printf("\n\nL'healer est mort \n\n");
    }
  }
  else
  {
    printf("\nL'healer n'existe plus");
  }

}




/*Récuperer la vie d'un healer*/
/**
 * \fn int vie_healer(healer_t * heal)
 * \brief fonction qui recupere le nombre de pv d'un healer
 *
 * \param heal Healer à tester
 *
 * \return int heal->pv
 *
 */
static
int vie_healer(healer_t * heal)
{
  return heal->pv;
}




/*Récuperer les points d'attaque*/
/**
 * \fn int points_attaque_healer(healer_t * heal)
 * \brief fonction qui recupere le nombre de point d'attaque d'un healer
 *
 * \param heal Healer à tester
 *
 * \return int heal->pt_attaque
 *
 */
static
int points_attaque_healer(healer_t * heal)
{
  return heal->pt_attaque;
}



/*
*Fonctions pour changer la direction des personnages
*/

/**
 * \fn void healer_haut(healer_t * heal)
 * \brief fonction qui met la position d'un healer à haut
 *
 * \param heal healer à tester
 *
 * \return self
 *
 */
static
void healer_haut(healer_t * heal)
{
  heal->direction = haut;
}

/**
 * \fn void healer_bas(healer_t * heal)
 * \brief fonction qui met la position d'un healer à bas
 *
 * \param heal healer à tester
 *
 * \return self
 *
 */
static
void healer_bas(healer_t * heal)
{
  heal->direction = bas;
}

/**
 * \fn void healer_droite(healer_t * heal)
 * \brief fonction qui met la position d'un healer à droite
 *
 * \param heal healer à tester
 *
 * \return self
 *
 */
static
void healer_droite(healer_t * heal)
{
  heal->direction = droite;
}

/**
 * \fn void healer_gauche(healer_t * heal)
 * \brief fonction qui met la position d'un healer à gauche
 *
 * \param heal healer à tester
 *
 * \return self
 *
 */
static
void healer_gauche(healer_t * heal)
{
  heal->direction = gauche;
}





/*Récuperer les points de deplacement*/
/**
 * \fn int points_deplacement_healer(healer_t * heal)
 * \brief fonction qui recupere le nombre de point de deplacement d'un healer
 *
 * \param heal Healer à tester
 *
 * \return int heal->pt_deplacement
 *
 */
static
int points_deplacement_healer(healer_t * heal)
{
  return heal->pt_deplacement;
}


/*Récuperer les points de deplacement*/
/**
 * \fn int etat_healer(healer_t * heal)
 * \brief fonction qui recupere si un healer est vivant ou pas
 *
 * \param heal Healer à tester
 *
 * \return int heal->etat
 *
 */
static
int etat_healer(healer_t * heal)
{
  return heal->etat;
}





/*Récuperer les points de deplacement*/
/**
 * \fn int portee_deplacement_healer(healer_t * heal)
 * \brief fonction qui recupere la portee de deplacement d'un healer
 *
 * \param heal Healer à tester
 *
 * \return int heal->portee.deplacement
 *
 */
static
int portee_deplacement_healer(healer_t * heal)
{
  return heal->portee.deplacement;
}


/*Récuperer les points de deplacement*/
/**
 * \fn int portee_attaque_healer(healer_t * heal)
 * \brief fonction qui recupere la portee de d'attaque d'un healer
 *
 * \param heal Healer à tester
 *
 * \return int heal->portee.attaques
 *
 */
static
int portee_attaque_healer(healer_t * heal)
{
  return heal->portee.attaques;
}



/*Récuperer l'esquive de l'healer*/
/**
 * \fn float esquive_healer(healer_t * heal)
 * \brief fonction qui recupere la chance d'esquive d'un healer
 *
 * \param heal Healer à tester
 *
 * \return float heal->esquive
 *
 */
static
float esquive_healer(healer_t * heal)
{
  return heal->esquive;
}




/*savoir si la compétence spéciale de l'healer est disponible*/
/**
 * \fn bool comp_spe_healer(healer_t * heal)
 * \brief fonction qui recupere si la compétance spéciale d'un healer est disponible
 *
 * \param heal Healer à tester
 *
 * \return bool heal->comp_special
 *
 */
static
bool comp_spe_healer(healer_t * heal)
{
  return heal->comp_special;
}








/*Récuperer la position de l'acher*/
/**
 * \fn void position_healer(int *x, int *y, healer_t * heal)
 * \brief fonction qui recupere la position d'un healer
 *
 * \param x Position X d'un healer
 * \param y Position Y d'un healer
 * \param heal Healer à tester
 *
 * \return t_pos position du healer
 *
 */
static
t_pos position_healer(int *x, int *y, healer_t * heal)
{
  t_pos pos;
  pos.x = 0;
  pos.y = 0;
  if(existe_healer(heal))
  {
    if(heal->etat == Vivant)
    {
      pos.x = heal->pos_x;
      pos.y = heal->pos_y;
    }
  }
  return pos;
}





/*Récuperer l'équipe d'un healer'*/
/**
 * \fn equipe_t * equipe_healer(healer_t * heal)
 * \brief fonction qui recupere l'équipe d'un healer
 *
 * \param heal Healer à tester
 *
 * \return heal->equipe
 *
 */
static
equipe_t equipe_healer(healer_t * heal)
{
  return heal->equipe;
}



/*Récuperer la precision d'un healer*/
/**
 * \fn float precision_healer(healer_t * heal)
 * \brief fonction qui recupere la precision d'un healer
 *
 * \param heal Healer à tester
 *
 * \return float heal->precision
 *
 */
static
float precision_healer(healer_t * heal)
{
  return heal->precision;
}







/*Récuperer la position de l'acher*/
/**
 * \fn void modifier_position_healer(int x, int y, healer_t * heal)
 * \brief fonction qui permet de deplacer un healer
 *
 * \param x Position X d'un healer
 * \param y Position Y d'un healer
 * \param heal Healer à tester
 *
 * \return void pas de retour
 *
 */

static
void modifier_position_healer(int x, int y, healer_t * heal)
{
  if(existe_healer(heal))
  {
    if(heal->etat == Vivant)
    {
      if(heal->pt_deplacement != 0)
      {
        heal->pos_x = x;
        heal->pos_y = y;
        heal->pt_deplacement--;
      }
    }
  }
}








/*Diminue les PV d'un healer selon les dégats subis*/
/**
 * \fn etat_t perdre_vie_healer(int degat, healer_t * heal)
 * \brief fonction qui effectue des degat sur un healer et diminue leur pv
 *
 * \param degat Nombre de degat a inflicher au healer
 * \param heal Healer à tester
 *
 * \return Vivant | Mort | Rien
 *
 */
static
etat_t perdre_vie_healer(int degat, healer_t * heal)
{
  if(existe_healer(heal))
  {
    heal->pv = (heal->pv)-(degat-(heal->resistance));
    if((heal->pv)<=0)
    {
        heal->etat = Mort;
        return Mort;
    }
    else
    {
        return Vivant;
    }

  }
  return  Rien;
}

/*permet de detruire un healer*/
/**
 * \fn err_t detruire_healer(healer_t ** heal)
 * \brief fonction qui detruit une structure healer
 *
 * \param heal Healer à detruire
 *
 * \return Ok
 *
 */
static
err_t detruire_healer(healer_t ** heal)
{

  free((*heal));
  (*heal) = NULL;

  return(Ok);
}


/*On créer un nouvel healer, il n'y a pas de parametres car toutes les stats sont pré-défini*/
/**
 * \fn healer_t * creer_healer(equipe_t * equipe, race_t * race)
 * \brief fonction qui creer une structure healer
 *
 * \param equipe Dans lequel ajouter le healer
 * \param race De l' healer
 *
 * \return nouveau_ass
 *
 */
extern
healer_t * creer_healer(equipe_t equipe, race_t * race, liste_perso_t * liste)
{
  healer_t * nouveau_heal = malloc(sizeof(healer_t));

  char nom[25];

  donner_nom(nom);

  /*Création des caractéristiques des healers*/
  nouveau_heal->classe = healer;
  strcpy(nouveau_heal->nom, nom);
  nouveau_heal->pv = 65;
  nouveau_heal->pv_max = 65;
  nouveau_heal->degat = 0;
  nouveau_heal->portee.deplacement = DEP;
  nouveau_heal->portee.attaques = ATT;
  nouveau_heal->esquive = 0.15;
  nouveau_heal->crit = 0.0;
  nouveau_heal->resistance = 3;
  nouveau_heal->precision = 1.00;
  nouveau_heal->pos_x = 0;
  nouveau_heal->pos_y = 0;
  nouveau_heal->parade = 0.30;
  nouveau_heal->etat = Vivant;
  nouveau_heal->equipe = equipe;
  nouveau_heal->pt_deplacement = 1;
  nouveau_heal->pt_attaque = 1;
  nouveau_heal->comp_special = true;
  nouveau_heal->race = race;
  nouveau_heal->direction = haut;
  nouveau_heal->liste = liste;
  nouveau_heal->type_race = race->race;

  /*Définition des compétences*/
  nouveau_heal->competence1 = benedictiondivine;
  nouveau_heal->competence2 = transfertdevitalite;
  nouveau_heal->competence3 = gracedeladeesse;

  nouveau_heal->info_comp1 = benedictiondivine_info;
  nouveau_heal->info_comp2 = transfertdevitalite_info;
  nouveau_heal->info_comp3 = gracedeladeesse_info;

  /*j'ai défini le type personnage_t qui va regrouper tous les types de personnages et permettre de faire des listes*/
  nouveau_heal->print =(void(*)(personnage_t* const)) afficher_healer;
  nouveau_heal->detruire = (err_t(*)(personnage_t**))detruire_healer;
  nouveau_heal->position_pers = (t_pos(*)(personnage_t*))position_healer;
  nouveau_heal->deplacer = (void(*)(int , int , personnage_t*))modifier_position_healer;
  nouveau_heal->f_subir_degats =(etat_t(*)(int, personnage_t*)) perdre_vie_healer;
  nouveau_heal->f_pt_deplacement = (int(*)(personnage_t *)) points_deplacement_healer;
  nouveau_heal->f_pt_attaque = (int(*)(personnage_t *)) points_attaque_healer;
  nouveau_heal->f_etat_pers = (etat_t(*)(personnage_t *)) etat_healer;
  nouveau_heal->f_vie = (int(*)(personnage_t *))vie_healer;
  nouveau_heal->f_esquive_parade = (float(*)(personnage_t *))esquive_healer;
  nouveau_heal->f_portee_attaque = (int(*)(personnage_t *))portee_attaque_healer;
  nouveau_heal->f_portee_deplacement = (int(*)(personnage_t *))portee_deplacement_healer;
  nouveau_heal->f_equipe = (equipe_t(*)(personnage_t *))equipe_healer;
  nouveau_heal->f_precision = (float(*)(personnage_t *))precision_healer;
  nouveau_heal->f_comp_spe = (bool(*)(personnage_t *))comp_spe_healer;
  nouveau_heal->tourner_haut = (void(*)(personnage_t *))healer_haut;
  nouveau_heal->tourner_bas = (void(*)(personnage_t *))healer_bas;
  nouveau_heal->tourner_droite = (void(*)(personnage_t *))healer_droite;
  nouveau_heal->tourner_gauche = (void(*)(personnage_t *))healer_gauche;



  return(nouveau_heal);
}
