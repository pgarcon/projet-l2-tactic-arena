/**
*  \file nom_alea.c
*  \brief clepee epeiste
*  \details Cette classe contient le corps de la fonction suivante :
* - donner_nom
*  \author Pierre garcon
*  \date 06 fevrier
*/


#include "../../include/nom_alea.h"



/*Fonction de génération d'un nom aléatoire*/
/**
 * \fn void donner_nom(char * nom)
 * \brief fonction qui met un nom aléatoire dans nom
 *
 * \param char nom
 *
 * \return self
 *
 */
extern
void donner_nom(char * nom)
{
  srand(time(NULL));
  int i1 = rand()%49, i2 = rand()%49, j = 0;
  int i = i1 + i2;
  FILE *fic = NULL;
  fic = fopen(NOM_FICHIER, "r");

  if(fic == NULL)
  {
    printf("\nImpossible d'ouvrir le fichier\n");
    return ;
  }

  do{
    j++;
    fscanf(fic, "%s", nom);
  }while(!feof(fic) && j != i);

  fclose(fic);
}
