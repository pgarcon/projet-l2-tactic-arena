/**
  \file personnage.c
  \brief methodes et fonctions de la classe personnage
  \details contient les fonctions relatives aux personnages
  \author Pierre garcon
  \date 06 Mars
*/

#include "../../include/equipe.h"

/**
 * \fn void personnage_MAJ_equipe_race(personnage_t * pers)
 * \brief fonction qui met à jour les mort des personnage
 *
 * \param pers personnage
 *
 * \return void
 *
 */
extern
void personnage_MAJ_equipe_mort(personnage_t * perso)
{
  if(perso->etat == Mort)
  {
    liste_decrementer_nb_vivant(perso->liste);
  }
}
