/**
*  \file race.c
*  \brief classe race
*  \details Ce fichier permet la creation des race et l'activation des bonus lié a ces dernières grace à différentes methodes et fonctions
*  il y a 6 race :
*   - humain
*   - demon
*   - kobolt
*   - nain
*   - orc
*   - elfes
*  \author Pierre garcon
*  \date 06 fevrier 2021
*  \fn int existe_archer, void afficher_archer, void position_archer, err_t detruire_archer, archer_t creer_archer
*/
#include "../../include/personnage.h"


/*Methodes*/


/*
  Caractéristiques ORCS
*/

/**
 * \fn void race_activer_orc(personnage_t * pers)
 * \brief fonction qui active les bonus de race d'un orc
 *
 * \param pers perosnnage
 *
 * \return void
 *
 */
static
void race_activer_orc(personnage_t * pers)
{
  pers->resistance = pers->resistance + pers->race->bonus;
  //pers->competences[2]->degat_max -= pers->race->malus;
}

/**
 * \fn void race_desactiver_orc(personnage_t * pers)
 * \brief fonction qui desactive les bonus de race d'un orc
 *
 * \param pers perosnnage
 *
 * \return void
 *
 */
static
void race_desactiver_orc(personnage_t * pers)
{
  pers->resistance = pers->resistance - pers->race->bonus;
  //pers->competences[2]->degat_max += pers->race->malus;
}


/*
  Caractéristiques ELFES
*/
/**
 * \fn void race_activer_elfe(personnage_t * pers)
 * \brief fonction qui active les bonus de race d'un efle
 *
 * \param pers personnage
 *
 * \return void
 *
 */
static
void race_activer_elfe(personnage_t * pers)
{
  pers->esquive = pers->esquive + (float)pers->race->bonus*1.0;
  //pers->competences[2]->degat_max -= pers->race->malus;
}

/**
 * \fn void race_desactiver_elfe(personnage_t * pers)
 * \brief fonction qui desactive les bonus de race d'un elfe
 *
 * \param pers personnage
 *
 * \return void
 *
 */
static
void race_desactiver_elfe(personnage_t * pers)
{
  pers->esquive = pers->esquive - (float)pers->race->bonus*1.0;
  //pers->competences[2]->degat_max += pers->race->malus;
}


/*
  Caractéristiques HUMAIN
*/

/**
 * \fn void race_activer_humain(personnage_t * pers)
 * \brief fonction qui active les bonus de race d'un humain
 *
 * \param pers personnage
 *
 * \return void
 *
 */
static
void race_activer_humain(personnage_t * pers)
{
  pers->portee.deplacement = pers->portee.deplacement + pers->race->bonus;
  //pers->competences[2]->degat_max -= pers->race->malus;
}

/**
 * \fn void race_desactiver_humain(personnage_t * pers)
 * \brief fonction qui desactive les bonus de race d'un humain
 *
 * \param pers personnage
 *
 * \return void
 *
 */
static
void race_desactiver_humain(personnage_t * pers)
{
  pers->portee.deplacement = pers->portee.deplacement - pers->race->bonus;
  if(pers->portee.deplacement > 0)
    pers->portee.deplacement += pers->race->bonus;
  //pers->competences[2]->degat_max += pers->race->malus;
}


/*
  Caractéristiques NAINS
*/

/**
 * \fn void race_activer_nain(personnage_t * pers)
 * \brief fonction qui active les bonus de race d'un nain
 *
 * \param pers personnage
 *
 * \return void
 *
 */
static
void race_activer_nain(personnage_t * pers)
{
  pers->portee.deplacement = pers->portee.deplacement + pers->race->bonus;
  //pers->competences[2]->degat_max -= pers->race->malus;
}

/**
 * \fn void race_desactiver_nain(personnage_t * pers)
 * \brief fonction qui desactive les bonus de race d'un nain
 *
 * \param pers personnage
 *
 * \return void
 *
 */
static
void race_desactiver_nain(personnage_t * pers)
{
  pers->resistance = pers->resistance - pers->race->bonus;
  if(pers->portee.deplacement > 0)
    pers->portee.deplacement += pers->race->bonus;
  //pers->competences[2]->degat_max += pers->race->malus;
}



/*
  Caractéristiques DEMON
*/

/**
 * \fn void race_activer_demon(personnage_t * pers)
 * \brief fonction qui active les bonus de race d'un demon
 *
 * \param pers personnage
 *
 * \return void
 *
 */
static
void race_activer_demon(personnage_t * pers)
{
  pers->portee.deplacement = pers->portee.deplacement + pers->race->bonus;
  //pers->competences[2]->degat_max -= pers->race->malus;
}

/**
 * \fn void race_desactiver_demon(personnage_t * pers)
 * \brief fonction qui desactive les bonus de race d'un demon
 *
 * \param pers personnage
 *
 * \return void
 *
 */
static
void race_desactiver_demon(personnage_t * pers)
{
  pers->portee.deplacement = pers->portee.deplacement - pers->race->bonus;
  if(pers->portee.deplacement > 0)
    pers->portee.deplacement += pers->race->bonus;
  //pers->competences[2]->degat_max += pers->race->malus;
}

/*
  Caractéristiques Kobolts
*/
/**
 * \fn void race_activer_kobold(personnage_t * pers)
 * \brief fonction qui active les bonus de race d'un kobold
 *
 * \param pers personnage
 *
 * \return void
 *
 */
static
void race_activer_kobolt(personnage_t * pers)
{
  pers->portee.deplacement = pers->portee.deplacement + pers->race->bonus;
  //pers->competences[2]->degat_max -= pers->race->malus;
}

/**
 * \fn void race_desactiver_kobold(personnage_t * pers)
 * \brief fonction qui desactive les bonus de race d'un kobold
 *
 * \param pers personnage
 *
 * \return void
 *
 */
static
void race_desactiver_kobolt(personnage_t * pers)
{
  pers->portee.deplacement = pers->portee.deplacement - pers->race->bonus;
  //pers->competences[2]->degat_max += pers->race->malus;
}

/*destruction d'un race*/
/**
 * \fn err_t race_detruire(race_t ** race)
 * \brief fonction qui detruit en mémoire une race
 *
 * \param race ** race
 *
 * \return err_t
 *
 */
static
err_t race_detruire(race_t ** race)
{
  free((*race));
  (*race)=NULL;

  return(Ok);
}

/*Fonctions*/

/*Création des différentes races*/
/**
 * \fn race_t * race_creer_orc()
 * \brief fonction qui créer une race orc
 * \return race_t *
 *
*/
extern
race_t * race_creer_orc()
{
  race_t * race = malloc(sizeof(race_t));

  sprintf(race->nom_race, "orc");
  race->bonus = 2;
  race->malus = 3;
  race->race = orc;

  race->race_activer_bonus = (void(*)(void * pers))race_activer_orc;
  race->race_desactiver_bonus = (void(*)(void * pers))race_desactiver_orc;

  race->detruire = race_detruire;

  return race;
}

/**
 * \fn race_t * race_creer_humain()
 * \brief fonction qui créer une race humain
 * \return race_t *
 *
*/
extern
race_t * race_creer_humain()
{
  race_t * race = malloc(sizeof(race_t));

  sprintf(race->nom_race, "humain");
  race->bonus = 2;
  race->malus = 5;
  race->race = humain;

  race->race_activer_bonus = (void(*)(void * pers))race_activer_humain;
  race->race_desactiver_bonus = (void(*)(void * pers))race_desactiver_humain;

  race->detruire = race_detruire;

  return race;
}

/**
 * \fn race_t * race_creer_elfe()
 * \brief fonction qui créer une race elfe
 * \return race_t *
 *
*/
extern
race_t * race_creer_elfe()
{
  race_t * race = malloc(sizeof(race_t));

  sprintf(race->nom_race, "elfe");
  race->bonus = 2;
  race->malus = 5;
  race->race = elfe;

  race->race_activer_bonus = (void(*)(void * pers))race_activer_elfe;
  race->race_desactiver_bonus = (void(*)(void * pers))race_desactiver_elfe;

  race->detruire = race_detruire;

  return race;
}

/**
 * \fn race_t * race_creer_kobolt()
 * \brief fonction qui créer une race kobolt
 * \return race_t *
 *
*/
extern
race_t * race_creer_kobolt()
{
  race_t * race = malloc(sizeof(race_t));

  sprintf(race->nom_race, "kobolt");
  race->bonus = 2;
  race->malus = 3;
  race->race = kobolt;

  race->race_activer_bonus = (void(*)(void * pers))race_activer_kobolt;
  race->race_desactiver_bonus = (void(*)(void * pers))race_desactiver_kobolt;

  race->detruire = race_detruire;

  return race;
}

/**
 * \fn race_t * race_creer_nain()
 * \brief fonction qui créer une race nain
 * \return race_t *
 *
*/
extern
race_t * race_creer_nain()
{
  race_t * race = malloc(sizeof(race_t));

  sprintf(race->nom_race, "nain");
  race->bonus = 4;
  race->malus = 2;
  race->race = nain;

  race->race_activer_bonus = (void(*)(void * pers))race_activer_nain;
  race->race_desactiver_bonus = (void(*)(void * pers))race_desactiver_nain;

  race->detruire = race_detruire;

  return race;
}

/**
 * \fn race_t * race_creer_demon()
 * \brief fonction qui créer une race demon
 * \return race_t *
 *
*/
race_t * race_creer_demon()
{
  race_t * race = malloc(sizeof(race_t));

  sprintf(race->nom_race, "demon");
  race->bonus = 2;
  race->malus = 2;
  race->race = demon;

  race->race_activer_bonus = (void(*)(void * pers))race_activer_demon;
  race->race_desactiver_bonus = (void(*)(void * pers))race_desactiver_demon;

  race->detruire = race_detruire;

  return race;
}

/**
 * \fn void race_activer(liste_perso_t *)
 * \brief fonction qui active toutes les races des personnages d'un equipe
 * \param  liste equipe
 * \return self
 *
*/
extern
void race_activer(liste_perso_t * liste)
{
  for(int i=0; i<liste->nb_pers+1; i++)
  {
    if(liste->liste[i]->race->race != demon)
      liste->liste[i]->race->race_activer_bonus(liste->liste[i]);
  }
}

/**
 * \fn void race_maj(liste_perso_t *, t_temps )
 * \brief fonction qui active toutes les races des personnages d'un equipe en fonction du temps (jour ou nuit)
 * \param  liste equipe, t_temps
 * \return self
 *
*/
static
void race_maj(liste_perso_t * liste, t_temps temps)
{
  for(int i=0; i<liste->nb_pers+1; i++)
  {
    if(liste->liste[i]->race->race == demon)
    {
      if(temps == nuit)
      {
        liste->liste[i]->race->race_activer_bonus(liste->liste[i]);
      }
      else
      {
        liste->liste[i]->race->race_desactiver_bonus(liste->liste[i]);
      }
    }
    else
    {
      if(temps == jour)
      {
        liste->liste[i]->race->race_activer_bonus(liste->liste[i]);
      }
      else
      {
        liste->liste[i]->race->race_desactiver_bonus(liste->liste[i]);
      }
    }

  }
}

/**
 * \fn void race_maj_equipe(liste_perso_t *)
 * \brief fonction qui active toutes les races des personnages des equipes en fonction du temps
 * \param  liste equipe1, liste equipe2, temps
 * \return self
 *
*/
extern
void race_maj_equipe(liste_perso_t * equipe1, liste_perso_t * equipe2, t_temps temps)
{
    race_maj(equipe1, temps);
    race_maj(equipe2, temps);
}
