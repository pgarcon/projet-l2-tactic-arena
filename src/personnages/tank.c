/**
*  \file tank.c
*  \brief cltanke tank
*  \details cette cltanke contient les fonctions et methodes disponibles pour la classe tank
*  Un tank contient les caractéristiques suivantes :
*   - pv = 100;
*   - degat = 20;
*   - portee de deplacement = 2
*   - portee d'attaques = 1
*   - parade = 0.30
*   - crit = 0.10
*   - resistance = 5
*   - precision = 1.00
*  \author Pierre garcon
*  \date 06 fevrier
*  \fn int existe_tank, void afficher_tank, void position_tank, err_t detruire_tank, tank_t creer_tank
*/




/*

  IMPORTANT : definir une fonction qui remet à 1 les points d'attaques et de deplacements

*/

#include <stdio.h>
#include <stdlib.h>
#include "../../include/tank.h"
#include "../../include/nom_alea.h"

/*On défini la portée maximale des attaques et des déplacements*/
#define DEP 2
#define ATT 1


/*Définition des fonctions et méthodes*/
/**
 * \fn int existe_tank(tank_t * tank)
 * \brief fonction qui test si un assassin existe
 *
 * \param tank Assasin à tester
 *
 * \return int 0 | 1
 *
 */
extern
int existe_tank(tank_t * tank)
{
  if(tank == NULL)
    return(0);
  else
    return(1);
}

/*S'occupe de l'affuche de tous les parametres d'un tank*/
/**
 * \fn void afficher_tank(tank_t * tank)
 * \brief fonction qui affiche toute les caracteristique d'un tank
 *
 * \param tank Tank à afficher
 *
 * \return void pas de retour
 *
 */
static
void afficher_tank(tank_t * tank)
{
  if(existe_tank(tank))
  {
    if(tank->etat == Vivant)
    {
      printf("\nNom classe : tank");
      printf("\nNom : %s", tank->nom);
      printf("\nPV : %d", tank->pv);
      printf("\nDegats : %d", tank->degat);
      printf("\nDeplacements : %d", tank->portee.deplacement);
      printf("\nattaque : %d", tank->portee.attaques);
      printf("\nEsquive : %f", tank->esquive);
      printf("\ncritique : %f", tank->crit);
      printf("\nResistance : %d", tank->resistance);
      printf("\nPrecision : %f", tank->precision);
      if(tank->equipe == Bleu)
      {
        printf("\nL'tank est dans l'equipe des bleus");
      }
      else
      {
        printf("\nL'tank est dans l'equipe des rouges");
      }
      printf("\nFin des stat\n");
    }
    else
    {
      printf("\n\nL'tank est mort \n\n");
    }
  }
  else
  {
    printf("\nL'tank n'existe plus");
  }

}




/*Récuperer la vie d'un tank*/
/**
 * \fn int vie_tank(tank_t * tank)
 * \brief fonction qui recupere le nombre de pv d'un tank
 *
 * \param tank Tank à tester
 *
 * \return int tank->pv
 *
 */
static
int vie_tank(tank_t * tank)
{
  return tank->pv;
}




/*Récuperer les points d'attaque*/
/**
 * \fn int points_attaque_tank(tank_t * tank)
 * \brief fonction qui recupere le nombre de point d'attaque d'un tank
 *
 * \param tank Tank à tester
 *
 * \return int tank->pt_attaque
 *
 */
static
int points_attaque_tank(tank_t * tank)
{
  return tank->pt_attaque;
}





/*Récuperer les points de deplacement*/
/**
 * \fn int points_deplacement_tank(tank_t * tank)
 * \brief fonction qui recupere le nombre de point de deplacement d'un tank
 *
 * \param tank Tank à tester
 *
 * \return int tank->pt_deplacement
 *
 */
static
int points_deplacement_tank(tank_t * tank)
{
  return tank->pt_deplacement;
}


/*Récuperer les points de deplacement*/
/**
 * \fn int etat_tank(tank_t * tank)
 * \brief fonction qui recupere si un tank est vivant ou pas
 *
 * \param tank Tank à tester
 *
 * \return int tank->etat
 *
 */
static
int etat_tank(tank_t * tank)
{
  return tank->etat;
}





/*Récuperer les points de deplacement*/
/**
 * \fn int portee_deplacement_tank(tank_t * tank)
 * \brief fonction qui recupere la portee de deplacement d'un tank
 *
 * \param tank Tank à tester
 *
 * \return int tank->portee.deplacement
 *
 */
static
int portee_deplacement_tank(tank_t * tank)
{
  return tank->portee.deplacement;
}


/*Récuperer les points de deplacement*/
/**
 * \fn int portee_attaque_tank(tank_t * tank)
 * \brief fonction qui recupere la portee de d'attaque d'un tank
 *
 * \param tank Tank à tester
 *
 * \return int tank->portee.attaques
 *
 */
static
int portee_attaque_tank(tank_t * tank)
{
  return tank->portee.attaques;
}



/*Récuperer l'esquive de l'tank*/
/**
 * \fn float parade_tank(tank_t * tank)
 * \brief fonction qui recupere la chance d'esquive d'un tank
 *
 * \param tank Tank à tester
 *
 * \return float tank->esquive
 *
 */
static
float esquive_tank(tank_t * tank)
{
  return tank->esquive;
}




/*savoir si la compétence spéciale de l'tank est disponible*/
/**
 * \fn bool comp_spe_tank(tank_t * tank)
 * \brief fonction qui recupere si la compétance spéciale d'un tank est disponible
 *
 * \param tank Tank à tester
 *
 * \return bool tank->comp_special
 *
 */
static
bool comp_spe_tank(tank_t * tank)
{
  return tank->comp_special;
}







/*Récuperer la position de l'acher*/
/**
 * \fn void position_tank(int *x, int *y, tank_t * tank)
 * \brief fonction qui recupere la position d'un tank
 *
 * \param x Position X d'un tank
 * \param y Position Y d'un tank
 * \param tank Tank à tester
 *
 * \return void pas de retour
 *
 */
static
t_pos position_tank(tank_t * tank)
{
  t_pos position;
  position.x = 0;
  position.y = 0;
  if(existe_tank(tank))
  {
    if(tank->etat == Vivant)
    {
      position.x = tank->pos_x;
      position.y = tank->pos_y;
    }
  }
  return position;
}


/*
*Fonctions pour changer la direction des personnages
*/
/**
 * \fn void tank_haut(tank_t * tank)
 * \brief fonction qui met la position d'un tank à haut
 *
 * \param tank Tank à tester
 *
 * \return void
 *
 */
static
void tank_haut(tank_t * tank)
{
  tank->direction = haut;
}

/**
 * \fn void tank_bas(tank_t * tank)
 * \brief fonction qui met la position d'un tank à bas
 *
 * \param tank Tank à tester
 *
 * \return void
 *
 */
static
void tank_bas(tank_t * tank)
{
  tank->direction = bas;
}

/**
 * \fn void tank_droite(tank_t * tank)
 * \brief fonction qui met la position d'un tank à droite
 *
 * \param tank Tank à tester
 *
 * \return void
 *
 */
static
void tank_droite(tank_t * tank)
{
  tank->direction = droite;
}

/**
 * \fn void tank_gauche(tank_t * tank)
 * \brief fonction qui met la position d'un tank à gauche
 *
 * \param tank Tank à tester
 *
 * \return void
 *
 */
static
void tank_gauche(tank_t * tank)
{
  tank->direction = gauche;
}





/*Récuperer l'équipe d'un tank'*/
/**
 * \fn equipe_t * equipe_tank(tank_t * tank)
 * \brief fonction qui recupere l'équipe d'un tank
 *
 * \param tank Tank à tester
 *
 * \return tank->equipe
 *
 */
static
equipe_t  equipe_tank(tank_t * tank)
{
  return tank->equipe;
}



/*Récuperer la precision d'un tank*/
/**
 * \fn float precision_tank(tank_t * tank)
 * \brief fonction qui recupere la precision d'un tank
 *
 * \param tank Tank à tester
 *
 * \return float tank->precision
 *
 */
static
float precision_tank(tank_t * tank)
{
  return tank->precision;
}








/*Récuperer la position de l'acher*/
/**
 * \fn void modifier_position_tank(int x, int y, tank_t * tank)
 * \brief fonction qui permet de deplacer un tank
 *
 * \param x Position X d'un tank
 * \param y Position Y d'un tank
 * \param tank Tank à tester
 *
 * \return void pas de retour
 *
 */
static
void modifier_position_tank(int x, int y, tank_t * tank)
{
  if(existe_tank(tank))
  {
    if(tank->etat == Vivant)
    {
      if(tank->pt_deplacement != 0)
      {
        tank->pos_x = x;
        tank->pos_y = y;
        tank->pt_deplacement--;
      }
    }
  }
}








/*Diminue les PV d'un tank selon les dégats subis*/
/**
 * \fn etat_t perdre_vie_tank(int degat, tank_t * tank)
 * \brief fonction qui effectue des degat sur un tank et diminue leur pv
 *
 * \param degat Nombre de degat a inflicher au tank
 * \param tank Tank à tester
 *
 * \return Vivant | Mort | Rien
 *
 */
static
etat_t perdre_vie_tank(int degat, tank_t * tank)
{
  if(existe_tank(tank))
  {
    tank->pv = (tank->pv)-(degat-(tank->resistance));
    if((tank->pv)<=0)
    {
        tank->etat = Mort;
        return Mort;
    }
    else
    {
        return Vivant;
    }

  }
  return  Mort;
}

/*permet de detruire un tank*/
/**
 * \fn err_t detruire_tank(tank_t ** tank)
 * \brief fonction qui detruit une structure tank
 *
 * \param tank Tank à detruire
 *
 * \return Ok
 *
 */
static
err_t detruire_tank(tank_t ** tank)
{
  free((*tank));
  (*tank) = NULL;

  return(Ok);
}


/*On créer un nouvel tank, il n'y a pas de parametres car toutes les stats sont pré-défini*/
/**
 * \fn assassin_t * creer_assassin(equipe_t * equipe, race_t * race)
 * \brief fonction qui creer une structure tank
 *
 * \param equipe Dans lequel ajouter le tank
 * \param race De l' tank
 *
 * \return nouveau_ass
 *
 */
extern
tank_t * creer_tank(equipe_t equipe, race_t * race, liste_perso_t * liste)
{
  tank_t * nouveau_tank = malloc(sizeof(tank_t));
  char nom[25];

  donner_nom(nom);

  /*Création des caractéristiques des tanks*/
  nouveau_tank->classe = tank;
  strcpy(nouveau_tank->nom, nom);
  nouveau_tank->pv = 100;
  nouveau_tank->pv_max = 100;
  nouveau_tank->degat = 20;
  nouveau_tank->portee.deplacement = DEP;
  nouveau_tank->portee.attaques = ATT;
  nouveau_tank->esquive = 0.00;
  nouveau_tank->crit = 0.10;
  nouveau_tank->resistance = 5;
  nouveau_tank->precision = 1.00;
  nouveau_tank->pos_x = 0;
  nouveau_tank->pos_y = 0;
  nouveau_tank->parade = 30.0;
  nouveau_tank->etat = Vivant;
  nouveau_tank->liste = liste;
  nouveau_tank->pt_deplacement = 1;
  nouveau_tank->pt_attaque = 1;
  nouveau_tank->comp_special = true;
  nouveau_tank->race = race;
  nouveau_tank->direction = haut;
  nouveau_tank->equipe = equipe;
  nouveau_tank->type_race = race->race;

  /*Définition des compétences*/
  nouveau_tank->competence1 = coupepeesimple;
  nouveau_tank->competence2 = coupdebouclierrecul;
  nouveau_tank->competence3 = volontedudefenseur;

  nouveau_tank->info_comp1 = coupepeesimple_info;
  nouveau_tank->info_comp2 = coupdebouclierrecul_info;
  nouveau_tank->info_comp3 = volontedudefenseur_info;

  /*j'ai défini le type personnage_t qui va regrouper tous les types de personnages et permettre de faire des listes*/
  nouveau_tank->print =(void(*)(personnage_t* const)) afficher_tank;
  nouveau_tank->detruire = (err_t(*)(personnage_t**))detruire_tank;
  nouveau_tank->position_pers = (t_pos(*)(personnage_t*))position_tank;
  nouveau_tank->deplacer = (void(*)(int , int , personnage_t*))modifier_position_tank;
  nouveau_tank->f_subir_degats =(etat_t(*)(int, personnage_t*)) perdre_vie_tank;
  nouveau_tank->f_pt_deplacement = (int(*)(personnage_t *)) points_deplacement_tank;
  nouveau_tank->f_pt_attaque = (int(*)(personnage_t *)) points_attaque_tank;
  nouveau_tank->f_etat_pers = (etat_t(*)(personnage_t *)) etat_tank;
  nouveau_tank->f_vie = (int(*)(personnage_t *))vie_tank;
  nouveau_tank->f_esquive_parade = (float(*)(personnage_t *))esquive_tank;
  nouveau_tank->f_portee_attaque = (int(*)(personnage_t *))portee_attaque_tank;
  nouveau_tank->f_portee_deplacement = (int(*)(personnage_t *))portee_deplacement_tank;
  nouveau_tank->f_equipe = (equipe_t(*)(personnage_t *))equipe_tank;
  nouveau_tank->f_precision = (float(*)(personnage_t *))precision_tank;
  nouveau_tank->f_comp_spe = (bool(*)(personnage_t *))comp_spe_tank;
  nouveau_tank->tourner_haut = (void(*)(personnage_t *))tank_haut;
  nouveau_tank->tourner_bas = (void(*)(personnage_t *))tank_bas;
  nouveau_tank->tourner_droite = (void(*)(personnage_t *))tank_droite;
  nouveau_tank->tourner_gauche = (void(*)(personnage_t *))tank_gauche;



  return(nouveau_tank);
}
