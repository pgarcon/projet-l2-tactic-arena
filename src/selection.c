/**
 * \file Selection.c
 * \brief Fichier de selection des perssonages de l'équipe
 * \author Nicolas G
 * \version 1.0
 * \date 11 Mars 2021
 *
 * Sert à la composition d'une équipe pour Tactis Arena
 *
 */

#include "../include/selection.h"

//gcc src/Selection.c -o bin/Selection -I include -L lib -lmingw32 -lSDL2main -lSDL2 -lSDL2_image

/**
 * \fn int selection (void)
 * \brief Entrée du programme.
 *
 * \return EXIT_SUCCESS - Arrêt normal du programme.
 */
int selection(int mode){
    /**_*/
    int i = 0,j,k=0;/* Variable i element actuel du tableau choosen et j pour les tour de boucles d'affichage du tableau */

    int nbarc=0,nbmage=0,nbtank=0,nbass=0,nbheal=0,nbepe=0;

    char selection = 'A';/* Caractere representant la fiche de l'uniter actuellement afficher */
    char choosen[NB_PERS];/* Tableau contenant les perssonage choisie */

    SDL_Window *window = NULL;/* Fenetre dans laquel on affiche le jeu */
    SDL_Renderer *renderer = NULL;/* Rendu moteur de rendu de fenetre */
    SDL_Surface *icon = NULL;/* Surface ou sera charger l'icone de la fenetre */
    SDL_Surface *screen = NULL;/* Surface pour recuperer le contenue de la fenetre */
    SDL_Surface *background = NULL;/* Surface pour afficher le fond */
    SDL_Surface *choose = NULL;/* Surface ou seront charger les initial des type de perso choisie */
    SDL_Texture *texture = NULL;/* Texture pour placer les events */

    //Initialisation de la SDL
    if(SDL_Init(SDL_INIT_VIDEO)!=0)
        SDLError("Initalisation SDL"); 

    //Initialisation de la SDL image
    if(IMG_Init(IMG_INIT_PNG) != IMG_INIT_PNG){
        SDLError("Failed initialisation SDL Image");
        return EXIT_FAILURE;
    }

    //Creation de la fenetre et du rendu
    if(SDL_CreateWindowAndRenderer(WINDOW_WIDTH_SELECTION,WINDOW_HEIGHT_SELECTION,0,&window,&renderer)!=0)
        SDLError("Impossible de creer le rendue et la fenêtre");

    //Test d'erreur de creation de la fenetre
    if(window==NULL)
        SDLError("Creation de la fenetre");

    //Test d'erreur de creation du rendu
    if(renderer==NULL)
        SDLError("Creation Rendue Echoue");

    //Definition du titre de la fenetre
    SDL_SetWindowTitle(window,"Tactis Arena");

    //Chargement et mis en place de l'icone de la fenetre
    icon = IMG_Load("../images/icon.png");
    if(!icon){
        SDLError("Failed to Load Image Icon");
    }

    SDL_SetWindowIcon(window,icon);

    //Chargement de l'image de fond
    background=IMG_Load("../images/Selection/Archer2.png");
    if(background==NULL)
    {
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        SDLError("Erreur lors du chargement de l'image");
    }

    //Affectation de la texture
    texture=SDL_CreateTextureFromSurface(renderer,background);
    
    if(texture==NULL)
    {
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        SDLError("Erreur lors de la création de la texture");
    }

    //chargement de la texture
    SDL_Rect rectangle;
    if(SDL_QueryTexture(texture,NULL,NULL,&rectangle.w,&rectangle.h)!=0)
    {
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        SDLError("Erreur lors de l'affichage de l'image");
    }

    //centrage de l'image
    rectangle.x=(WINDOW_WIDTH_SELECTION-rectangle.w)/2;
    rectangle.y=(WINDOW_HEIGHT_SELECTION-rectangle.h)/2;
    //
    //affichage de l'image

    if(SDL_RenderCopy(renderer,texture,NULL, &rectangle)!=0)
    {
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        SDLError("Erreur lors de l'affichage de l'image");
    }

    SDL_RenderPresent(renderer);

    //Recuperation de ce qui est afficher
    screen = SDL_GetWindowSurface(window);

    //Mis a jour de la fenetre avec screen pour afficher tout les elements 
    SDL_BlitSurface(background,NULL,screen,NULL);
    SDL_FreeSurface(background);
    SDL_UpdateWindowSurface(window);

    //
    //Gestion des évènements*********************************
    SDL_bool program_launched=SDL_TRUE;
    SDL_Event event;

    //Définition des intervalles de position des différents onglets de la fenetre
    t_position LeftArrow={36,83,250,306};
    t_position RightArrow={731,778,251,307};
    t_position SelectionButton={430,624,441,503};
    t_position Suppr={480,545,512,550};

    //Initialisation du tableau de caractere avec un espace (equivalence NULL)
    for(j=0;j<NB_PERS;j++){
        choosen[j]=' ';
    }

    //Rectangle de positionnement de la liste des persso choisie 
    SDL_Rect posSelect;
    posSelect.y = 67;

    //On execute le programme tant que l'utilisateur ne quitte pas ou que le nombre de perssonage max n'est pas attein
    if(mode == Partage){
        while(k<2){

            while (program_launched && i < NB_PERS)
            {
                //Affichage des personnage choisie formant l'équipe
                for(j=0,posSelect.x=317;j<NB_PERS;j++){
                    if(choosen[j]!=' '){
                        posSelect.x += 63;//incremente la position x                
                        DisplaySelection(choosen[j],window,choose,screen,posSelect);
                    }
                }

                while (SDL_PollEvent(&event))
                {   

                    switch (event.type)
                    {
                    //Ferme le programme si on appuis sur la croix 
                    case SDL_QUIT:
                        program_launched=SDL_FALSE;
                        return EXIT_FAILURE;
                        break;
                    //gestion clavier pour toutes les action disponible quitter,naviguer,selectionner et supprimer une uniter
                    case SDL_KEYDOWN:
                        switch (event.key.keysym.sym)
                        {
                            case SDLK_ESCAPE:
                                program_launched=SDL_FALSE;
                                return EXIT_FAILURE;
                                break;
                            
                            case SDLK_DELETE:
                                printf("Suppr press\n");
                                if(i!=0){
                                    switch(choosen[--i]){
                                    case 'A':
                                        nbarc--;
                                        break;
                                    case 'M':
                                        nbmage--;
                                        break;
                                    case 'T':
                                        nbtank--;
                                        break;
                                        
                                    case 'S':
                                        nbass--;
                                        break;
                                        
                                    case 'H':
                                        nbheal--;
                                        break;
                                        
                                    case 'E':
                                        nbepe--;
                                        break;
                                    }
                                    choosen[i] = ' ';
                                    ReloadScreen(selection,window,background,screen);
                                }
                                break;
                            
                            case SDLK_LEFT:
                                selection = GoLeft(selection,window,background,screen);
                                break;
                            
                            case SDLK_RIGHT:
                                selection = GoRight(selection,window,background,screen);
                                break;

                            case SDLK_RETURN:
                                switch(selection){
                                    case 'A':
                                        if(nbarc<2){
                                            nbarc++;
                                            PressEnter(selection,choose);
                                            choosen[i++]=selection;//Ajoute la lettre de l'uniter actuel et incremente l'indice
                                        }  
                                        else{
                                            printf("too many archer\n");
                                        }
                                        break;
                                    case 'M':
                                        if(nbmage<2){
                                            nbmage++;
                                            PressEnter(selection,choose);
                                            choosen[i++]=selection;//Ajoute la lettre de l'uniter actuel et incremente l'indice
                                        }  
                                        else{
                                            printf("too many mage\n");
                                        }
                                        break;
                                    case 'T':
                                        if(nbtank<2){
                                            nbtank++;
                                            PressEnter(selection,choose);
                                            choosen[i++]=selection;//Ajoute la lettre de l'uniter actuel et incremente l'indice
                                        }  
                                        else{
                                            printf("too many tank\n");
                                        }
                                        break;
                                        
                                    case 'S':
                                        if(nbass<2){
                                            nbass++;
                                            PressEnter(selection,choose);
                                            choosen[i++]=selection;//Ajoute la lettre de l'uniter actuel et incremente l'indice
                                        }  
                                        else{
                                            printf("too many assassins\n");
                                        }
                                        break;
                                        
                                    case 'H':
                                        if(nbheal<2){
                                            nbheal++;
                                            PressEnter(selection,choose);
                                            choosen[i++]=selection;//Ajoute la lettre de l'uniter actuel et incremente l'indice
                                        }  
                                        else{
                                            printf("too many healer\n");
                                        }
                                        break;
                                        
                                    case 'E':
                                        if(nbepe<2){
                                            nbepe++;
                                            PressEnter(selection,choose);
                                            choosen[i++]=selection;//Ajoute la lettre de l'uniter actuel et incremente l'indice
                                        }  
                                        else{
                                            printf("too many epeiste\n");
                                        }
                                        break;
                                }
                                
                                break;

                            default:
                                continue;
                        }
                    break;
                    //
                    //Gestion souris

                    case SDL_MOUSEBUTTONUP:
                        
                        if(((event.motion.x>=LeftArrow.minX)&&(event.motion.x<=LeftArrow.maxX))&&((event.motion.y>=LeftArrow.minY)&&(event.motion.y<=LeftArrow.maxY))){
                            //Quand on clique sur la fleche de gauche affiche l'uniter précédante
                            selection = GoLeft(selection,window,background,screen);

                        }

                        if(((event.motion.x>=RightArrow.minX)&&(event.motion.x<=RightArrow.maxX))&&((event.motion.y>=RightArrow.minY)&&(event.motion.y<=RightArrow.maxY))){
                            //Quand on clique sur la fleche de droite affiche l'uniter suivante
                            selection = GoRight(selection,window,background,screen);
                
                        }

                        if(((event.motion.x>=SelectionButton.minX)&&(event.motion.x<=SelectionButton.maxX))&&((event.motion.y>=SelectionButton.minY)&&(event.motion.y<=SelectionButton.maxY))){
                            //Quand on clique sur le boutton de selection ajoute la lettre de l'uniter actuelle au tableau et incremente l'indice
                            switch(selection){
                            case 'A':
                                if(nbarc<2){
                                    nbarc++;
                                    PressEnter(selection,choose);
                                    choosen[i++]=selection;//Ajoute la lettre de l'uniter actuel et incremente l'indice
                                }  
                                else{
                                    printf("too many archer\n");
                                }
                                break;
                            case 'M':
                                if(nbmage<2){
                                    nbmage++;
                                    PressEnter(selection,choose);
                                    choosen[i++]=selection;//Ajoute la lettre de l'uniter actuel et incremente l'indice
                                }  
                                else{
                                    printf("too many mage\n");
                                }
                                break;
                            case 'T':
                                if(nbtank<2){
                                    nbtank++;
                                    PressEnter(selection,choose);
                                    choosen[i++]=selection;//Ajoute la lettre de l'uniter actuel et incremente l'indice
                                }  
                                else{
                                    printf("too many tank\n");
                                }
                                break;
                                        
                            case 'S':
                                if(nbass<2){
                                    nbass++;
                                    PressEnter(selection,choose);
                                    choosen[i++]=selection;//Ajoute la lettre de l'uniter actuel et incremente l'indice
                                }  
                                else{
                                    printf("too many assassins\n");
                                }
                                break;
                                        
                            case 'H':
                                if(nbheal<2){
                                    nbheal++;
                                    PressEnter(selection,choose);
                                    choosen[i++]=selection;//Ajoute la lettre de l'uniter actuel et incremente l'indice
                                }  
                                else{
                                    printf("too many healer\n");
                                }
                                break;
                                        
                            case 'E':
                                if(nbepe<2){
                                    nbepe++;
                                    PressEnter(selection,choose);
                                    choosen[i++]=selection;//Ajoute la lettre de l'uniter actuel et incremente l'indice
                                }  
                                else{
                                    printf("too many epeiste\n");
                                }
                                break;
                            default:
                                continue;
                            }
                        }
                            
                        if(((event.motion.x>=Suppr.minX)&&(event.motion.x<=Suppr.maxX))&&((event.motion.y>=Suppr.minY)&&(event.motion.y<=Suppr.maxY))){
                            //Quand on clique sur le boutton de suppression decremente l'indice puis remplace la lettre par un espace (equivalent NULL) et actualise l'écran
                            printf("Suppr press\n");
                            if(i!=0){
                                switch(choosen[--i]){
                                case 'A':
                                     nbarc--;
                                    break;
                                case 'M':
                                    nbmage--;
                                    break;
                                case 'T':
                                    nbtank--;
                                    break;     
                                case 'S':
                                    nbass--;
                                    break;           
                                case 'H':
                                    nbheal--;
                                    break;          
                                case 'E':
                                    nbepe--;
                                    break;
                                }
                                choosen[i] = ' ';
                                ReloadScreen(selection,window,background,screen);
                            }
                            break;
                        }
                    }
                }
            }
            //Affiche en console la liste total des persso choisie
            for(i=0;i<NB_PERS;i++){
                printf(" %c ",choosen[i]);
            }
            printf("\n");
            k++;

            nbarc=nbmage=nbtank=nbass=nbheal=nbepe=0;

            FILE * saveSelection;


            //Enregistre les perssonage choisie dans des fichiers
            if(k==1)
                saveSelection = fopen("../team_files/equipe_1.txt","w");
            else if(k==2)
                saveSelection = fopen("../team_files/equipe_2.txt","w");

            for(i=0;i<NB_PERS;i++){
                switch (choosen[i]){
                case 'A':
                    fprintf(saveSelection," %i ",archer);
                    break;

                case 'M':
                    fprintf(saveSelection," %i ",mage);
                    break;

                case 'T':
                    fprintf(saveSelection," %i ",tank);
                    break;
                
                case 'S':
                    fprintf(saveSelection," %i ",assassin);
                    break;

                case 'H':
                    fprintf(saveSelection," %i ",healer);
                    break;

                case 'E':
                    fprintf(saveSelection," %i ",epeiste);
                    break;

                }
            }
            fclose(saveSelection);
            
            for(i=0;i<NB_PERS;i++){
                choosen[i]=' ';
            }
            ReloadScreen(selection,window,background,screen);
            i=0;

        }

    }


    

    //*******************************************************
    //*******************************************************
    //Destruction
    SDL_FreeSurface(icon);
    SDL_FreeSurface(screen);
    SDL_FreeSurface(choose);
    SDL_DestroyTexture(texture);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    IMG_Quit();
    SDL_Quit();
    //*******************************************************

    return EXIT_SUCCESS;//return 0;
}

//***********************************************************
//*********************END OF MAIN***************************
//***********************************************************



/**
 * \fn void SDLError(const char *message)
 * \brief Fonction de message d'erreurs SDL
 *
 * \param message message à afficher si on rentre dans la fonction plus quitte SDL
 * \return EXIT_FAILURE
 */
void SDLError(const char *message)
{
    /**_*/
    SDL_Log("Erreur : %s > %s ! \n",message,SDL_GetError());
    SDL_Quit();
    exit(EXIT_FAILURE);

}

/**
 * \fn void DisplaySelection(char choosen, SDL_Window *window, SDL_Surface *choose, SDL_Surface *screen, SDL_Rect posSelect)
 * \brief Fonction d'affichage du perssonages choisie 
 *
 * \param choosen Caractere definissant le type de perssonages
 * \param window  Fenetre sur laquel afficher la surface
 * \param choose  Surface pour charger l'image du type de perssonage choisie
 * \param screen  Surface de destination sur laquel on va blitter choose
 * \param posSelect Rectangle de coordoner ou l'on va afficher choose
 * 
 * \return void pas de retour
 */
void DisplaySelection(char choosen, SDL_Window *window, SDL_Surface *choose, SDL_Surface *screen, SDL_Rect posSelect)
{
    //Selon le caractere passer parametre on charge l'image en consequence
    switch(choosen)
    {

    case 'A':
        choose = IMG_Load("../images/Selection/Ar.png");
        if(!choose)
        {
            SDLError("Failed to Load choose");
        }
        break;

    case 'M':
        choose = IMG_Load("../images/Selection/Ma.png");
        if(!choose)
        {
            SDLError("Failed to Load choose");
        }
        break;

    case 'T':
        choose = IMG_Load("../images/Selection/Ta.png");
        if(!choose)
        {
            SDLError("Failed to Load choose");
        }
        break;

    case 'E':
        choose = IMG_Load("../images/Selection/Ep.png");
        if(!choose)
        {
            SDLError("Failed to Load choose");
        }
        break;

    case 'S':
        choose = IMG_Load("../images/Selection/As.png");
        if(!choose)
        {
            SDLError("Failed to Load choose");
        }
        break;

    case 'H':
        choose = IMG_Load("../images/Selection/He.png");
        if(!choose)
        {
            SDLError("Failed to Load choose");
        }
        break;

    }

    //Affichage de l'image charger sur le fond
    SDL_BlitSurface(choose,NULL,screen,&posSelect);
    SDL_UpdateWindowSurface(window);
    SDL_FreeSurface(choose);
}


/**
 * \fn void ReloadScreen(char selection, SDL_Window *window, SDL_Surface *background, SDL_Surface *screen)
 * \brief Fonction pour rafraichir l'écran recharge le fond en fonction de l'uniter actuellement afficher
 *
 * \param selection Caractere definissant le type de perssonages
 * \param window  Fenetre sur laquel afficher la surface
 * \param background  Surface pour charger l'image du type de perssonage choisie
 * \param screen  Surface de destination sur laquel on va blitter choose
 * 
 * \return void pas de retour 
 */
void ReloadScreen(char selection, SDL_Window *window, SDL_Surface *background, SDL_Surface *screen){
    //Selon le caractere Selection affiche la fiche de l'uniter en question
    switch(selection){
                        
        case 'A':   background = IMG_Load("../images/Selection/Archer2.png");
                    if(!background){
                        SDLError("Failed to Load Image");
                    }
                    break;
                            
        case 'M':   background = IMG_Load("../images/Selection/Mages2.png");
                    if(!background){
                        SDLError("Failed to Load Image");
                    }
                    break;
                            
        case 'T':   background = IMG_Load("../images/Selection/Tanks2.png");
                    if(!background){
                        SDLError("Failed to Load Image");
                    }
                    break;

        case 'E':   background = IMG_Load("../images/Selection/Epéistes2.png");
                    if(!background){
                        SDLError("Failed to Load Image");
                    }
                    break;
                            
        case 'S':   background = IMG_Load("../images/Selection/Assassins2.png");
                    if(!background){
                        SDLError("Failed to Load Image");
                    }
                    break;
                                                    
        case 'H':   background = IMG_Load("../images/Selection/Healer2.png");
                    if(!background){
                        SDLError("Failed to Load Image");
                    }
                    break;

    }

    //Affiche l'image charger sur le fond              
    SDL_BlitSurface(background,NULL,screen,NULL);

    SDL_FreeSurface(background);
    SDL_UpdateWindowSurface(window);

}


/**
 * \fn char GoLeft(char selection, SDL_Window *window, SDL_Surface *background, SDL_Surface *screen )
 * \brief Fonction pour afficher la page de l'unité précédante
 *
 * \param selection Caractere definissant le type de perssonages
 * \param window  Fenetre sur laquel afficher la surface
 * \param background  Surface pour charger l'image du type de perssonage choisie
 * \param screen  Surface de destination sur laquel on va blitter choose
 * 
 * \return char retour du caractere du nouveau perssonage afficher
 */
char GoLeft(char selection, SDL_Window *window, SDL_Surface *background, SDL_Surface *screen ){
        printf("Left Arrow Press\n");
        //Selon le caractere passer en parametre charge l'image de l'uniter precedante concerner et met a jour le caractere avec le nouveaux fond
        switch(selection)
        {
        case 'A':
            background = IMG_Load("../images/Selection/Healer2.png");
            if(!background)
            {
                SDLError("Failed to Load Image");
            }
            selection = 'H';
            break;

        case 'H':
            background = IMG_Load("../images/Selection/Assassins2.png");
            if(!background)
            {
                SDLError("Failed to Load Image");
            }
            selection = 'S';
            break;

        case 'S':
            background = IMG_Load("../images/Selection/Epéistes2.png");
            if(!background)
            {
                SDLError("Failed to Load Image");
            }
            selection = 'E';
            break;

        case 'E':
            background = IMG_Load("../images/Selection/Tanks2.png");
            if(!background)
            {
                SDLError("Failed to Load Image");
            }
            selection = 'T';
            break;

        case 'T':
            background = IMG_Load("../images/Selection/Mages2.png");
            if(!background)
            {
                SDLError("Failed to Load Image");
            }
            selection = 'M';
            break;

        case 'M':
            background = IMG_Load("../images/Selection/Archer2.png");
            if(!background)
            {
                SDLError("Failed to Load Image");
            }
            selection = 'A';
            break;
        }

        //Affiche l'image charger sur le fond
        SDL_BlitSurface(background,NULL,screen,NULL);

        SDL_FreeSurface(background);
        SDL_UpdateWindowSurface(window);

        //Retourne le caractere du nouveau perssonage afficher
        return selection;

    }

/**
 * \fn char GoRight(char selection, SDL_Window *window, SDL_Surface *background, SDL_Surface *screen )
 * \brief Fonction pour afficher la page de l'unité suivante
 *
 * \param selection Caractere definissant le type de perssonages
 * \param window  Fenetre sur laquel afficher la surface
 * \param background  Surface pour charger l'image du type de perssonage choisie
 * \param screen  Surface de destination sur laquel on va blitter choose
 * 
 * \return char retour du caractere du nouveau perssonage afficher
 */
char GoRight(char selection, SDL_Window *window, SDL_Surface *background, SDL_Surface *screen ){
    printf("Right Arrow Press\n");
    //Selon le caractere passer en parametre charge l'image de l'uniter suivant concerner et met a jour le caractere avec le nouveaux fond
    switch(selection)
    {

    case 'A':
        background = IMG_Load("../images/Selection/Mages2.png");
        if(!background)
        {
            SDLError("Failed to Load Image");
        }
        selection = 'M';
        break;

    case 'M':
        background = IMG_Load("../images/Selection/Tanks2.png");
        if(!background)
        {
            SDLError("Failed to Load Image");
        }
        selection = 'T';
        break;

    case 'T':
        background = IMG_Load("../images/Selection/Epéistes2.png");
        if(!background)
        {
            SDLError("Failed to Load Image");
        }
        selection = 'E';
        break;

    case 'E':
        background = IMG_Load("../images/Selection/Assassins2.png");
        if(!background)
        {
            SDLError("Failed to Load Image");
        }
        selection = 'S';
        break;

    case 'S':
        background = IMG_Load("../images/Selection/Healer2.png");
        if(!background)
        {
            SDLError("Failed to Load Image");
        }
        selection = 'H';
        break;

    case 'H':
        background = IMG_Load("../images/Selection/Archer2.png");
        if(!background)
        {
            SDLError("Failed to Load Image");
        }
        selection = 'A';
        break;


    }

    //Affiche l'image charger sur le fond
    SDL_BlitSurface(background,NULL,screen,NULL);

    SDL_FreeSurface(background);
    SDL_UpdateWindowSurface(window);

    //Retourne le caractere du nouveau perssonage afficher
    return selection;

}

/**
 * \fn void PressEnter(char selection, SDL_Surface *choose)
 * \brief Fonction pour afficher les initial de l'uniter séléctioner
 *
 * \param selection Caractere definissant le type de perssonages
 * \param choose Surface pour charger l'image du type de perssonage choisie
 * 
 * \return void pas de retour
 */
void PressEnter(char selection, SDL_Surface *choose){
    printf("Selection Button Press\n");
    //En fonction du perssonage passer en parametre charge l'image des initial de l'uniter
    switch(selection)
    {

    case 'A':
        choose = IMG_Load("../images/Selection/Ar.png");
        if(!choose)
        {
            SDLError("Failed to Load choose");
        }
        break;

    case 'M':
        choose = IMG_Load("../images/Selection/Ma.png");
        if(!choose)
        {
            SDLError("Failed to Load choose");
        }
        break;

    case 'T':
        choose = IMG_Load("../images/Selection/Ta.png");
        if(!choose)
        {
            SDLError("Failed to Load choose");
        }
        break;

    case 'E':
        choose = IMG_Load("../images/Selection/Ep.png");
        if(!choose)
        {
            SDLError("Failed to Load choose");
        }
        break;

    case 'S':
        choose = IMG_Load("../images/Selection/As.png");
        if(!choose)
        {
            SDLError("Failed to Load choose");
        }
        break;

    case 'H':
        choose = IMG_Load("../images/Selection/He.png");
        if(!choose)
        {
            SDLError("Failed to Load choose");
        }
        break;
    }

}