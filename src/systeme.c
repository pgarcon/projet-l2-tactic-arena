/**
 * \file systeme.c
 * \brief Fichier d'avantage
 * \author 
 * \version 1.0
 * \date 
 *
 * test les classe de deux perssonage et retourne un float si avantage > 1 ou < 1 pour desavantage
 *
 */

#include <include/systeme.h>

/**
 * \fn float Systeme(personnage_t * perso1, personnage_t * perso2)
 * \brief fonction main
 * 
 * \param perso1 Classe sur lequel on test l'avantage
 * \param perso2 Classe Adverssaire 
 * 
 * \return float 0.80 | 1 | 1.20
 * 
 */
extern
float Systeme(personnage_t * perso1, personnage_t * perso2)
{
    if (perso1->classe=='archer')
    {
        if(perso2->classe=='assassin')
            return 0.80;
        else if (perso2->classe=='epeiste')
        {
            return 1.20;
        }
        else
            return 1;
    }
    
    if (perso1->classe=='mage')
    {
        if(perso2->classe=='epeiste')
            return 0.80;
        else if (perso2->classe=='tank')
        {
            return 1.20;
        }
        else
            return 1;
    }

    if (perso1->classe=='tank')
    {
        if(perso2->classe=='Mage')
            return 0.80;
        else if (perso2->classe=='assassin')
        {
            return 1.20;
        }
        else
            return 1;
        
    }

    if (perso1->classe=='assassin')
    {
        if(perso2->classe=='tank')
            return 0.80;
        else if (perso2->classe=='archer')
        {
            return 1.20;
        }
        else
            return 1;
    }


    if (perso1->classe=='healer')
    {
        return 1;
    }

    if (perso1->classe=='epeiste')
    {
        if(perso2->classe=='archer')
            return 0.80;
        else if (perso2->classe=='mage')
        {
            return 1.20;
        }
        else
            return 1;
    }
}