/**
 * \file test_assassin.c
 * \brief Fichier de test de la classe assassin
 * \author Pierre Garcon
 * \version 1.0
 * \date 2 fevrier
 *
 * Test de toute les caracteristique de la classe assassin
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include "../include/assassin.h"


  /*On créer les variables de travailles*/
/**
 * \fn int main (void)
 * \brief Fonction main
 *
 * \return int 0
 */
int main()
{
  /**_*/
  err_t erreur;
  etat_t etat;
  equipe_t coul_equipe;
  liste_perso_t * equipe = NULL;
  race_t * orc = race_creer_orc();
  race_t * demon = race_creer_demon();

  coul_equipe = Bleu;

  /*On commence par tester la création des assassins*/
  printf("\nCeci est un test de assassins");
  assassin_t * ass1, *ass2;
  printf("\n Test de création d'un assassin : \n");
  ass1 = creer_assassin(coul_equipe, orc, equipe);
  ass2 = creer_assassin(coul_equipe, demon, equipe);

  /*Ensuite, on test l'affichage*/
  printf("\n Test d'affichage : \n");
  ass1->print((personnage_t*)ass1);
  printf("\nPersonnage 2 :\n");
  ass2->print((personnage_t*)ass2);


  /*On active les bonus des deux personnages (bonus différents en fonction des races)*/
  ass1->race->race_activer_bonus(ass1);
  ass2->race->race_activer_bonus(ass2);

  /*On affiche les personnage après activation des bonus pour voir si ils ont massé*/
  printf("\n--------------------------------------------------------------------\n");
  printf("\nAffichage après activation du bonus\n");

  printf("\n Test d'affichage : \n");
  ass1->print((personnage_t*)ass1);
  printf("\nPersonnage 2 :\n");
  ass2->print((personnage_t*)ass2);
  printf("\n--------------------------------------------------------------------\n");

  /*On test l'attaque sur les archer*/
  printf("\nTest d'attaque sur un archer : \n");

  ass2->deplacer(4,2,(personnage_t*)ass2);

  printf("\naffichage des coordonnées de archer2\n\n");
  printf("x : %d \ny : %d", ass2->pos_x, ass2->pos_y);

  statut_att comp = ass1->competence1((personnage_t*)ass1, ass2->position_pers((personnage_t*)ass2));
  afficher_statut_attaque(comp);

  etat = Vivant;

  /*On vérifie l'état de l'assassin*/
  if(etat == Vivant)
  {
    printf("\nL'assassin est toujours vivant");
  }
  else
  {
    printf("\nL'assassin est mort\n");
  }

  /*On désactive les bonus*/
  ass1->race->race_desactiver_bonus(ass1);
  ass2->race->race_desactiver_bonus(ass2);

  /*On test l'affichage et on supprime les assassins*/
  printf("\nAfficher après dégats et les bonus sont enlevés : \n");
  ass1->print((personnage_t*)ass1);

  printf("\ntest de suppression : \n");
  erreur = ass1->detruire((personnage_t**)&ass1);
  erreur = ass2->detruire((personnage_t **)&ass2);

  demon->detruire(&demon);
  orc->detruire(&orc);

  if(erreur != Ok)
  {
    printf("\nerreur de suppression\n");
  }
  else
    printf("\nTout s'est bien passé\n");
  return 0;
}
