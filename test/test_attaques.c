#include <stdio.h>
#include "../include/attaques.h"
#include "../include/liste_positions.h"
#include "../include/map.h"

void test_attaques(personnage_t * perso1, personnage_t * perso2, personnage_t * perso3);


void test_algorithme_cibles_zone(t_pos case_cible, int taille_zone)
{
	/* Algorithme de recherche des cases comprises dans la zone */
	printf("Attaque lancée sur la case : [%i,%i]\n", case_cible.x, case_cible.y);
	printf("Recherche de personnages sur les cases :");
	for(int x = -taille_zone; x <= taille_zone; x++)
	{	
		int cov_y = taille_zone - abs(x); 
		for(int y = -cov_y; y <= cov_y ; y++)
		{
			int seek_x = case_cible.x - x;
			int seek_y = case_cible.y - y;
			if(seek_x >= 0 && seek_x < N && seek_y >=0 && seek_y < M)
			{
				printf(" [%i,%i],", seek_x, seek_y);
			}
			
		}
	}
	printf("[end]\n");
}



int main()
{
	srand(time(NULL));
	//Création de deux personnages tests
	personnage_t * perso1;
	personnage_t * perso2;
	personnage_t * perso3;
	

	perso1 = malloc(sizeof(personnage_t));
	t_pos pos_p1;
	pos_p1.x = 5;
	pos_p1.y = 6;
	map[5][6].perso = perso1;
	perso1->pos_x = pos_p1.x;
	perso1->pos_y = pos_p1.y;
	perso1->pv = 54;
	perso1->esquive = 0.60;
	perso1->resistance = 4;
	perso1->classe=archer;
	perso1->etat = Vivant;
	printf("PERSO1 : %p, PV : %i \n", perso1, perso1->pv);

	perso2 = malloc(sizeof(personnage_t));
	t_pos pos_p2;
	pos_p2.x = 2;
	pos_p2.y = 3;
	map[2][3].perso = perso2;
	perso2->pos_x = pos_p2.x;
	perso2->pos_y = pos_p2.y;
	perso2->pv = 154;
	perso2->esquive = 0.15;
	perso2->resistance = 8;
	perso2->classe=assassin;
	perso2->etat = Vivant;
	printf("PERSO2 : %p, PV : %i \n", perso2, perso2->pv);

	perso3 = malloc(sizeof(personnage_t));
	t_pos pos_p3;
	pos_p3.x = 4;
	pos_p3.y = 3;
	map[2][3].perso = perso3;
	perso3->pos_x = pos_p3.x;
	perso3->pos_y = pos_p3.y;
	perso3->pv = 120;
	perso3->esquive = 0.15;
	perso3->resistance = 3;
	perso3->classe=epeiste;
	perso3->etat = Vivant;
	printf("PERSO3 : %p, PV : %i \n", perso3, perso3->pv);
	


	//Test fonction de tirage aléatoire
	printf("Test de la fonction de tirage aléatoire des dommages :\n");
	int tirage;
	for(int z = 0; z < 10; z++)
	{	
		tirage = tirage_jet_dommage(-5, 10);
		printf("Tirage aléatoire entre -5 et 10 : %i \n", tirage);
		if(tirage < -5 || tirage > 10)
		{
			fprintf(stderr, "Erreur dans le tirage aléatoire");
		}
	}

	//Test fonction d'échec
	for(int i = 0; i <3; i++)
	{
		int nb_echecs = 0;
		for(int z = 0; z < 100; z++)
		{
			if(echec_attaque() == 1)
			{
				nb_echecs++;
			}
		}
		printf("TEST D'ALEATOIRITE DES ECHECS %i : Pour un taux de 1 sur 100, le nombre d'échecs sur 100 tentatives est de : %i \n", i+1, nb_echecs);
	}

	//Test fonction d'esquive
	for(int i = 0; i <3; i++)
	{
		int nb_esquives = 0;
		for(int z = 0; z < 100; z++)
		{
			if(esquiver_attaque(perso1) == 1)
			{
				nb_esquives++;
			}
		}
		printf("TEST D'ALEATOIRITE DES ESQUIVES %i : Pour 60 pour 100 chances d'esquiver, le nombre d'esquives sur 100 tentatives est de : %i \n", i+1, nb_esquives);
	}

	//Recherche de cibles dans une zone
	printf("\nTest de la fonction de recherche des cibles dans une zone : Les cases ne doivent pas déborder de la map en 18 x 22\nLes valeurs pour x doivent être comprises entre 0 et 17 , pour y entre 0 et 21\n");
	t_pos cible_attaque = {0, 0};
	printf("Test avec attaque sur [%i,%i] \n", cible_attaque.x, cible_attaque.y);
	test_algorithme_cibles_zone(cible_attaque, 2);
	cible_attaque.x = N-1;
	cible_attaque.y = M-1;
	printf("Test avec attaque sur [%i,%i] \n", cible_attaque.x, cible_attaque.y);
	test_algorithme_cibles_zone(cible_attaque, 2);

	


	//TEST DE FONCTION DE COMPARAISON DE POSITION
	printf("\nTest de fonction de comparaison de position\n");
	t_pos case1, case2;
	case1.x = 1;
	case1.y = 2;
	case2.x = -1;
	case2.y = -2;
	printf("Valeur de retour de comparaison de la grandeur des chemins de coordonnées [%i,%i] et [%i,%i] : %i\n", case1.x, case1.y, case2.x, case2.y, comparaison_position(case1.x, case1.y, case2.x, case2.y));


	case1.x = 1;
	case1.y = 3;
	case2.x = -1;
	case2.y = -2;
	printf("Valeur de retour de comparaison de la grandeur des chemins de coordonnées [%i,%i] et [%i,%i] : %i\n", case1.x, case1.y, case2.x, case2.y, comparaison_position(case1.x, case1.y, case2.x, case2.y));


	case1.x = 1;
	case1.y = 0;
	case2.x = -1;
	case2.y = -2;
	printf("Valeur de retour de comparaison de la grandeur des chemins de coordonnées [%i,%i] et [%i,%i] : %i\n", case1.x, case1.y, case2.x, case2.y, comparaison_position(case1.x, case1.y, case2.x, case2.y));


	printf("Test de fonction de tri des personnages du plus proche au plus lointain selon une zone donnée\n");
	t_pos case_test = {5,5};
	personnage_t** liste_perso = trouver_la_cible_la_plus_proche(case_test, 5);
	int i = 0;
	for(i = 0; i < NB_CASES_TABLEAU_CIBLES; i++)
	{
		if(liste_perso[i] != NULL)
		{
			printf("%i : PERSO %p , position [%i,%i]\n", i, liste_perso[i], liste_perso[i]->pos_x, liste_perso[i]->pos_y);
		}
		//printf("\nliste_perso[%i] = %p \n", i, liste_perso[i]);
	}

	t_pos test_j = {5,4};
	t_pos test_c = {4,4};
	if(cible_corps_a_corps(test_j, test_c) == 0)
	{
		printf("La cible est au corps à corps\n");
	}

	test_attaques(perso1, perso2, perso3);


	printf("Quels personnages sont morts ?\n");
	if(perso1->etat == Mort) printf("%p est mort\n", perso1);
	else printf("%p est vivant\n", perso1);
	if(perso2->etat == Mort) printf("%p est mort\n", perso2);
	else printf("%p est vivant\n", perso2);
	if(perso3->etat == Mort) printf("%p est mort\n", perso3);
	else printf("%p est vivant\n", perso3);

	free(liste_perso);
	free(perso1);
	free(perso2);
	free(perso3);
}

void test_attaques(personnage_t * perso1, personnage_t * perso2, personnage_t * perso3) //Utilise un switch pour tester chacune des attaques disponibles
{
	int choix = -1;
	t_pos cible_attaque;
	while(choix != 0)
	{
		printf("\n\nMenu de test des attaques \nListe des attaques : \n\n");
		printf("ARCHERS : \n	1: Tir de l'aigle\n	2: Tir de combat\n	3: Barrage Absolu\n");
		printf("ASSASSINS : \n	4: Coup de daggue\n	5: Lancer de dagues\n	6: Béni par les dieux du sang\n");
		printf("MAGES : \n	7: Firaball\n	8: Fabriquant de Glaçons\n	9: Interitum Notitia\n");
		printf("HEALERS : \n	10: Bénédiction divine\n	11: Transfert de Vitalité\n	12: Grâce de la déesse\n");
		printf("TANK : \n	13: Coup d'épée simple\n	14: Coup de bouclier recul\n	15: Volonté du défenseur\n");
		printf("EPEISTES : \n	16: Volée d'épée\n	17: Rage du duéliste\n	18: Danse de la mort\n");
		printf("\n QUITTER : 0 ");
		printf("::::CHOIX -> ");
		scanf("%i", &choix);
		

		switch(choix)
		{
			case 1:
				//Test attaque Tir de l'aigle
				afficher_description_attaque(tirdelaigle_info);
				cible_attaque.x = 2;
				cible_attaque.y = 3;
				statut_att tir_aigle1 = tirdelaigle(perso1, cible_attaque);
				afficher_statut_attaque(tir_aigle1);

				printf("PERSO1 : %p, PV : %i \n", perso1, perso1->pv);
				printf("PERSO2 : %p, PV : %i \n", perso2, perso2->pv);
				printf("PERSO3 : %p, PV : %i \n", perso3, perso3->pv);
				break;
			case 2 :
				//Test attaque Tir de de combat
				afficher_description_attaque(tirdecombat_info);
				cible_attaque.x = 4;
				cible_attaque.y = 3;
				statut_att tir_combat1 = tirdecombat(perso1, cible_attaque);
				afficher_statut_attaque(tir_combat1);

				printf("PERSO1 : %p, PV : %i \n", perso1, perso1->pv);
				printf("PERSO2 : %p, PV : %i \n", perso2, perso2->pv);
				printf("PERSO3 : %p, PV : %i \n", perso3, perso3->pv);
				break;
			case 3 :
				//Test attaque Barrage absolu
				afficher_description_attaque(barrageabsolu_info);
				cible_attaque.x = 3;
				cible_attaque.y = 3;
				statut_att barrage = barrageabsolu(perso1, cible_attaque);
				afficher_statut_attaque(barrage);
				
				printf("PERSO1 : %p, PV : %i \n", perso1, perso1->pv);
				printf("PERSO2 : %p, PV : %i \n", perso2, perso2->pv);
				printf("PERSO3 : %p, PV : %i \n", perso3, perso3->pv);
				break;
			case 4 :
				//Test attaque Coup de dague
				afficher_description_attaque(coupdeDague_info);
				cible_attaque.x = 5;
				cible_attaque.y = 6;
				statut_att coupDague = coupdeDague(perso1, cible_attaque);
				afficher_statut_attaque(coupDague);
				
				printf("PERSO1 : %p, PV : %i \n", perso1, perso1->pv);
				printf("PERSO2 : %p, PV : %i \n", perso2, perso2->pv);
				printf("PERSO3 : %p, PV : %i \n", perso3, perso3->pv);
				break;

			case 5 : 
				//Test attaque lancer de dagues
				afficher_description_attaque(lancerdedagues_info);
				cible_attaque.x = 4;
				cible_attaque.y = 3;
				statut_att lancerDagues = lancerdedagues(perso1, cible_attaque);
				afficher_statut_attaque(lancerDagues);
				
				printf("PERSO1 : %p, PV : %i \n", perso1, perso1->pv);
				printf("PERSO2 : %p, PV : %i \n", perso2, perso2->pv);
				printf("PERSO3 : %p, PV : %i \n", perso3, perso3->pv);
				break;
			case 6 : 
				//Test attaque Beni Par les dieux du sang
				afficher_description_attaque(beniparlesdieuxdusang_info);
				cible_attaque.x = 4;
				cible_attaque.y = 3;
				statut_att beni = beniparlesdieuxdusang(perso2, cible_attaque);
				afficher_statut_attaque(beni);
				
				printf("PERSO1 : %p, PV : %i \n", perso1, perso1->pv);
				printf("PERSO2 : %p, PV : %i \n", perso2, perso2->pv);
				printf("PERSO3 : %p, PV : %i \n", perso3, perso3->pv);
				break;
			case 7 : 
				//Test attaque firaball
				afficher_description_attaque(firaball_info);
				cible_attaque.x = 4;
				cible_attaque.y = 3;
				statut_att firaballs = firaball(perso2, cible_attaque);
				afficher_statut_attaque(firaballs);
				
				printf("PERSO1 : %p, PV : %i \n", perso1, perso1->pv);
				printf("PERSO2 : %p, PV : %i \n", perso2, perso2->pv);
				printf("PERSO3 : %p, PV : %i \n", perso3, perso3->pv);
				break;
			case 8 : 
				//Test attaque fabriquantdeglacons
				afficher_description_attaque(fabriquantdeglacons_info);
				cible_attaque.x = 4;
				cible_attaque.y = 3;
				statut_att fabriquantdeglaconsa = fabriquantdeglacons(perso2, cible_attaque);
				afficher_statut_attaque(fabriquantdeglaconsa);
				
				printf("PERSO1 : %p, PV : %i \n", perso1, perso1->pv);
				printf("PERSO2 : %p, PV : %i \n", perso2, perso2->pv);
				printf("PERSO3 : %p, PV : %i \n", perso3, perso3->pv);
				break;
			case 9 : 
				//Test attaque interitumnotitia
				afficher_description_attaque(interitumnotitia_info);
				cible_attaque.x = 4;
				cible_attaque.y = 3;
				statut_att ainteritumnotitia = interitumnotitia(perso2, cible_attaque);
				afficher_statut_attaque(ainteritumnotitia);
				
				printf("PERSO1 : %p, PV : %i \n", perso1, perso1->pv);
				printf("PERSO2 : %p, PV : %i \n", perso2, perso2->pv);
				printf("PERSO3 : %p, PV : %i \n", perso3, perso3->pv);
				break;
			
			case 10 : 
				//Test attaque benedictiondivine
				afficher_description_attaque(benedictiondivine_info);
				cible_attaque.x = 4;
				cible_attaque.y = 3;
				statut_att abenedictiondivine = benedictiondivine(perso2, cible_attaque);
				afficher_statut_attaque(abenedictiondivine);
				
				printf("PERSO1 : %p, PV : %i \n", perso1, perso1->pv);
				printf("PERSO2 : %p, PV : %i \n", perso2, perso2->pv);
				printf("PERSO3 : %p, PV : %i \n", perso3, perso3->pv);
				break;

			
			
			case 11 : 
				//Test attaque transfert de vitalité
				afficher_description_attaque(transfertdevitalite_info);
				cible_attaque.x = 4;
				cible_attaque.y = 3;
				statut_att benediction = transfertdevitalite(perso1, cible_attaque);
				afficher_statut_attaque(benediction);
				
				printf("PERSO1 : %p, PV : %i \n", perso1, perso1->pv);
				printf("PERSO2 : %p, PV : %i \n", perso2, perso2->pv);
				printf("PERSO3 : %p, PV : %i \n", perso3, perso3->pv);
				break;

			case 12 : 
				//Test attaque gracedeladeesse
				afficher_description_attaque(gracedeladeesse_info);
				cible_attaque.x = 4;
				cible_attaque.y = 3;
				statut_att agracedeladeesse = gracedeladeesse(perso2, cible_attaque);
				afficher_statut_attaque(agracedeladeesse);
				
				printf("PERSO1 : %p, PV : %i \n", perso1, perso1->pv);
				printf("PERSO2 : %p, PV : %i \n", perso2, perso2->pv);
				printf("PERSO3 : %p, PV : %i \n", perso3, perso3->pv);
				break;

			case 13 : 
				//Test attaque coupepeesimple
				afficher_description_attaque(coupepeesimple_info);
				cible_attaque.x = 4;
				cible_attaque.y = 3;
				statut_att acoupepeesimple = coupepeesimple(perso2, cible_attaque);
				afficher_statut_attaque(acoupepeesimple);
				
				printf("PERSO1 : %p, PV : %i \n", perso1, perso1->pv);
				printf("PERSO2 : %p, PV : %i \n", perso2, perso2->pv);
				printf("PERSO3 : %p, PV : %i \n", perso3, perso3->pv);
				break;

			case 14 : 
				//Test attaque coupdebouclierrecul
				afficher_description_attaque(coupdebouclierrecul_info);
				cible_attaque.x = 4;
				cible_attaque.y = 3;
				statut_att acoupdebouclierrecul = coupdebouclierrecul(perso2, cible_attaque);
				afficher_statut_attaque(acoupdebouclierrecul);
				
				printf("PERSO1 : %p, PV : %i \n", perso1, perso1->pv);
				printf("PERSO2 : %p, PV : %i \n", perso2, perso2->pv);
				printf("PERSO3 : %p, PV : %i \n", perso3, perso3->pv);
				break;

			case 15 : 
				//Test attaque volontedudefenseur
				afficher_description_attaque(volontedudefenseur_info);
				cible_attaque.x = 4;
				cible_attaque.y = 3;
				statut_att avolontedudefenseur = volontedudefenseur(perso2, cible_attaque);
				afficher_statut_attaque(avolontedudefenseur);
				
				printf("PERSO1 : %p, PV : %i \n", perso1, perso1->pv);
				printf("PERSO2 : %p, PV : %i \n", perso2, perso2->pv);
				printf("PERSO3 : %p, PV : %i \n", perso3, perso3->pv);
				break;

			case 16 : 
				//Test attaque voleedepee
				afficher_description_attaque(voleedepee_info);
				cible_attaque.x = 4;
				cible_attaque.y = 3;
				statut_att avoleedepee = voleedepee(perso2, cible_attaque);
				afficher_statut_attaque(avoleedepee);
				
				printf("PERSO1 : %p, PV : %i \n", perso1, perso1->pv);
				printf("PERSO2 : %p, PV : %i \n", perso2, perso2->pv);
				printf("PERSO3 : %p, PV : %i \n", perso3, perso3->pv);
				break;

			case 17 : 
				//Test attaque ragedudueliste
				afficher_description_attaque(ragedudueliste_info);
				cible_attaque.x = 4;
				cible_attaque.y = 3;
				statut_att aragedudueliste = ragedudueliste(perso2, cible_attaque);
				afficher_statut_attaque(aragedudueliste);
				
				printf("PERSO1 : %p, PV : %i \n", perso1, perso1->pv);
				printf("PERSO2 : %p, PV : %i \n", perso2, perso2->pv);
				printf("PERSO3 : %p, PV : %i \n", perso3, perso3->pv);
				break;

			case 18 : 
				//Test attaque dansedelamort
				afficher_description_attaque(dansedelamort_info);
				cible_attaque.x = 4;
				cible_attaque.y = 3;
				statut_att adansedelamort = dansedelamort(perso2, cible_attaque);
				afficher_statut_attaque(adansedelamort);
				
				printf("PERSO1 : %p, PV : %i \n", perso1, perso1->pv);
				printf("PERSO2 : %p, PV : %i \n", perso2, perso2->pv);
				printf("PERSO3 : %p, PV : %i \n", perso3, perso3->pv);
				break;
		}	
		
	}
}
