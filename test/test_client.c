/**
 * \file test_client.c
 * \brief Fichier de test du client pour la partie en réseau
 * \author Pierre garcon
 * \version 1.0
 * \date 10 mars
 *
 *
 */

#include "../include/reseau.h"
#include "../include/archer.h"




int main(void)
{
  int erreur = reseau_demarrer();

  SOCKET sock;
  SOCKADDR_IN sin;
  socklen_t recsize = sizeof(sin);
  int test = 0;


  if(!erreur)
  {
      /* Création de la socket */
      sock = socket(AF_INET, SOCK_STREAM, 0);

      erreur = bind(sock, (SOCKADDR*)&sin, recsize);

      /* Configuration de la connexion */
      sin = reseau_init_sin_client("127.0.0.1");

      /* Si le client arrive à se connecter */
      do{
        erreur = reseau_connection_client(sock, &sin, sizeof(sin));
        if(erreur != SOCKET_ERROR)
        {
          /*je commence par charger la carte*/
          if(chargement_carte() != 0)
          {
            fprintf(stderr, "Failed to load map.\n");
            return EXIT_FAILURE;
          }


          equipe_t equipe = -1;
          printf("\nLe deuxième joueur se connecte\n");

          /*Je transmet l'equipe à laquelle appartiendra le joueur*/
          recv(sock, (char*)&equipe, sizeof(equipe_t), 0);
          if(equipe != -1)
            printf("\nles joeurs sont connecté\n");


          /*Une fois les deux joueur connecté, je récupère la carte envoyé par le serveur*/

          reseau_map_recevoir(sock, map);
          /*Puis je l'affiche*/

          afficher_plateau(VISUEL);

          /*Je creer les deux equipe de personnage*/

          liste_perso_t * equipe_pers = liste_perso_creer(NB_PERS);
          liste_perso_t * equipe_pers_advers = liste_perso_creer(NB_PERS);

          /*Je selectionne mon équipe*/


          liste_perso_equipe_creer(equipe_pers, equipe);
          int autorise = 1;


          /*J'envoie mon equipe au serveur et je récupère l'equipe adverse*/

          reseau_equipe_envoyer(sock, equipe_pers);

          printf("\n------------------------------------------------------------------------------------------------------\n");
          printf("\nComposition equipe adverse\n");
          printf("\n------------------------------------------------------------------------------------------------------\n");

          reseau_equipe_recevoir(sock, equipe_pers_advers);


          /*Je prépare la carte*/

          placer_equipes(equipe_pers, equipe_pers_advers);

          /*On peut commencer à jouer*/


          int continuerJeu = 1;
          int p_equipe;
          t_temps cycle_jour_nuit;
          //int poubelle;

          //while(recv(sock, (char*)&poubelle, sizeof(int), 0) != SOCKET_ERROR) printf("\nPoubelle\n");




          while(continuerJeu == 1)
        	{
            afficher_plateau(VISUEL);
            printf("\nTour %d\n", 1);
            printf("\nattendez votre tour\n");
            if(recv(sock, (char*)&continuerJeu, sizeof(int), 0) != SOCKET_ERROR)
            {
                printf("\nContinuer jeu : %i\n\n", continuerJeu);
                reseau_recevoir_variables(&p_equipe, &cycle_jour_nuit, sock);
                reseau_jouer_tour(equipe_pers, p_equipe, sock);
                send(sock, (char*)&continuerJeu, sizeof(int), 0);
            }
            else
            {
              printf("Réception echoué\n");
            }
        	}


          test = 1;
        }
        else
            printf("Impossible de se connecter\n");
      }while(test == 0);


      if(shutdown(sock, 2) == -1)
        printf("\nArrêt impossible\n");


      /* On ferme la socket précédemment ouverte */

      closesocket(sock);

      reseau_fermer();

  }

  return EXIT_SUCCESS;
}
