/**
 * \file test_combats.c
 * \brief Fichier pour tester les combats
 * \author 
 * \version 1.0
 * \date 
 *
 * Permet de s'assurer que les fonction pour le combat fonctionne
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include "../include/archer.h"
#include "../include/commun.h"
#include "../include/combat.h"

/**
 * \fn int main(void)
 * \brief fonction main
 * 
 * \param self
 * 
 * \return 0
 * 
 */
int main()
{
  printf("\n\nDébuts test de combats\n\n");

  equipe_t bleu = Bleu;
  equipe_t rouge = Rouge;

  equipe_t * eq_bleu = &bleu;
  equipe_t * eq_rouge = &rouge;



  archer_t * archer1 = creer_archer(eq_bleu);
  archer_t * archer2 = creer_archer(eq_rouge);



  printf("\nAffichage des personnages : \n\n");

  archer1->print((personnage_t*)archer1);
  archer2->print((personnage_t*)archer2);

  printf("\nDébut des combats : \n");

  attaquer((personnage_t*)archer1,(personnage_t*)archer2, 2);
  attaquer((personnage_t*)archer2, (personnage_t*)archer1, 1);

  printf("\n\nfin des combats\n\n");

  printf("\nAffichage des personnages après combat: \n\n");

  archer1->print((personnage_t*)archer1);
  archer2->print((personnage_t*)archer2);

  printf("\n\nfin du test de combats\n");

  return 0;

}
