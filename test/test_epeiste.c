/**
 * \file test_epeiste.c
 * \brief Fichier de test de la classe epeiste
 * \author Pierre Garcon
 * \version 1.0
 * \date 2 fevrier
 *
 * Test de toute les caracteristique de la classe epeiste
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include "../include/epeiste.h"
/**
 * \fn int main (void)
 * \brief Fonction main
 *
 * \return int 0
 */
int main()
{
  /**_*/
  err_t erreur;
  etat_t etat;
  liste_perso_t * equipe = NULL;
  equipe_t coul_equipe;
  race_t * orc = race_creer_orc();
  race_t * demon = race_creer_demon();

  coul_equipe = Bleu;

  /*On commence par tester la création des epeistes*/
  printf("\nCeci est un test de epeistes");
  epeiste_t * epe1, *epe2;
  printf("\n Test de création d'un epeiste : \n");
  epe1 = creer_epeiste(coul_equipe, orc, equipe);
  epe2 = creer_epeiste(coul_equipe, demon, equipe);

  /*Ensuite, on test l'affichage*/
  printf("\n Test d'affichage : \n");
  epe1->print((personnage_t*)epe1);
  printf("\nPersonnage 2 :\n");
  epe2->print((personnage_t*)epe2);


  /*On active les bonus des deux personnages (bonus différents en fonction des races)*/
  epe1->race->race_activer_bonus(epe1);
  epe2->race->race_activer_bonus(epe2);

  /*On affiche les personnage après activation des bonus pour voir si ils ont mepeé*/
  printf("\n--------------------------------------------------------------------\n");
  printf("\nAffichage après activation du bonus\n");

  printf("\n Test d'affichage : \n");
  epe1->print((personnage_t*)epe1);
  printf("\nPersonnage 2 :\n");
  epe2->print((personnage_t*)epe2);
  printf("\n--------------------------------------------------------------------\n");

  /*On test l'attaque sur les archer*/
  printf("\nTest d'attaque sur un archer : \n");

  epe2->deplacer(4,2,(personnage_t*)epe2);

  printf("\naffichage des coordonnées de archer2\n\n");
  printf("x : %d \ny : %d", epe2->pos_x, epe2->pos_y);

  statut_att comp = epe1->competence1((personnage_t*)epe1, epe2->position_pers((personnage_t*)epe2));
  afficher_statut_attaque(comp);

  etat = Vivant;
  /*On vérifie l'état de l'epeiste*/
  if(etat == Vivant)
  {
    printf("\nL'epeiste est toujours vivant");
  }
  else
  {
    printf("\nL'epeiste est mort\n");
  }


  if(etat == Vivant)
  {
    printf("\nL'epeiste est toujours vivant");
  }
  else
  {
    printf("\nL'epeiste est mort\n");
  }

  /*On désactive les bonus*/
  epe1->race->race_desactiver_bonus(epe1);
  epe2->race->race_desactiver_bonus(epe2);

  /*On test l'affichage et on supprime les epeistes*/
  printf("\nAfficher après dégats et les bonus sont enlevés : \n");
  epe1->print((personnage_t*)epe1);

  printf("\ntest de suppression : \n");
  erreur = epe1->detruire((personnage_t**)&epe1);
  erreur = epe2->detruire((personnage_t **)&epe2);

  demon->detruire(&demon);
  orc->detruire(&orc);

  if(erreur != Ok)
  {
    printf("\nerreur de suppression\n");
  }
  else
    printf("\nTout s'est bien passé\n");
  return 0;
}
