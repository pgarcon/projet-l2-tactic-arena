/**
 * \file test_equipe.c
 * \brief Fichier pour tester les equipe
 * \author Pierre garcon
 * \version 1.0
 * \date 2 Fevrier
 *
 * Permet de s'assurer que les fonction pour les equipe fonctionne
 *
 */

#include "../include/equipe.h"
#include "../include/personnage.h"
#include "../include/deplacements.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>



/**
 * \fn int main(void)
 * \brief fonction main
 *
 * \param self
 *
 * \return EXIT_SUCCESS
 *
 */
int main(){

  /*Ceci est un test des equipes*/
  liste_perso_t * armee1;
  liste_perso_t * armee2;
  liste_perso_t * armee3;
  err_t erreur;

  t_temps temps = jour;

  equipe_t bleu = Bleu;
  equipe_t rouge = Rouge;

  int nb_arc=0, nb_ass=0, nb_mage=0, nb_tank=0, nb_heal=0, nb_epe=0;


  printf("Ceci est un test des équipes\n");
  armee1 = liste_perso_creer(NB_PERS);
  armee2 = liste_perso_creer(NB_PERS);
  armee3 = liste_perso_creer(NB_PERS);

  printf("\nCréation : Ok\n");

  liste_ajouter_perso_equipe(archer, demon, armee3, Bleu, &nb_arc, &nb_ass, &nb_heal, &nb_tank, &nb_epe, &nb_mage);
  liste_ajouter_perso_equipe(tank, kobolt, armee3, Bleu, &nb_arc, &nb_ass, &nb_heal, &nb_tank, &nb_epe, &nb_mage);
  liste_ajouter_perso_equipe(mage, demon, armee3, Bleu, &nb_arc, &nb_ass, &nb_heal, &nb_tank, &nb_epe, &nb_mage);
  liste_ajouter_perso_equipe(assassin, elfe, armee3, Bleu, &nb_arc, &nb_ass, &nb_heal, &nb_tank, &nb_epe, &nb_mage);
  liste_ajouter_perso_equipe(archer, humain, armee3, Bleu, &nb_arc, &nb_ass, &nb_heal, &nb_tank, &nb_epe, &nb_mage);

  /***/
  printf("\nTest ajout de personnage dans la liste\n");

  liste_perso_equipe_creer(armee1, bleu);
  liste_perso_equipe_creer(armee2, rouge);

  printf("\nTest ajout de personnage : Ok\n");

  printf("\n------------------------------------------------------------------\n");
  printf("\nActivation des bonus de race\n");
  printf("\n------------------------------------------------------------------\n");

  race_activer(armee2);
  //race_activer(armee2);

  printf("\n------------------------------------------------------------------\n");
  printf("\nAffichage après activation des races\n");
  printf("\n------------------------------------------------------------------\n");
  liste_afficher(armee2);


  printf("\n------------------------------------------------------------------\n");
  printf("\nMAJ des bonus de race (nuit)\n");
  printf("\n------------------------------------------------------------------\n");
  race_maj_equipe(armee1, armee2, temps);

  printf("\n------------------------------------------------------------------\n");
  printf("\nAffichage après activation des la nuit\n");
  printf("\n------------------------------------------------------------------\n");
  liste_afficher(armee2);

  //printf("\n------------------------------------------------------------------\n");
  //printf("\nTest d'affichage de l'equipe 1\n");
  //printf("\n------------------------------------------------------------------\n");

  /*test affichage d'une équipe*/
  printf("\n------------------------------------------------------------------\n");
  printf("\nAffichage de la liste avec methode SDL\n");
  printf("\n------------------------------------------------------------------\n");
  liste_afficher(armee3);

  race_maj_equipe(armee1, armee2, jour);
  printf("\n\n------------------------------------------------------------------\n");
  printf("\nTest d'affichage de l'equipe 2 (avec les bonus)\n");
  printf("\n------------------------------------------------------------------\n");

  race_activer(armee3);

  /*test affichage d'une équipe*/
  liste_afficher(armee3);


  printf("\n\n------------------------------------------------------------------\n");
  printf("\nTest d'affichage a partir d'un personnage\n");
  printf("\n------------------------------------------------------------------\n");
  liste_afficher(armee2->liste[0]->liste);

  printf("\nTest affichage : Ok\n");

  printf("\n\n------------------------------------------------------------------\n");
  printf("\nTest de suppression d'un personnage\n");
  printf("\n------------------------------------------------------------------\n");

  armee2->liste[1]->etat = Mort;
  personnage_MAJ_equipe_mort(armee2->liste[1]);

  printf("\nOk\n");

  printf("\nTest suppression de l'quipe\n");

  erreur = liste_detruire(&armee1);

  if(erreur == Ok)
  {
    printf("\nLa suppression s'est bien déroulé");
  }

  erreur = liste_detruire(&armee2);

  if(erreur == Ok)
  {
    printf("\nLa suppression s'est bien déroulé");
  }

  if(armee1 == NULL)
    printf("\nArmee null");
  else
    printf("\nArmée non null");

  if(armee2 == NULL)
    printf("\nArmee null");
  else
    printf("\nArmée non null ");

  printf("\nSuppression : Ok\n");




  return EXIT_SUCCESS;
}
