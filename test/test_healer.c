/**
 * \file test_healer.c
 * \brief Fichier de test de la classe healer
 * \author Pierre Garcon
 * \version 1.0
 * \date 2 fevrier
 *
 * Test de toute les caracteristique de la classe healer
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include "../include/healer.h"

/**
 * \fn int main (void)
 * \brief Fonction main
 *
 * \return int 0
 */
int main()
{
  /**_*/
  err_t erreur;
  etat_t etat;
  equipe_t coul_equipe;
  liste_perso_t * equipe = NULL;
  race_t * orc = race_creer_orc();
  race_t * demon = race_creer_demon();

  coul_equipe = Bleu;

  /*On commence par tester la création des healers*/
  printf("\nCeci est un test de healers");
  healer_t * heal1, *heal2;
  printf("\n Test de création d'un healer : \n");
  heal1 = creer_healer(coul_equipe, orc, equipe);
  heal2 = creer_healer(coul_equipe, demon, equipe);

  /*Ensuite, on test l'affichage*/
  printf("\n Test d'affichage : \n");
  heal1->print((personnage_t*)heal1);
  printf("\nPersonnage 2 :\n");
  heal2->print((personnage_t*)heal2);


  /*On active les bonus des deux personnages (bonus différents en fonction des races)*/
  heal1->race->race_activer_bonus(heal1);
  heal2->race->race_activer_bonus(heal2);

  /*On affiche les personnage après activation des bonus pour voir si ils ont mhealé*/
  printf("\n--------------------------------------------------------------------\n");
  printf("\nAffichage après activation du bonus\n");

  printf("\n Test d'affichage : \n");
  heal1->print((personnage_t*)heal1);
  printf("\nPersonnage 2 :\n");
  heal2->print((personnage_t*)heal2);
  printf("\n--------------------------------------------------------------------\n");

  etat = Vivant;

  /*On vérifie l'état de l'healer*/
  if(etat == Vivant)
  {
    printf("\nL'healer est toujours vivant");
  }
  else
  {
    printf("\nL'healer est mort\n");
  }


  if(etat == Vivant)
  {
    printf("\nL'healer est toujours vivant");
  }
  else
  {
    printf("\nL'healer est mort\n");
  }

  /*On désactive les bonus*/
  heal1->race->race_desactiver_bonus(heal1);
  heal2->race->race_desactiver_bonus(heal2);

  /*On test l'affichage et on supprime les healers*/
  printf("\nAfficher après dégats et les bonus sont enlevés : \n");
  heal1->print((personnage_t*)heal1);

  printf("\ntest de suppression : \n");
  erreur = heal1->detruire((personnage_t**)&heal1);
  erreur = heal2->detruire((personnage_t **)&heal2);

  demon->detruire(&demon);
  orc->detruire(&orc);

  if(erreur != Ok)
  {
    printf("\nerreur de suppression\n");
  }
  else
    printf("\nTout s'est bien passé\n");
  return 0;
}
