#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "../include/lignedevue.h"


void remise_a_zero_repere_ldv()
{
	int i, j;
	for(i = 0; i < N; i++)
	{
		for(j = 0; j < M; j++)
		{
			
			afficher_plateau(VISUEL);
		}
	}
}

void initialiser_map_vide()
{
	int i, j;
	for(i = 0; i < N; i++)
	{
		for(j = 0; j < M; j++)
		{
			map[i][j].type_emplacement = VIDE;	
		}
	}
}

void transformer_position_perso(personnage_t * perso)
{
	int i, j;
	for(i = 0; i < N; i++)
	{
		for(j = 0; j < M; j++)
		{
			if(map[i][j].type_emplacement == POSITION_ROUGE || map[i][j].type_emplacement == POSITION_BLEUE)
			{
				map[i][j].type_emplacement = PERSO;
				map[i][j].perso = perso;
			}
		}
	}
}	

int main()
{
	srand(time(NULL));
	int test_global = 1;
	int test_entier = 1;
	int i, j;
	
	
	personnage_t * perso = malloc( sizeof( personnage_t ));
	perso->equipe = Bleu;
	t_pos pos_perso; 
	
	initialiser_map_vide();
	

	t_pos mur;
	mur.x = 5;
	mur.y = 14;	
	map[mur.x][mur.y].type_emplacement = MUR;

	

	if(test_global == 1)
	{
		printf("Les '.' représentent les cases non ciblables\n");
		
		pos_perso.x = 4;
		pos_perso.y = 19;
		map[pos_perso.x][pos_perso.y].type_emplacement = PERSO;
		map[pos_perso.x][pos_perso.y].perso = perso;

		liste_pos_t * liste_pos_invalides = ligne_de_vue(pos_perso, mur, 150);
		ajouter_marqueurs_attaque_carte(liste_pos_invalides);
		afficher_plateau(VISUEL);
		retirer_marqueurs_attaque_carte(liste_pos_invalides);
		liste_positions_detruire(&liste_pos_invalides);
	}
	else
	{
		for(i = 0; i < N; i++)
		{
			for(j = 0; j < M; j++)
			{
				if(map[i][j].type_emplacement == VIDE)
				{
					initialiser_map_vide();
					map[i][j].type_emplacement = PERSO;
					map[i][j].perso = perso;
					map[mur.x][mur.y].type_emplacement = MUR;
					pos_perso.x = i;
					pos_perso.y = j;

					liste_pos_t * liste_pos_invalides = ligne_de_vue(pos_perso, mur, 189);
					ajouter_marqueurs_attaque_carte(liste_pos_invalides);
					afficher_plateau(VISUEL);
					retirer_marqueurs_attaque_carte(liste_pos_invalides);
					liste_positions_detruire(&liste_pos_invalides);
				}
			}
		}
	}

	if(test_entier == 0)
	{
		if( chargement_carte() == 1)
		{
			return EXIT_FAILURE;
		}

		t_pos position_perso2 = {11,5};
		personnage_t * perso_pos = malloc( sizeof( personnage_t ));
		perso_pos->equipe = Rouge;
		perso_pos->pos_x = position_perso2.x;
		perso_pos->pos_y = position_perso2.y;
		map[position_perso2.x][position_perso2.y].type_emplacement = PERSO;
		map[position_perso2.x][position_perso2.y].perso = perso_pos;
		
		transformer_position_perso(perso);

		liste_pos_t * nouvelle_liste = attaque_valide(perso_pos, 25);
		ajouter_marqueurs_attaque_carte(nouvelle_liste);
		afficher_plateau(VISUEL);
		retirer_marqueurs_attaque_carte(nouvelle_liste);
	}

	return 0;
}
