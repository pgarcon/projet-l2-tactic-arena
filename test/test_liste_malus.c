/**
 * \file test_liste_malus.c
 * \brief Fichier pour tester les malus
 * \author
 * \version 1.0
 * \date
 *
 * Permet de s'assurer que les fonction pour les malus fonctionne
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include "../include/liste_malus.h"
#include "../include/archer.h"
#include "../include/equipe.h"


/**
 * \fn main(void)
 * \brief fonction main
 *
 * \param self
 *
 * \return void pas de retour
 *
 */
int main() {
  /*Création des variables de travaille*/

  err_t erreur;
  equipe_t coul_equipe;
  liste_perso_t * equipe = NULL;
  race_t * orc = race_creer_orc();
  coul_equipe = Bleu;

  archer_t * arch1 = creer_archer(coul_equipe, orc, equipe);

  /*
  printf("\nTest d'afficha d'un archer : \n");
  arch1->print((personnage_t*)arch1);
  */

  printf("\nInitialisation de la liste de malus\n");
  liste_malus_initialiser();
  liste_malus_actualiser();

  printf("\nTest de malus sur archer\n\n");

  printf("\nTest de l'ajout du malus dans la liste\n\n");
  liste_malus_ajouter(degats, 5, (personnage_t*)arch1, 3);

  printf("\nTest de l'affichage de la liste de malus\n\n");
  liste_malus_afficher();

  printf("\nTest de la mise à jour de la liste\n\n");
  liste_malus_actualiser();

  printf("\n\n----------------------------------------------------------------------------\n\n");

  printf("\nTest de l'affichage de la liste de malus après MAj 1 \n\n");
  liste_malus_afficher();

  printf("\n\n----------------------------------------------------------------------------\n\n");

  printf("\nTest d'ajout d'un deuxième malus :\n");
  archer_t * arch2 = creer_archer(coul_equipe, orc, equipe);
  liste_malus_ajouter(bouclier, 8, (personnage_t*)arch2, 2);

  printf("\nTest de l'affichage de la liste de malus\n\n");
  liste_malus_afficher();

  printf("\nTest de la mise à jour de la liste\n\n");
  liste_malus_actualiser();
  liste_malus_actualiser();
  liste_malus_actualiser();
  liste_malus_actualiser();

  printf("\n\n----------------------------------------------------------------------------\n\n");

  printf("\nTest de l'affichage de la liste de malus après MAj\n\n");
  liste_malus_afficher();

  printf("\nTest de la suppression de la liste\n\n");

  liste_malus_vider();
  liste_malus_afficher();

  liste_malus_detruire();
  arch1->detruire((personnage_t**)&arch1);
  arch2->detruire((personnage_t**)&arch2);
  orc->detruire(&orc);

  erreur = Ok;
  if(erreur != Ok)
    printf("Erreur de suppression\n");
  else
    printf("\nSuppression : Ok\n");

  return 0;
}
