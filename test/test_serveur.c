/**
 * \file test_serveur.c
 * \brief Fichier de test du serveur pour la partie en réseau
 * \author Pierre garcon
 * \version 1.0
 * \date 10 mars
 *
 *
 */

#include "../include/reseau.h"
#include "../include/archer.h"
#include <unistd.h>
#include <process.h>



/*Pour compiler sous windows : gcc test_reseau.c -o test_reseau -lws2_32*/



int main()
{
  //pthread_t threadClient;


  /*Si on est sous windows, il faut utiliser ces fonctions*/
  int erreur = reseau_demarrer();


  /*Code sur les sockets*/

  /*Je créer mon socket et je l'initialise, les valeur AF_NET et SOCK_STREAM sont pour utiliser le protocole TCP/IP*/
  /*Je créer mon sin qui est une structure pour le serveur*/
  SOCKET sock;
  SOCKADDR_IN sin;
  socklen_t recsize = sizeof(sin);

  /*Je fais la même chose pour le joueur 1*/
  SOCKADDR_IN joueur1_sin;
  SOCKET joueur1_sock;
  socklen_t joueur1_recsize = sizeof(joueur1_sin);

  /*Je fais la même chose pour le joueur2*/
  SOCKADDR_IN joueur2_sin;
  SOCKET joueur2_sock;
  socklen_t joueur2_recsize = sizeof(joueur2_sin);
  int sock_err;

  if(erreur == 0)
  {
    /*Création d'un socket*/
    sock = socket(AF_INET, SOCK_STREAM, 0);

    /*Vérification de la validité du socket*/
    if(sock != INVALID_SOCKET)
    {
      printf("La socket est maintenant ouverte en mode TCP_IP");


      sin = reseau_init_sin_serveur();


      /*On assicie les sockets aux informations entrées*/
      sock_err = bind(sock, (SOCKADDR*)&sin, recsize);

      /*On vérifi que tout sock est fonctionnel*/
      if(sock_err != SOCKET_ERROR)
      {
        /*je charge la map sur le serveur, et je l'enverrai aux joueurs en début de partie pour que les deux aient la même*/
        if(chargement_carte() != 0)
        {
          fprintf(stderr, "Failed to load map.\n");
          return EXIT_FAILURE;
        }

        /*je créer les structures que je vais envoyer aux threads*/
        donnees_carte_t data_carte_j1;
        donnees_carte_t data_carte_j2;
        pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
        pthread_t thread[2];

        /*je créer les equipes*/
        equipe_t bleu = Bleu;
        equipe_t rouge = Rouge;

        /*Je créer mes liste de personages*/
        liste_perso_t * equipe1 = liste_perso_creer(NB_PERS);
        liste_perso_t * equipe2 = liste_perso_creer(NB_PERS);

        data_carte_j1.mutex = mutex;
        data_carte_j2.mutex = mutex;


        //personnage_t * pers = malloc(sizeof(personnage_t));

        joueur1_sock = reseau_connection_serveur(sock, &joueur1_sin, &joueur1_recsize);
        joueur2_sock = reseau_connection_serveur(sock, &joueur2_sin, &joueur2_recsize);

        /*Quand les deux joueurs sont connecté, j'envoie un send pour les prévenir*/
        send(joueur1_sock, (char*)&bleu, sizeof(equipe_t), 0);
        send(joueur2_sock, (char*)&rouge, sizeof(equipe_t), 0);

        printf("\nDebut connexion\n\n");
        printf("\n-----------------------------------------------------------------------------------------------------------------");
        printf("\n\nInitialisation de la partie\n\n");
        printf("\n-----------------------------------------------------------------------------------------------------------------\n\n");

        data_carte_j1.arg = (int*)&joueur1_sock;
        data_carte_j2.arg = (int*)&joueur2_sock;


        /*On envoie la carte avec les joueurs dessus*/
        pthread_create(&thread[0], NULL, fonction_envoyer_carte, &data_carte_j1);
        pthread_create(&thread[1], NULL, fonction_envoyer_carte, &data_carte_j2);
        pthread_join(thread[1], NULL);
        pthread_join(thread[0], NULL);
        /*On attend que les envoies soient terminé pour continuer*/



        /*On récupère les équipes des deux joueurs*/

        reseau_equipe_recevoir(joueur1_sock, equipe1);
        reseau_equipe_recevoir(joueur2_sock, equipe2);

        race_activer(equipe1);
        race_activer(equipe2);

        donnees_equipe_t data_equipe_j1;
        donnees_equipe_t data_equipe_j2;

        data_equipe_j1.mutex = mutex;
        data_equipe_j1.arg = (int*)&joueur1_sock;
        data_equipe_j1.equipe = equipe2;

         /*J'envoie l'equipe 2 au personnage 1*/

        data_equipe_j2.mutex = mutex;
        data_equipe_j2.arg = (int*)&joueur2_sock;
        data_equipe_j2.equipe = equipe1;
         /*J'envoie l'equipe 1 au personnage 2*/

        /*Ensuite, on créer deux nouveaux threads, qui vont servir à envoyer les équipes adverses aux joueurs*/


        pthread_create(&thread[0], NULL, fonction_envoyer_equipe, &data_equipe_j1);
        pthread_create(&thread[1], NULL, fonction_envoyer_equipe, &data_equipe_j2);

        pthread_join(thread[0], NULL);
        pthread_join(thread[1], NULL);


        printf("\nPlacement des equipes\n");
        placer_equipes(equipe1, equipe2);
        printf("\nFin du placement\n");

        /*On initialise les variables utiles pour jouer*/




        equipe_t tour_en_cours = Bleu;

        int tour_pas_fini = 1;
      	int compteur_cycle = 1;
      	int continuerJeu = 1;
      	int p_bleu = 0;
      	int p_rouge = 0;
      	personnage_t * joueur_courant; //Contient le joueur courant
      	int nb_tours = 1;
        int poubelle;

        t_temps cycle_jour_nuit = jour;


        liste_malus_initialiser();

        reseau_afficher_direction_persos(equipe1 ,equipe2);

        afficher_plateau(VISUEL);

        while(continuerJeu == 1)
        {
          if(tour_en_cours == Bleu)
          {
            printf("\n\n\nTour du joueur 1\n\n\n\n");
            do{
              send(joueur1_sock, (char*)&continuerJeu, sizeof(int), 0);
              p_bleu = p_bleu%5;
              joueur_courant = equipe1->liste[p_bleu];
              if(reseau_envoyer_variables(p_bleu, cycle_jour_nuit, joueur1_sock) == -1)
                return 1;
              tour_en_cours = Rouge;
              reseau_recuperer_tour(equipe1, p_bleu, joueur_courant, joueur1_sock);
              p_bleu++;
              afficher_plateau(VISUEL);
              printf("\nAttente de la reception de continuer jeu");
              //recv(joueur1_sock, (char*)&continuerJeu, sizeof(int), 0);
              recv(joueur1_sock, (char*)&tour_pas_fini, sizeof(int), 0);
            }while(tour_pas_fini == 1);

          }
          else
          {
            /*je dis au joueur 2 de jouer*/
            printf("\n\n\nTour du joueur 2\n\n\n\n");
            if(send(joueur2_sock, (char*)&continuerJeu, sizeof(int), 0) == SOCKET_ERROR)
              return 1;
            p_rouge = p_rouge%5;
            if(reseau_envoyer_variables(p_rouge, cycle_jour_nuit, joueur2_sock) == -1)
              return 1;
            tour_en_cours = Bleu;
            p_rouge++;
            reseau_recuperer_tour(equipe2, p_bleu, joueur_courant, joueur2_sock);
            recv(joueur2_sock, (char*)&continuerJeu, sizeof(int), 0);

          }

          nb_tours++;
        }


      	liste_detruire(&equipe1);
      	liste_detruire(&equipe2);

        if(shutdown(joueur1_sock, 2) == -1)
          printf("Arrêt impossible");

        if(shutdown(joueur2_sock, 2) == -1)
          printf("Arrêt impossible");

        }
        else
          {
            printf("\nErreur 2\n");
            perror("listen");
          }

      }
      else
      {
        perror("Blind");
      }
      /*Fermeture de la socket client / serveur*/

      printf("\nFermeture de la socket serveur\n");
      closesocket(sock);
      printf("\nFermeture du serveur terminé\n");


    }
    else
      perror("SOCKET");


  reseau_fermer();

  return EXIT_SUCCESS;
}
